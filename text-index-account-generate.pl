#!/usr/bin/perl -w

open ACCOUNT, "text-index-account" or die "can't find text index account file\n";

my %mime_type_word_cost;
my %mime_type_byte_cost;
my %directory_word_cost;
my %directory_byte_cost;

sub by_mime_type_word_cost {
    $mime_type_word_cost{$b} <=> $mime_type_word_cost{$a};
} 

sub by_mime_type_byte_cost {
    $mime_type_byte_cost{$b} <=> $mime_type_byte_cost{$a};
}

sub by_byte_to_word_ratio {
    $mime_type_byte_cost{$b} == 0 && return 1;
    $mime_type_byte_cost{$a} == 0 && return -1;
    ($mime_type_word_cost{$b} / $mime_type_byte_cost{$b}) <=>
	($mime_type_word_cost{$a} / $mime_type_byte_cost{$a});
}

sub by_directory_word_cost {
    $directory_word_cost{$b} <=> $directory_word_cost{$a};
}	


sub by_directory_byte_cost {
    $directory_byte_cost{$b} <=> $directory_byte_cost{$a};
}	


while (<ACCOUNT>) {
    @fields = split /\t/;

    # Skip non log lines, (those that don't have tabs)
    if ($#fields < 3) {
	next;
    }

    if (!defined $mime_type_word_cost{$fields[1]}) {
	$mime_type_word_cost{$fields[1]} = $fields[2];
	$mime_type_byte_cost{$fields[1]} = $fields[3];
    }
    else {
	$mime_type_word_cost{$fields[1]} += $fields[2];
	$mime_type_byte_cost{$fields[1]} += $fields[3];
    }

    my $directory = $fields[0];
    $directory =~ s/^file:\/\///;
    $directory =~ s/\/[^\/]*$//;
    while ($directory =~ /\//) {
	if (!defined $directory_word_cost{$directory}) {
	$directory_word_cost{$directory} = $fields[2];
	$directory_byte_cost{$directory} = $fields[3];
    }
    else {
	$directory_word_cost{$directory} += $fields[2];
	$directory_byte_cost{$directory} += $fields[3];
    }
	
	$directory =~ s/\/[^\/]*$//;
    }
    

}
	    
print "Mime Type\tCost in the Text Index\n";

foreach $mime_type (sort by_mime_type_word_cost (keys %mime_type_word_cost)) {
    print $mime_type, "\t", $mime_type_word_cost{$mime_type} * 4, "\n";
}
    
print "Mime Type\tCost on my System\n";


foreach $mime_type (sort by_mime_type_byte_cost (keys %mime_type_byte_cost)) {
    print $mime_type, "\t", $mime_type_byte_cost{$mime_type}, "\n";
}

print "Mime Type\tFile Bytes per index Byte\n";


foreach $mime_type (sort by_byte_to_word_ratio (keys %mime_type_byte_cost)) {
    if ($mime_type_word_cost{$mime_type} != 0) {
	print $mime_type, "\t", $mime_type_byte_cost{$mime_type} / ($mime_type_word_cost{$mime_type} * 4), "\n";
    }
}


printf "Directory\tCost in the Text Index\n";

foreach $directory (sort by_directory_word_cost (keys %directory_byte_cost)) {
    print $directory, "\t", $directory_word_cost{$directory}, "\n";
}


printf "Directory\tCost of the Text on Disk\n";

foreach $directory (sort by_directory_byte_cost (keys %directory_byte_cost)) {
    print $directory, "\t", $directory_byte_cost{$directory}, "\n";
}
