
#include "medusa-io-handler.h"

#define IO_TEST_FILE "/gnome-source/medusa/databases/io-test-db"
#define IO_TEST_MAGIC_NUMBER "300"
#define IO_TEST_VERSION_NUMBER "0.01"

int data[4] = {235353,4,5,6};
char *blah = "a;klsef;jlskadjf;dlaksfj;ladskfj;lsdkfjas;dlkfjdsa;lkfjsd;lkfjdsa;lf";

int
main () {
  MedusaIOHandler *handler;
  handler = medusa_io_handler_new (IO_TEST_FILE, 
				   IO_TEST_MAGIC_NUMBER,
				   IO_TEST_VERSION_NUMBER, 
				   0);
  medusa_io_handler_append_data (handler, data, 4*sizeof(int));
  medusa_io_handler_append_string (handler, blah);
  medusa_io_handler_free (handler);
	
  return 0;
}
