
#include <medusa-rdb-table.h>
#include <string.h>

#define MEDUSA_RELATIONAL_DATABASE_TEST_FILE "/gnome-source/medusa/databases/test-rdb"

int data[4] = {235353,4,5,6};
char *blah = "a;klsef;jlskadjf;dlaksfj;ladskfj;lsdkfjas;dlkfjdsa;lkfjsd;lkfjdsa;lf";

static void
id_encode (char *result, int id, gpointer data)
{
  strncpy (result, (char *) &id, 4);
}

static void 
id_decode (int *id, char *field, gpointer data)
{
  *id = * (int *) field; 
}

static void
name_encode (char *result, char *name, gpointer data) 
{
  strncpy (result, name, 2);
}

static void
name_decode (char *name, char *field, gpointer data)
{
  strncpy (name, field, 2);
}

static void 
number_encode (char *result, char number, gpointer data)
{
  strncpy (result, &number, 1);
}

static void
number_decode (char *name, char *field, gpointer data)
{
  *name = *field;
}

int
main () {
  MedusaRelationalDatabaseFile *file;
  MedusaRelationalDatabaseTable *table;
  file = medusa_relational_database_file_new (MEDUSA_RELATIONAL_DATABASE_TEST_FILE);
  medusa_relational_database_file_add_field (file, "id", 4, (MedusaRDBEncodeFunc) id_encode, 
					     (MedusaRDBDecodeFunc) id_decode);
  medusa_relational_database_file_add_field (file, "name", 2, (MedusaRDBEncodeFunc) name_encode,
					     (MedusaRDBDecodeFunc) name_decode);
  medusa_relational_database_file_add_field (file, "number", 1, (MedusaRDBEncodeFunc) number_encode,
					     (MedusaRDBDecodeFunc) number_decode);
  
  table = medusa_relational_table_all_rows (file);
  medusa_relational_table_insert (table, "ffffgg1");
  medusa_relational_table_insert (table, "bekarbs");
  medusa_relational_database_file_free (file);
	
  return 0;
}
