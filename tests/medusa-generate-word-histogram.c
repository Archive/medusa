/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  medusa-generate-word-histogram.c - Read the medusa index and generate
 *       a profile of the number of times words are use 1, 2, 3, etc times
 *       to determine what kinds of caching might work
 *
 *  Copyright (C) 2001 Rebecca Schulman
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@ai.mit.edu>
 */

#include <stdio.h>

#include <glib.h>

#include <libmedusa/medusa-index-filenames.h>
#include <libmedusa-internal/medusa-text-index.h>
#include <libmedusa-internal/medusa-versioned-file.h>

#define START_INDEX "text-index-start-file"
#define START_INDEX_MAGIC_NUMBER "9125"
#define START_INDEX_VERSION_NUMBER "0.1"

static void
print_frequency (gpointer key,
                 gpointer value,
                 gpointer data)
{
        int occurrences_count, word_count;

        occurrences_count = (int) key;
        word_count = (int) value;
        
        g_print ("%d\t%d\n", occurrences_count, word_count);
}

int
main (int argc, char **argv)
{
        char *start_index_filename;
        int next_word[2];
        MedusaVersionedFileResult open_result;
        MedusaVersionedFile *start_index;
        GHashTable *histogram;
        int frequency, number_of_times;

        start_index_filename = medusa_generate_index_filename (START_INDEX, "", FALSE);
        histogram = g_hash_table_new (g_direct_hash,
                                      g_direct_equal);
        
        start_index = medusa_versioned_file_open (start_index_filename, 
                                                  START_INDEX_MAGIC_NUMBER,
                                                  START_INDEX_VERSION_NUMBER,
                                                  &open_result);
        if (open_result != MEDUSA_VERSIONED_FILE_OK) {
                g_print ("Can't find index file %s. Error was %s\n", 
                         start_index_filename,
                         medusa_versioned_file_result_to_string (open_result));
                return 1;
        }
        
        /* Read the header */

        while (medusa_versioned_file_read (start_index,
                                           next_word, 
                                           sizeof(gint32), 
                                           2) == MEDUSA_VERSIONED_FILE_OK) {
                if (next_word[0] == 0) {
                        /* It's an empty node in the hash table */
                        continue;
                }
                number_of_times = next_word[1] - next_word[0];
                frequency = (int) g_hash_table_lookup (histogram,
                                                       (gpointer) number_of_times);
                g_hash_table_insert (histogram, 
                                     (gpointer) number_of_times, 
                                     (gpointer) (frequency + 1));

        }

        g_hash_table_foreach (histogram, print_frequency, NULL);

        return 0;
}
