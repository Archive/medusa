#!/usr/bin/perl -w

# Perl script to run the medusa search uri tests, and 
# report back results.
# by Rebecca Schulman <rebecka@eazel.com>
# (C) Eazel 2000
# This file is GPLed.  For more info, see medusa's COPYING file

my $directory_to_index_for_tests = "/gnome-source/medusa/doc/medusa-smoke-tests";
my $test_data_file = "/gnome-source/medusa/tests/search-uri-test-data";


open TESTS, $test_data_file or die "Can't open the test file $test_data_file\n";


my %command_line_options = ( "--help" => spew_command_line_options);
my %command_line_explanations = ( "--help" => "Get this information and exit");


sub spew_command_line_options {
    my $option;
    print STDERR "The available command line options for $0 are\n";
    foreach $option (keys %command_line_explanations) {
	print STDERR "$option\t$command_line_explanations{$option}\n";
    }
    exit 1;
}


sub do_reindexing {
    print "Executing medusa-indexd -f $directory_to_index_for_tests -n test-index\n";
    system ("medusa-indexd -f $directory_to_index_for_tests -n test-index > test-index-log");
}



sub deal_with_command_line_options 
{
    for $argument (@ARGV) {
	if (!defined $command_line_options{$argument}) {
	    print STDERR "Unknown command line option $argument\n";
	    exit 1;
	}
	eval $command_line_options{$argument} ;
    }
}

sub got_a_daemon_error 
{
    my $output = shift;
    if (defined $output and $output =~ /The search service/) {
	return 1;
    }
    return 0;
}

sub process_error_and_exit
{
    my $uri_before_error = shift;
    my $error_output_received = shift;
    print STDERR "The following error occurred while running the test for $uri_before_error\n";
    print STDERR $error_output_received,"\n";
    exit 1;
}

sub check_that_output_matches_expected_answers
{
    my $test_uri = shift;
    my @expected_answers = sort @_;
    my $success = 1;

    $#answers_we_got = -1;
    while (<MSEARCH_OUTPUT>) {
	chomp;
	push @answers_we_got, $_;
    }
#    print "we got $#answers_we_got answers\n";
    if (got_a_daemon_error ($answers_we_got[0])) {
	process_error_and_exit ($answers_we_got[0]);
    }
    print "\nTest $test_uri\n";
    # check all the answers we expected showed up
    foreach  $answer (@expected_answers) {
	if ((grep /^$answer$/, @answers_we_got) == 0) {
	    print "Correct result unreturned: $answer\n";
	    $success = 0;
	}
    }
    # now check that all the answers that showed up were expected ones
    foreach  $answer (@answers_we_got) {
	if ((grep /^$answer$/, @expected_answers) == 0) {
	    print "Invalid result returned: $answer\n";
	    $success = 0;
	}
    }
    if ($success) {
	print "Test succeeded\n\n";
    }
}


deal_with_command_line_options;
print "Doing reindexing\n";
do_reindexing;
print "Starting search daemon\n";
system ("rm /tmp/medusa-search-server-test-index");
system ("medusa-searchd -n test-index > test-search-log");

print "Running tests\n";
$get_search_results = 0;
while (<TESTS>) {
    chomp;

    # If we hit a blank line and we're getting search results, we're done; run the tests
    if ($_ eq "" && $get_search_results) {
	open MSEARCH_OUTPUT, "export MEDUSA_INDEX_NAME=test-index && /gnome/bin/msearch \"$test_search_uri\" |" or process_error_and_exit ($test_search_uri, "Error running msearch\n");
	check_that_output_matches_expected_answers ($test_search_uri,
						    @expected_answers);
	$test_search_uri = "";
	$#expected_answers = -1;
	$get_search_results = 0;
	next;
    }
    if ($_ eq "") {
	next;
    }
    if ($get_search_results) {
	push @expected_answers, $_;
    }
    else {
	$test_search_uri = $_;
	$get_search_results = 1;
    }
    
}

