/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  Copyright (C) 2000 Eazel, Inc., 2001 Rebecca Schulman
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@eazel.com>
 *
 *  medusa-test.c: Runs all of the class test functions
 *
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <glib.h>
#include <libgnomevfs/gnome-vfs-init.h>

#include <libmedusa/medusa-file-info-utilities.h>
#include <libmedusa/medusa-index-filenames.h>
#include <libmedusa/medusa-lock.h>
#include <libmedusa/medusa-log.h>
#include <libmedusa/medusa-stdio-extensions.h>
#include <libmedusa-internal/medusa-debug.h>
#include <libmedusa-internal/medusa-extensible-hash.h>
#include <libmedusa-internal/medusa-lexicon.h>
#include <libmedusa-internal/medusa-text-index.h>


/* If you create a new test harness, add it here, after setup_tests.
   If your tests require external setup, you can add the setup
   function to setup_tests. */
int main ()
{
        medusa_make_warnings_and_criticals_stop_in_debugger
                (G_LOG_DOMAIN,
		 "GLib",
		 "GLib-GObject",
		 "GModule",
		 "GThread",
                 "GnomeVFS",
                 "Medusa",
                 NULL);

        gnome_vfs_init ();

        medusa_log_setup ();

        medusa_index_filenames_self_check ();
        medusa_lock_self_check ();
        medusa_log_self_check ();
        medusa_file_info_utilities_self_check ();
        medusa_extensible_hash_self_check ();
        medusa_lexicon_self_check ();
        medusa_stdio_extensions_self_check ();
        medusa_text_index_self_check ();

        /* FIXME: These tests no longer work */
#if 0
        medusa_text_index_mime_module_test ();
#endif
        return 0;
}

