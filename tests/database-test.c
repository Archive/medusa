
#include <medusa-file-index.h>
#include <glib.h>
#include <libgnomevfs/gnome-vfs-init.h>
#include <unistd.h>
#include <medusa-byte.h>
#include <sys/types.h>
#include <medusa-conf.h>

int main ()
{
  MedusaFileSystemDB *file_system_db;
  /* struct poptOption options[] = {}; */
  char *record;
  char file_name[3], directory_name[2];
  uid_t owner_bytes;
  time_t mtime_bytes;
  int i;

  
  gnome_vfs_init();
  file_system_db = medusa_file_system_db_initialize (FILE_SYSTEM_DB_FILE,
						     ROOT_DIRECTORY,
						     DIRECTORY_NAME_HASH,
						     FILE_NAME_HASH,
						     MIME_TYPE_HASH);
  for (; ;) {
    /* Get a record number */
    scanf ("%d", &i);
    record = medusa_relational_database_record_number_to_record (file_system_db->file_database, i);
    memcpy (file_name, record, 3);
    memcpy (directory_name, &record[3], 2);
    printf ("File name is %s\n", medusa_token_to_string (file_system_db->file_names, medusa_bytes_to_token (file_name,
													  MEDUSA_FILE_INDEX_FILENAME_FIELD_SIZE)));
    printf ("Directory name is %s\n", medusa_token_to_string (file_system_db->directory_names, medusa_bytes_to_token (directory_name,
														    MEDUSA_FILE_INDEX_DIRECTORY_NAME_FIELD_SIZE)));
    memcpy (&owner_bytes, medusa_relational_database_record_get_field_by_title (record, 
										file_system_db->file_database->file->field_info,
										MEDUSA_FILE_INDEX_OWNER_FIELD_TITLE),
	    MEDUSA_FILE_INDEX_OWNER_FIELD_SIZE);
    memcpy (&mtime_bytes, medusa_relational_database_record_get_field_by_title (record,
										file_system_db->file_database->file->field_info,
										MEDUSA_FILE_INDEX_MTIME_FIELD_TITLE),
	    MEDUSA_FILE_INDEX_MTIME_FIELD_SIZE);
    printf ("Owner is 0x%x\n", (uid_t) owner_bytes);
    printf ("Mod time is 0x%lu\n", (time_t) mtime_bytes);
  }
  
  exit (1);
  return 1;
}
