/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  Copyright (C) 2000 Eazel, Inc., 2002 Rebecca Schulman
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@caltech.edu>
 * 
 *  medusa-log.c  -- API for logging errors and progress, depending on the
 *  current log level
 */

#include <config.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <glib.h>
#include <libgnome/gnome-util.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "medusa-log.h"
#include "medusa-stdio-extensions.h"
#include "medusa-string.h"
#include "medusa-test.h"


#define MEDUSA_LOG_DEBUG 1

/* FIXME: Log messages should also log the time of the event */
#define LOG_FILE_PATH ".medusa"
#define LOG_FILE "log"

static FILE *log_file = NULL;
static gboolean log_file_setup_succeeded = FALSE;

static MedusaLogLevel log_level;
static gboolean determined_log_level_from_environment = FALSE;

/* FIXME: These should include index names */
static char *
get_log_file_path (void)
{
        return g_strdup_printf ("%s/%s", g_get_home_dir (), LOG_FILE_PATH);
}

static char *
get_log_file_name (void)
{
        return g_strdup_printf ("%s/%s/%s", 
                                g_get_home_dir (), LOG_FILE_PATH, LOG_FILE);
}

static gboolean
medusa_log_setup_internal (void)
{
        char *log_file_name, *log_file_path;

        log_file_path = get_log_file_path ();
        
        /* First make sure the log file is ready for writing. */
        if (access (log_file_path, W_OK) != 0) {
                if (access (log_file_path, X_OK) == 0) {
                        if (!medusa_make_file_writeable (log_file_path)) {
                                g_warning ("Could not write to "
                                           "log file directory %s.  "
                                           "Errno is %d.\n",
                                           log_file_path, errno);
                                g_free (log_file_path);
                                return FALSE;
                        }
                }
                if (mkdir (log_file_path, S_IRWXU) != 0) {
                        g_warning ("Could not create log file "
                                   "directory %s.  "
                                   "Errno is %d\n",
                                   log_file_path,
                                   errno);
                        g_free (log_file_path);
                        return FALSE;
                        }
        }

        g_free (log_file_path);

        log_file_name = get_log_file_name ();
        log_file = fopen (log_file_name, "a");
        
        if (log_file == NULL) {
                g_warning ("Opening the log file failed.  "
                           "Log file name is %s.  "
                           "Errno is %d\n", 
                           log_file_name, errno);
                g_free (log_file_name);
                return FALSE;
        }

        g_free (log_file_name);
        log_file_setup_succeeded = TRUE;
        return TRUE;
}

gboolean
medusa_log_setup (void)
{
        gboolean setup_succeeded;
        char *log_file_name;

        setup_succeeded = medusa_log_setup_internal ();

        if (!setup_succeeded) {
                log_file_name = get_log_file_name ();
                g_warning ("Medusa's log file %s could not be opened.  "
                           "As a result, messages will not be logged, "
                           "and will instead be displayed as terminal "
                           "warnings.", log_file_name);
                g_free (log_file_name);
        }

        return setup_succeeded;
}

MedusaLogLevel
medusa_log_get_current_log_level (void)
{
        if (!determined_log_level_from_environment) {
                if (getenv ("MEDUSA_INDEX_DEBUG") != NULL) {
                        log_level = MEDUSA_DB_LOG_EVERYTHING;
                }
                else if (getenv ("MEDUSA_INDEX_LOG_ABBREVIATED") != NULL) {
                        log_level = MEDUSA_DB_LOG_ABBREVIATED;
                }
                else if (getenv ("MEDUSA_INDEX_LOG_NOTHING") != NULL) {
                        log_level = MEDUSA_DB_LOG_NOTHING;
                }
                else if (getenv ("MEDUSA_INDEX_LOG_TEXT_INDEX_DATA") != NULL) {
                        log_level = MEDUSA_DB_LOG_TEXT_INDEX_DATA;
                }
                else log_level = MEDUSA_DB_LOG_ERRORS;
        }
        
        return log_level;
}

void
medusa_log_enable_index_accounting (void)
{
        gnome_unsetenv ("MEDUSA_INDEX_LOG_DEBUG");
        gnome_unsetenv ("MEDUSA_INDEX_LOG_ABBREVIATED");
        gnome_unsetenv ("MEDUSA_INDEX_LOG_NOTHING");
        
        gnome_setenv ("MEDUSA_INDEX_LOG_TEXT_INDEX_DATA", "1", 0);
}

static char *
current_time_as_string (void)
{
        time_t current_time;
        
        current_time = time (NULL);
        return asctime (localtime (&current_time));
        

}

static void
medusa_log_error_arg_list (const char  *message,
                           va_list      extra_argument_list)
{
        char *formatted_message;
        char *complete_error_message, *time_string;
        
        if (medusa_log_get_current_log_level () >= MEDUSA_DB_LOG_ERRORS) {
                formatted_message = g_strdup_vprintf (message, extra_argument_list);
                time_string = current_time_as_string ();
                complete_error_message = g_strdup_printf ("%s (Errno = %d) on %s\n", formatted_message, errno, time_string);
                g_free (formatted_message);
                if (log_file_setup_succeeded) {
                        fprintf (log_file, complete_error_message);
                }
                else {
                        g_warning (complete_error_message);
                }
#ifdef MEDUSA_LOG_DEBUG
                g_warning (complete_error_message);
#endif
                g_free (complete_error_message);
        }
}

void
medusa_log_error (const char *message,
                  ...)
{
        va_list extra_argument_list;


        g_return_if_fail (message != NULL);

        va_start (extra_argument_list, message);
        medusa_log_error_arg_list (message, extra_argument_list);
}

void
medusa_log_fatal_error (const char *message,
                        ...)
{
        va_list extra_argument_list;

        va_start (extra_argument_list, message);
        medusa_log_error_arg_list (message,
                                   extra_argument_list);
        medusa_log_error ("Above error is fatal.  Exiting now");
        exit (1);
}

void
medusa_log_event (MedusaLogLevel   minimum_log_level,
                  const char      *message,
                  ...)
{
        va_list extra_argument_list;
        char *formatted_message;
        char *time_string, *complete_error_message;


        g_return_if_fail (message != NULL);

        if (medusa_log_get_current_log_level () >= minimum_log_level) {
                va_start (extra_argument_list, message);

                formatted_message = g_strdup_vprintf (message, extra_argument_list);
                time_string = current_time_as_string ();
                complete_error_message = g_strdup_printf ("%s at %s", formatted_message, time_string);
                g_free (formatted_message);
                if (log_file_setup_succeeded) {
                        fprintf (log_file, complete_error_message);
                }
                else {
                        g_message (complete_error_message);
                }

#ifdef MEDUSA_LOG_DEBUG
                g_message (message);
#endif

                g_free (complete_error_message);
        }
}

void
medusa_log_text_index_account_data (const char *message)
{
        if (medusa_log_get_current_log_level () == MEDUSA_DB_LOG_TEXT_INDEX_DATA) {
                if (log_file_setup_succeeded) {
                        fprintf (log_file, message);
                }
                else {
                        g_message (message);
                }

#ifdef MEDUSA_LOG_DEBUG
                g_message (message);
#endif
        }
}

void
medusa_close_log_file_on_exit (void)
{
        if (log_file) {
                fclose (log_file);
        }
}

static void 
log_file_names_self_check ()
{
        char *log_path, *log_file_name;

        log_path = get_log_file_path ();
        log_file_name = get_log_file_name ();
        
        /* Test that the log name is in the log path */
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_str_has_prefix 
                                    (log_file_name, log_path));

        /* We'll test that the log file is in the user's
           homedirectory, in the medusa subdir, and that it's
           readable, or if it doesn't exist can be made. */
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_str_has_prefix 
                                    (log_file_name, g_get_home_dir ()));
        
        /* If the log already exists: */
        if (access (log_file_name, X_OK) == 0) {
                MEDUSA_TEST_SYSTEM_CALL (access (log_file_name, R_OK));
        }

        g_free (log_file_name);
        g_free (log_path);
}

void
medusa_log_self_check (void)
{
        log_file_names_self_check ();
}
