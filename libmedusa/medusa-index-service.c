/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 * 
 *  Copyright (C) 2000 Eazel, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Authors: Rebecca Schulman <rebecka@eazel.com>
 *  
 */


/* medusa-index-service.c -- API for externals users of the medusa
   indexing service. */

#include <config.h>
#include "medusa-index-filenames.h"
#include "medusa-index-service.h"
#include "medusa-index-service-private.h"
#include "medusa-index-progress.h"
#include "medusa-indexed-search.h"
#include "medusa-unindexed-search.h"
#include "medusa-log.h"
#include "medusa-service-private.h"
#include "medusa-utils.h"

#include <glib.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#define SYSTEM_STATE_POLL_INTERVAL 3000

GConfClient *get_gconf_client (void);

typedef struct UpdateClosure {
	MedusaSystemStateFunc update;
	gpointer update_argument;
	int update_function_id;
	gboolean execute_only_once;
} UpdateClosure;

typedef struct SystemStatePoll {
	gboolean was_enabled_last_time;
	GList *updates;
	int check_for_state_changes_timeout_id;
} SystemStatePoll;

static SystemStatePoll *system_state_poll;
static int current_function_id;
static GConfClient *gc_client;

GConfClient *
get_gconf_client (void)
{
	if (NULL == gc_client) {
		gc_client = gconf_client_get_default ();
	}

	return gc_client;
}


/**
 * medusa_index_service_get_last_index_update_time
 * returns the value of gconf key /apps/medusa/last-index-time expressed as
 * the time since the epoch that the index was last updated.
 *
 * returns time_t since the epoch or 0 if there is no index.
 **/
time_t
medusa_index_service_get_last_index_update_time (void)
{
	GConfClient *gc_client;
	const gchar *key = "/apps/medusa/last-index-time";
	GError *err = NULL;

	gchar *time_value;
	long seconds;
	gboolean success;

	gc_client = get_gconf_client ();
	time_value = gconf_client_get_string (gc_client, key, &err);

	if (NULL != err) {
		g_error (err->message);
		g_error_free (err);
	}

	success = sscanf (time_value, "%ld", &seconds);

	if (!success) {
		seconds = 0;
	}

	g_free (time_value);

	return (time_t) seconds;
}


/**
 * medusa_index_service_set_last_index_update_time
 * updated the value of gconf key /apps/medusa/last-index-time expressed as
 * the time since the epoch that the index was last updated.
 *
 * params time_t seconds since the epoch.
 **/
void
medusa_index_service_set_last_index_update_time (time_t seconds)
{
	GConfClient *gc_client;
	const gchar *key = "/apps/medusa/last-index-time";
	GError *err = NULL;

	gchar *time_value;
	gboolean success;

	time_value = g_strdup_printf ("%ld", seconds);

	gc_client = get_gconf_client ();
	success = gconf_client_set_string (gc_client, key, time_value, &err);
	g_free (time_value);

	if (NULL != err) {
		g_error (err->message);
		g_error_free (err);
	}
}


/**
 * medusa_index_service_is_enabled
 * returns the value of gconf key /apps/medusa/indexer-enabled.
 *
 * returns true if medusa-indexd is configured to index.
 **/
gboolean
medusa_index_service_is_enabled (void)
{
	GConfClient *gc_client;
	const gchar *key = "/apps/medusa/indexer-enabled";
	GError *err = NULL;

	gboolean is_enabled = TRUE;

	gc_client = get_gconf_client ();
	is_enabled = gconf_client_get_bool (gc_client, key, &err);

	if (NULL != err) {
		g_error (err->message);
		g_error_free (err);
	}

	return is_enabled;
}


/**
 * medusa_index_service_get_default_index_root
 * returned the default index root used when medusa_indexd is not passed there
 * -f option.
 *
 * returns the file path of the root directory
 **/
gchar *
medusa_index_service_get_default_index_root (void)
{
	GConfClient *gc_client;
	const gchar *key = "/apps/medusa/default-index-root";
	GError *err = NULL;

	gchar *local_path;
	gchar *default_path;

	gc_client = get_gconf_client ();
	local_path = gconf_client_get_string (gc_client, key, &err);

	if (NULL != err) {
		g_error (err->message);
		g_error_free (err);
	}

	default_path = gnome_vfs_expand_initial_tilde (local_path);
	g_free (local_path);

	return default_path;
}


/**
 * medusa_index_service_get_file_index_stoplist
 * returns the list of URIs that the indexer will not index
 *
 * returns GList of URIs
 **/
GSList *
medusa_index_service_get_file_index_stoplist (void)
{
	GConfClient *gc_client;
	const gchar *key = "/apps/medusa/file-index-stoplist";
	GError *err = NULL;

	GSList *file_index_stoplist;

	gc_client = get_gconf_client ();
	file_index_stoplist = gconf_client_get_list (gc_client, key,
						     GCONF_VALUE_STRING,
						     &err);

	if (NULL != err) {
		g_error (err->message);
		g_error_free (err);
	}

	if (NULL == file_index_stoplist) {
		g_warning ("file_index_stoplist is null.");
	}

	return file_index_stoplist;
}


/**
 * medusa_index_service_get_mount_type_stoplist
 * returns the list of URIs that the indexer will not index
 *
 * returns GList of filesystem types
 **/
GSList *
medusa_index_service_get_mount_type_stoplist (void)
{
	GConfClient *gc_client;
	const gchar *key = "/apps/medusa/mount-type-stoplist";
	GError *err = NULL;

	GSList *mount_type_stoplist;

	gc_client = get_gconf_client ();
	mount_type_stoplist = gconf_client_get_list (gc_client, key,
						     GCONF_VALUE_STRING,
						     &err);

	if (NULL != err) {
		g_error (err->message);
		g_error_free (err);
	}

	if (NULL == mount_type_stoplist) {
		g_warning ("mount_type_stoplist is null.");
	}

	return mount_type_stoplist;
}

/**
 * medusa_index_service_get_index_percentage_complete
 * returns the percentage completed by the indexing process.
 *
 * returns int representing the percentage.
 **/
int
medusa_index_service_get_index_percentage_complete (void)
{
	return medusa_index_progress_get_percentage_complete ();
}

/**
 * medusa_index_service_indexing_is_currently_in_progress
 * return true if indexing is in process.
 *
 * returns gboolean true if indexing is in process.
 **/
gboolean
medusa_index_service_indexing_is_currently_in_progress (void)
{
	return medusa_indexing_is_currently_in_progress ();
}

/**
 * medusa_index_service_indexed_search_is_available
 * return true if an index was created.
 *
 * returns gboolean true if an index was created.
 **/
gboolean
medusa_index_service_indexed_search_is_available (void)
{
	time_t seconds;

	seconds = medusa_index_service_get_last_index_update_time ();

	if (0 == seconds) {
		return FALSE;
	}

	return TRUE;
}

/**
 * medusa_index_service_index_files_exist
 * return true if the default index files exist.
 *
 * returns gboolean true the default index files exist.
 **/
gboolean
medusa_index_service_index_files_exist (void)
{
	return medusa_indexed_search_system_index_files_look_available ();
}

/**
 * medusa_index_service_unindexed_search_is_available_for_uri
 * return true if the search URI only queryied filesystem information.
 * Content searches require an index.
 *
 * params uri const char * the seach uri.
 *
 * returns gboolean true if the search URI only queryied filesystem information.
 **/
gboolean
medusa_index_service_unindexed_search_is_available_for_uri (const char
							    *uri)
{
	GnomeVFSResult result;

	result = medusa_unindexed_search_is_available_for_uri (uri);

	if (result == GNOME_VFS_OK) {
		return TRUE;
	}

	return FALSE;
}

static void
call_state_changed_callback (gpointer list_data, gpointer callback_data)
{
	UpdateClosure *update_closure;

	g_assert (list_data != NULL);
	update_closure = (UpdateClosure *) list_data;

	update_closure->update (update_closure->update_argument);
}


static gboolean
is_a_onetime_callback (gpointer list_data, gpointer callback_data)
{
	UpdateClosure *update_closure;

	g_assert (list_data != NULL);

	update_closure = (UpdateClosure *) list_data;

	return update_closure->execute_only_once;
}


static gboolean
check_for_system_state_changes (gpointer data)
{
	gboolean now_enabled;

	now_enabled = medusa_index_service_is_enabled ();

	if (now_enabled != system_state_poll->was_enabled_last_time) {
		g_list_foreach (system_state_poll->updates,
				call_state_changed_callback, NULL);
		system_state_poll->updates =
		    medusa_g_list_remove_deep_custom
		    (system_state_poll->updates, is_a_onetime_callback,
		     g_free, NULL);
		system_state_poll->was_enabled_last_time = now_enabled;
	}

	return TRUE;
}


static int
medusa_setup_callback_for_system_state_change (MedusaSystemStateFunc
					       callback,
					       gpointer callback_data,
					       gboolean
					       execute_callback_only_once)
{
	UpdateClosure *callback_closure;

	if (system_state_poll == NULL) {
		system_state_poll = g_new0 (SystemStatePoll, 1);
		system_state_poll->was_enabled_last_time =
		    medusa_index_service_is_enabled ();
		system_state_poll->check_for_state_changes_timeout_id =
		    g_timeout_add (SYSTEM_STATE_POLL_INTERVAL,
				   check_for_system_state_changes,
				   system_state_poll);
	}

	callback_closure = g_new0 (UpdateClosure, 1);
	callback_closure->update = callback;
	callback_closure->update_argument = callback_data;
	callback_closure->update_function_id = ++current_function_id;
	callback_closure->execute_only_once = execute_callback_only_once;

	system_state_poll->updates =
	    g_list_prepend (system_state_poll->updates, callback_closure);

	return callback_closure->update_function_id;
}


int
medusa_execute_when_system_state_changes (MedusaSystemStateFunc callback,
					  gpointer callback_data)
{
	return medusa_setup_callback_for_system_state_change (callback,
							      callback_data,
							      FALSE);
}


int
medusa_execute_once_when_system_state_changes (MedusaSystemStateFunc
					       callback,
					       gpointer callback_data)
{
	return medusa_setup_callback_for_system_state_change (callback,
							      callback_data,
							      TRUE);
}


static gboolean
has_update_id (gpointer list_data, gpointer callback_data)
{
	UpdateClosure *update_closure;
	int function_id_to_match;

	g_assert (list_data != NULL);

	update_closure = (UpdateClosure *) list_data;
	function_id_to_match = GPOINTER_TO_INT (callback_data);

	return (update_closure->update_function_id ==
		function_id_to_match);
}


static void
system_state_poll_destroy_and_set_to_null (void)
{
	g_assert (system_state_poll->updates == NULL);

	// gtk2 docs say the code below is unnecessary
	//gtk_timeout_remove (system_state_poll->check_for_state_changes_timeout_id);
	g_free (system_state_poll);
	system_state_poll = NULL;
}


void
medusa_remove_state_changed_function (int update_function_id)
{
	if (system_state_poll == NULL) {
		return;
	}

	system_state_poll->updates =
	    medusa_g_list_remove_deep_custom (system_state_poll->updates,
					      has_update_id, g_free,
					      GINT_TO_POINTER
					      (update_function_id));
	if (system_state_poll->updates == NULL) {
		system_state_poll_destroy_and_set_to_null ();
	}
}
