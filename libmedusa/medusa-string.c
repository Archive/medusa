/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  Copyright (C) 2000 Eazel, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@eazel.com>
 *
 *  medusa-string.c -- String functions written or copied
 *  for use in medusa
 *
 */

#include <glib.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "medusa-string.h"

gboolean
medusa_str_has_prefix (const char *string,
                       const char *prefix)
{
        g_return_val_if_fail (string != NULL, FALSE);
        g_return_val_if_fail (prefix != NULL, FALSE);

        return strlen (prefix) <= strlen (string) &&
                strncmp (string, prefix, strlen (prefix)) == 0;
}


gboolean
medusa_str_has_suffix (const char *string,
                       const char *suffix)
{
        g_return_val_if_fail (string != NULL, FALSE);
        g_return_val_if_fail (suffix != NULL, FALSE);

        return strlen (suffix) <= strlen (string) &&
                strcmp (string + strlen (string) - strlen (suffix), 
                        suffix) == 0;
}

gboolean       
medusa_strcase_has_prefix (const char                     *string,
			   const char                     *prefix)
{
        g_return_val_if_fail (string != NULL, FALSE);
        g_return_val_if_fail (prefix != NULL, FALSE);

        return strlen (prefix) <= strlen (string) &&
                strncasecmp (string, prefix, strlen (prefix)) == 0;
}

gboolean
medusa_strcase_has_suffix (const char *string,
			   const char *suffix)
{
        g_return_val_if_fail (string != NULL, FALSE);
        g_return_val_if_fail (suffix != NULL, FALSE);
  
        return strlen (suffix) <= strlen (string) &&
                strcasecmp (string + strlen (string) - strlen (suffix), 
                            suffix) == 0;
}



gboolean       
medusa_strstr_case_insensitive  (const char                     *string,
                                 const char                     *substring)
{
        const char *string_location;
        int possible_prefix_locations;

        g_return_val_if_fail (string != NULL, FALSE);
        g_return_val_if_fail (substring != NULL, FALSE);
        
        if (strlen (substring) <= strlen (string)) {
                possible_prefix_locations = strlen (string) - strlen (substring);
                for (string_location = string;
                     possible_prefix_locations >= 0;
                     possible_prefix_locations--,
                             string_location++) {
                        if (medusa_strcase_has_prefix (string_location,
                                                       substring)) {
                                return TRUE;
                        }
                }
        }

        return FALSE;
}


/* medusa_n_byte_test_string_new
 *
 * Returns a string of arbitrary content of length n, that must be
 * freed by the caller.  0 is an acceptable length and returns the empty
 * string (""). 
 *
 * @desired_length  The length the arbitrary string should be
 *
 * @Return value    The arbitrary string.
 */
char *
medusa_fixed_length_test_string_new (int desired_length)
{
        char *string;

        g_return_val_if_fail (desired_length >= 0, NULL);

        string = g_new (char, desired_length + 1);
        string [desired_length] = 0;
        while (desired_length > 0) {
                string[desired_length - 1] = 'g';
                desired_length--;
        }
        
        return string;
}

#define MAXIMUM_LENGTH_FOR_RANDOM_WORD 20

char *
medusa_random_word (void)
{
        int i, length, rand_char_value;
        char *word;
        
        length = rand () % MAXIMUM_LENGTH_FOR_RANDOM_WORD + 1;
        word = g_new (char, length + 1);
        for (i = 0; i < length; i++) {
                rand_char_value = rand () % 36;
                if (rand_char_value < 10) {
                        /* A digit */
                        word[i] = rand_char_value + 48;

                }
                else if (rand_char_value < 35) {
                        /* A letter */
                        word[i] = rand_char_value + 87;
                }
                else {
                        word[i]='_';
                }
        }
        word[i] = 0;

        return word;
}

/* FIXME: Need to move code around so that the string functions can have their
   own self check functions */
