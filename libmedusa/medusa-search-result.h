/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 * 
 *  Copyright (C) 2001 Rebecca Schulman
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Authors: Rebecca Schulman <rebecka@ai.mit.edu>
 *  
 */

#ifndef MEDUSA_SEARCH_RESULT_H
#define MEDUSA_SEARCH_RESULT_H

typedef enum {
        MEDUSA_OK,
        MEDUSA_ERROR_SERVICE_NOT_ENABLED,
        MEDUSA_ERROR_NOT_FOUND,
        MEDUSA_ERROR_DAEMON_NOT_FOUND,
        MEDUSA_ERROR_DAEMON_NOT_RESPONDING,
        MEDUSA_ERROR_DAEMON_WONT_ACCEPT_DATA,
        MEDUSA_ERROR_DAEMON_ERROR_INTERNAL,
        MEDUSA_ERROR_DAEMON_COMMUNICATION_FAILURE,
        MEDUSA_ERROR_INVALID_URI,
        MEDUSA_ERROR_TOO_BIG,
	MEDUSA_ERROR_SERVICE_OBSOLETE,
	MEDUSA_ERROR_DETAILS_UNKNOWN
        
} MedusaSearchResult;


GnomeVFSResult                 medusa_search_result_to_gnome_vfs_result (MedusaSearchResult indexed_search_result);
MedusaSearchResult             gnome_vfs_result_to_medusa_search_result (GnomeVFSResult     gnome_vfs_result);

#endif /* MEDUSA_SEARCH_RESULT_H */
