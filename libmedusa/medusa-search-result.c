/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 * 
 *  medusa-search-result.c - code for reporting the results of opening 
 *  medusa indexes or doing a slow unindexed search
 *
 *  Copyright (C) 2001 Rebecca Schulman
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Authors: Rebecca Schulman <rebecka@eazel.com>
 *  
 */

#include <libgnomevfs/gnome-vfs-types.h>

#include "medusa-search-result.h"

GnomeVFSResult 
medusa_search_result_to_gnome_vfs_result (MedusaSearchResult search_result)
{
        switch (search_result) {
        case MEDUSA_OK:
                return GNOME_VFS_OK;
        case MEDUSA_ERROR_SERVICE_NOT_ENABLED:
        case MEDUSA_ERROR_NOT_FOUND:
        case MEDUSA_ERROR_DAEMON_NOT_RESPONDING:
        case MEDUSA_ERROR_DAEMON_WONT_ACCEPT_DATA:
        case MEDUSA_ERROR_DAEMON_ERROR_INTERNAL:
                return GNOME_VFS_ERROR_SERVICE_NOT_AVAILABLE;
        case MEDUSA_ERROR_INVALID_URI:
                return GNOME_VFS_ERROR_INVALID_URI;
        case MEDUSA_ERROR_TOO_BIG:
                return GNOME_VFS_ERROR_TOO_BIG;
        default:
                g_assert_not_reached ();
                return GNOME_VFS_ERROR_INTERNAL;
        }
        
}

MedusaSearchResult 
gnome_vfs_result_to_medusa_search_result (GnomeVFSResult gnome_vfs_result)
{
  switch (gnome_vfs_result) {
  case GNOME_VFS_OK:
    return MEDUSA_OK;
  case GNOME_VFS_ERROR_INVALID_URI:
    return MEDUSA_ERROR_INVALID_URI;
  case GNOME_VFS_ERROR_TOO_BIG:
    return MEDUSA_ERROR_TOO_BIG;
  case GNOME_VFS_ERROR_SERVICE_NOT_AVAILABLE:
  default:
    return MEDUSA_ERROR_DETAILS_UNKNOWN;
  }
}
