/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  Copyright (C) 2000 Eazel, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@eazel.com>
 *
 *  medusa-stdio-extensions.h - Functions to do standard tasks with local I/O
 */


#ifndef MEDUSA_STDIO_EXTENSIONS_H
#define MEDUSA_STDIO_EXTENSIONS_H

#include <sys/stat.h>
#include <sys/types.h>

/* Reads a whole file into a new buffer.
   Returns the number of bytes read, 0 if the file
   is empty, and -1 if an error occurs */
int             medusa_read_whole_file       (const char *file_name,
                                              char **file_contents);

/* Read the size of the file, -1 if the file does not exist or there is
   an error */
off_t           medusa_get_file_size         (const char *file_name);

gboolean        medusa_create_file           (const char *file_name);

/* Removes file if it exists, NO-OP if file does not exist */
gboolean        medusa_unlink_if_present     (const char *file_name);

/* Read a single line from a non-interactive file stream.
   Return a malloc'ed string, similarly to readline.
   If EOF is encountered and no characters have been read, line will be NULL.
   fgetc does not differentiate between EOF and error conditions, so now
   error reporting is done.
*/
char *          medusa_readline_from_stream  (FILE       *stream);

gboolean        medusa_make_file_writeable   (const char *filename);
gboolean        medusa_create_directory_recursive (const char *index_name,
                                                   mode_t      mode);

/* These functions perform the standard I/O function.
   They return TRUE if the operation succeeded, FALSE otherwise.
   (except for the fopen cover, which returns either a valid FILE *
   or NULL)
   If an error occurs, the file name and errno are logged to
   the medusa log file */
gboolean        medusa_chmod_error_cover     (const char *path,
                                              mode_t      mode);
FILE *          medusa_fopen_error_cover     (const char  *path,
                                              const char  *mode);

gboolean        medusa_fseek_error_cover     (FILE        *stream,
                                              long         offset,
                                              int          whence,
                                              const char  *file_name);
gboolean        medusa_fread_error_cover     (void        *buffer,
                                              size_t       item_size,
                                              size_t       number_of_items,
                                              FILE        *stream,
                                              const char  *file_name,
                                              gboolean     partial_read_is_an_error);
gboolean        medusa_fwrite_error_cover    (const void  *buffer,
                                              size_t       item_size,
                                              size_t       number_of_items,
                                              FILE        *stream,
                                              const char  *file_name);
gboolean        medusa_fclose_error_cover    (FILE        *stream,
                                              const char  *file_name);
gboolean        medusa_mkdir_error_cover     (const char  *path,
                                              mode_t       mode);
gboolean        medusa_rename_error_cover    (const char  *old_path_name,
                                              const char  *new_path_name);
gboolean        medusa_stat_error_cover      (const char *file_name,
                                              struct stat *stat_buffer);

void            medusa_stdio_extensions_self_check (void);
#endif /* MEDUSA_STDIO_EXTENSIONS_H */
