/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  Copyright (C) 2000, 2001 Eazel, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@eazel.com>
 *
 *  medusa-unsearched-locations.c -- Way to specify files and directories to
 *  skip, like nfs mounts, removeable media, and other files we specifically aren't
 *  interested in.
 */

#include <config.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include "medusa-log.h"
#include "medusa-utils.h"
#include "medusa-index-service.h"
#include "medusa-index-filenames.h"

#ifdef HAVE_GETMNTINFO
# undef MAX
# undef MIN
# include <sys/param.h>
# include <sys/ucred.h>
# include <sys/mount.h>
#elif (HAVE_SYS_MNTTAB_H)
#include <sys/mnttab.h>
#else
#if defined (HAVE_GETMNTENT)
# include <mntent.h>
#else
#warning "Can't find a valid mount function type"
#endif
#endif

#include <glib.h>
#include <libgnomevfs/gnome-vfs-utils.h>

#include "medusa-unsearched-locations.h"

#define CDROM_DRIVE_STATUS	0x5326  /* Get tray position, etc. */
#define CDSL_CURRENT    	((int) (~0U>>1))

static void                 file_stoplist_initialize                          (void);
static void                 unsearched_mount_list_initialize                  (void);
static void                 medusa_unsearched_locations_shutdown              (void);
static gint                 find_glob_match                                   (gconstpointer pattern, gconstpointer string);
static void                  append_unsearched_locations                      (const char*);

#ifdef HAVE_GETMNTINFO
static gboolean             mount_point_is_cdrom                              (const struct statfs *fs_list);
#elif HAVE_SYS_MNTTAB_H
static  gboolean            mount_point_is_cdrom                              (const struct mnttab *mnttab_entry);
#elif defined(HAVE_GETMNTENT)
static gboolean             mount_point_is_cdrom                              (const struct mntent *mount_entry);
#endif

static GSList *unsearched_locations = NULL;
static GSList *mount_type_skip_list = NULL;



static gboolean
mount_type_is_in_skip_list (const char *mount_type)
{
        GSList *current_item;
        char *current_string;
       
        g_return_val_if_fail (mount_type != NULL, TRUE);

        for ( current_item = mount_type_skip_list ;
              current_item != NULL ;
              current_item = g_slist_next (current_item)) {

		current_string = (char *)current_item->data; 
                if (strcmp (current_string, mount_type) == 0) {
                        return TRUE;
                }
        }

        return FALSE;
}

gboolean
medusa_is_unsearched_location (const char *file_name)
{
	g_return_val_if_fail (unsearched_locations != NULL, FALSE);
	
        if (NULL != g_slist_find_custom (unsearched_locations, file_name, find_glob_match)) {
                return TRUE;
        }
        
	return FALSE;
}

static gint
find_glob_match (gconstpointer pattern, gconstpointer string)
{
	gboolean success;
	
	success = g_pattern_match_string ((GPatternSpec *) pattern, (gchar *) string);
	
	if (success) {
		return 0;
	}
	
	return -1;
}

void
medusa_unsearched_locations_initialize (void)
{
        static gboolean unsearched_locations_initialized = FALSE;

        if (!unsearched_locations_initialized) {
                file_stoplist_initialize ();
                unsearched_mount_list_initialize ();
		
		// convert strings to GPatternSpecs
		GSList *list_item;
	
		for (list_item = unsearched_locations ;
		      list_item != NULL ;
		      list_item = g_slist_next (list_item)) {
			GPatternSpec * pattern;
			pattern = g_pattern_spec_new (list_item->data);
			g_free (list_item->data);
			list_item->data = pattern;
		}
                
                g_atexit (medusa_unsearched_locations_shutdown);
                
                unsearched_locations_initialized = TRUE;
        }
}

static void
medusa_unsearched_locations_shutdown (void)
{
	GSList *list_item;

        for ( list_item = unsearched_locations ;
              list_item != NULL ;
              list_item = g_slist_next (list_item)) {

		g_free (list_item->data);
		list_item->data = NULL;
	}
	g_slist_free (unsearched_locations);
        unsearched_locations = NULL;
	
        for ( list_item = mount_type_skip_list ;
              list_item != NULL ;
              list_item = g_slist_next (list_item)) {

		g_free (list_item->data);
		list_item->data = NULL;
	}
	g_slist_free (mount_type_skip_list);
        mount_type_skip_list = NULL;
}

static void
file_stoplist_initialize  (void)
{
	unsearched_locations = medusa_index_service_get_file_index_stoplist ();    
}

static void
unsearched_mount_list_initialize_internal (void)
{

#ifdef HAVE_GETMNTINFO

	struct statfs *fs_list;
	int fs_count, i;

	fs_count = getmntinfo (&fs_list, MNT_LOCAL); /* returns the number of FSes. */
	for (i = 0; i < fs_count; ++i) {
		if (mount_type_is_in_skip_list (fs_list[i].f_fstypename)) {
			append_unsearched_locations (fs_list[i].f_mntonname);
		}
                if (mount_point_is_cdrom (&fs_list[i])) {
                        append_unsearched_locations (fs_list[i].f_mntonname);
                }
        }

#elif HAVE_SYS_MNTTAB_H

        FILE *mount_file;
        struct mnttab mnttab_entry;

        mount_file = fopen ("/etc/mnttab", "r");
        while (getmntent (mount_file, &mnttab_entry) != 0) {
                if (mount_type_is_in_skip_list (mnttab_entry.mnt_fstype)) {
			append_unsearched_locations (mnttab_entry.mnt_mountp);
                }
                if (mount_point_is_cdrom (&mnttab_entry)) {
                        append_unsearched_locations (mnttab_entry.mnt_mountp);
                }
        }
#ifdef HAVE_ENDMNTENT
        endmntent (mount_file);
#else
        fclose (mount_file);
#endif

#elif defined(HAVE_GETMNTENT)

        FILE *mount_file;
        struct mntent *mount_entry;
        
        mount_file = setmntent (MOUNTED, "r");
        while ((mount_entry = getmntent (mount_file)) != NULL) {
                if (mount_type_is_in_skip_list (mount_entry->mnt_type)) {
			append_unsearched_locations (mount_entry->mnt_dir);
                }
                if (mount_point_is_cdrom (mount_entry)) {
			append_unsearched_locations (mount_entry->mnt_dir);
                }
        }
	endmntent (mount_file);
        g_free (mount_entry);

#endif

}

static void
append_unsearched_locations (const char* path)
{
	GPatternSpec *pattern;
	char *uri;
	
	uri = g_strdup_printf ("%s*", gnome_vfs_get_uri_from_local_path (path));
	g_slist_append (unsearched_locations, g_strdup (uri));
	g_free (uri);
}

/* Don't index or search nfs mount points and removeable media, for now */
static void              
unsearched_mount_list_initialize (void)
{
	mount_type_skip_list = medusa_index_service_get_mount_type_stoplist ();
	unsearched_mount_list_initialize_internal ();
}

static gboolean
mount_point_is_cdrom_internal (const char *path)
{
	int fd;
        
        fd = open (path, O_RDONLY | O_NONBLOCK);
        
        /* These tests are shamelessly stolen from Gene Ragan's
         * nautilus-volume-monitor code in the nautilus module.
         */
        
        if (fd < 0) {
                return FALSE;
        }
        
	if (ioctl (fd, CDROM_DRIVE_STATUS, CDSL_CURRENT) < 0) {
                close (fd);
                return FALSE;
        }
        
        close (fd);
        
        return TRUE;
}

#ifdef HAVE_GETMNTINFO        

static gboolean             
mount_point_is_cdrom (const struct statfs *fs_list)
{
        if (strcmp (fs_list->f_fstypename, "cdrom")) {
                return FALSE;
        }
        return mount_point_is_cdrom_internal (fs_list->f_mntonname);
}

#elif HAVE_SYS_MNTTAB_H

static gboolean
mount_point_is_cdrom (const struct mnttab *mnttab_entry)
{
        if (strcmp (mnttab_entry->mnt_fstype, "iso9660")) {
                return FALSE;
        }
        return mount_point_is_cdrom_internal (mnttab_entry->mnt_mountp);
}

#elif defined(HAVE_GETMNTENT)

static gboolean
mount_point_is_cdrom (const struct mntent *mount_entry)
{
        if (strcmp (mount_entry->mnt_type, "iso9660")) {
                return FALSE;
        }

        return mount_point_is_cdrom_internal (mount_entry->mnt_fsname);
}

#endif
