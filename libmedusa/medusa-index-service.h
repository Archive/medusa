/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 * 
 *  Copyright (C) 2000 Eazel, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Authors: Rebecca Schulman <rebecka@eazel.com>
 *  
 */


/* medusa-index-service.h -- API for externals users of the medusa
   indexing service. */

#ifndef MEDUSA_INDEX_SERVICE_H
#define MEDUSA_INDEX_SERVICE_H

#include <time.h>
#include <glib.h>


/* Returns 0 if getting the index time fails. */
time_t medusa_index_service_get_last_index_update_time (void);

/* Returns 0 if getting the index time fails. */
void medusa_index_service_set_last_index_update_time (time_t);

/* This API is used to determine whether medusa is on on the system or not. */
gboolean medusa_index_service_is_enabled (void);

/* Returns the default path of the root of the index. */
gchar * medusa_index_service_get_default_index_root (void);

/* Returns the list of URIs (and their subdirectories) that the indexer will ignore.. */
GSList * medusa_index_service_get_file_index_stoplist (void);

/* Returns the list of filesystem types that will not be indexed */
GSList * medusa_index_service_get_mount_type_stoplist (void);

/* Returns the percentage complete of the indexing process */
int medusa_index_service_get_index_percentage_complete (void);

/* Return true if indexing is in process */
gboolean medusa_index_service_indexing_is_currently_in_progress (void);

/* Return true if an index is available for searching */
gboolean medusa_index_service_indexed_search_is_available (void);

/* Return true if the index files exist */
gboolean medusa_index_service_index_files_exist (void);

/* Return true if medusa can perform an unindexed search*/
gboolean medusa_index_service_unindexed_search_is_available_for_uri (const char *uri);

/* These are currently disabled, and need repair before they may be used again. */
typedef void (* MedusaSystemStateFunc) (gpointer data);

/* Returns an update function id, that can be used to remove the callback with  medusa_system_services_remove_update_function */
int medusa_execute_when_system_state_changes (MedusaSystemStateFunc callback,
                                                gpointer callback_data);
int medusa_execute_once_when_system_state_changes (MedusaSystemStateFunc callback,
                                                        gpointer callback_data);

void medusa_remove_state_changed_function (int update_function_id);

#endif /* MEDUSA_INDEX_SERVICE_H */
