/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  Copyright (C) 2000 Eazel, Inc., 2001 Rebecca Schulman
 *  
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@eazel.com>
 *
 *  medusa-file-index-utilties.c  -- Utility functions to manipulate
 *  dates and user and group information.
 *
 */

#include <dirent.h>
#include <grp.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>   /* FreeBSD <grp.h> needs this */
#include <time.h>
#include <unistd.h>

#include <glib.h>
#include <libgnomevfs/gnome-vfs-file-info.h>
#include <libgnomevfs/gnome-vfs-ops.h>
#include <libgnomevfs/gnome-vfs-utils.h>

#include <config.h>
#include "medusa-file-info-utilities.h"
#include "medusa-index-filenames.h"
#include "medusa-log.h"
#include "medusa-stdio-extensions.h"
#include "medusa-test.h"
#include "medusa-utils.h"

static gboolean     username_exists                       (const char *username);
static gboolean     group_exists                          (const char *username);

static int          number_of_strsplit_fields_returned    (char **return_value);
static struct tm*   get_time_struct_for_beginning_of_date (const char *date);
static struct tm*   get_time_struct_for_end_of_date       (const char *date);
static gboolean     is_a_leap_year                        (int year);
static void         move_time_struct_a_week_into_the_past (struct tm *time_struct);




static gboolean
username_exists (const char *username) 
{
        const struct passwd *user_info;
        
        user_info = getpwnam (username);
        return user_info != NULL; 
}


static gboolean
group_exists (const char *group) 
{
        const struct group *group_info;
        
        group_info = getgrnam (group);
        return group_info != NULL; 
}
                
gboolean
medusa_username_to_uid (const char *username,
                        uid_t *uid)
{
        struct passwd *user_info;

        if (username_exists (username) == FALSE) {
                return FALSE;
        }
            
        
        user_info = getpwnam (username);
        g_assert (user_info != NULL);
        *uid = user_info->pw_uid;
        
        return TRUE;
}


gboolean
medusa_group_to_gid (const char *group,
                     gid_t *gid)
{
        const struct group *group_info;

        if (group_exists (group) == FALSE) {
                return FALSE;
        }
            
        
        group_info = getgrnam (group);
        g_assert (group_info != NULL);
        *gid = group_info->gr_gid;


        return TRUE;

}


static int
number_of_strsplit_fields_returned (char **return_value)
{
        int i;
        for (i = 0; return_value[i] != NULL; i++);
        return i;
}

static struct tm*
get_time_struct_for_beginning_of_date (const char *date)
{
        char **date_fields;
        int month, day_of_month, year;
        struct tm *time_struct;

        date_fields = g_strsplit (date, "/", 0);
        /* Don't deal with invalid dates */
        if (number_of_strsplit_fields_returned (date_fields) != 3) {
                return NULL;
        }
        month = strtol (date_fields[0], NULL, 10);
        day_of_month = strtol (date_fields[1], NULL, 10);
        year = strtol (date_fields[2], NULL, 10);

        time_struct = g_new0 (struct tm, 1);

        time_struct->tm_sec = 0;
        time_struct->tm_min = 0;
        time_struct->tm_hour = 0;
        time_struct->tm_mday = day_of_month;
        time_struct->tm_mon = month - 1;
        time_struct->tm_year = year - 1900;
        /* FIXME bugzilla.eazel.com 2997:  
           What to do about the tm_isdst (daylight savings time)
           field? */
        return time_struct;
}
        

static struct tm*
get_time_struct_for_end_of_date (const char *date)
{
        char **date_fields;
        int month, day_of_month, year;
        struct tm *time_struct;

        date_fields = g_strsplit (date, "/", 0);
        /* Don't deal with invalid dates */
        if (number_of_strsplit_fields_returned (date_fields) != 3) {
                return NULL;
        }
        month = strtol (date_fields[0], NULL, 10);
        day_of_month = strtol (date_fields[1], NULL, 10);
        year = strtol (date_fields[2], NULL, 10);
        
        time_struct = g_new0 (struct tm, 1);

        time_struct->tm_sec = 59;
        time_struct->tm_min = 59;
        time_struct->tm_hour = 23;
        time_struct->tm_mday = day_of_month;
        time_struct->tm_mon = month - 1;
        time_struct->tm_year = year - 1900;
        /* FIXME bugzilla.eazel.com 2997:  
           What to do about the tm_isdst (daylight savings time)
           field? */
        return time_struct;
}

static gboolean
is_a_leap_year (int year) 
{
        /* I could be anal here, but I think this is enough */
        return ((year % 4) == 0);
}

static void
move_time_struct_a_week_into_the_past (struct tm *time_struct)
{
        if (time_struct->tm_mday > 7) {
                time_struct->tm_mday -= 7;
                return;
        }
        switch (time_struct->tm_mon) {
                /* roll back the year for early January dates */
        case 0:
                time_struct->tm_year--;
                time_struct->tm_mon = 11;
                time_struct->tm_mday = time_struct->tm_mday - 7 + 31;
                break;
                /* 30 days has September, April, June, and November */
        case 3: case 5: case 8: case 10:
                time_struct->tm_mon--;
                time_struct->tm_mday = time_struct->tm_mday - 7 + 30;
                break;
                /* All the rest have 31, except for February */
        case 2: case 4: case 6: case 7: case 9: case 11:
                time_struct->tm_mon--;
                time_struct->tm_mday = time_struct->tm_mday - 7 + 31;
                break;
                /* Except for February, which has 28 (or 29!) */
        case 1:
                time_struct->tm_mon = 0;
                if (is_a_leap_year (time_struct->tm_year)) {
                        time_struct->tm_mday = time_struct->tm_mday - 7 + 29;
                }
                else {
                        time_struct->tm_mday = time_struct->tm_mday - 7 + 28;
                }
                break;
        }
}                     
                
 
static void
move_time_struct_a_week_into_the_future (struct tm *time_struct)
{
        switch (time_struct->tm_mon) {
                /* advance the date for late December dates */
        case 11:
                if (time_struct->tm_mday > 24) {
                        time_struct->tm_year++;
                        time_struct->tm_mon = 0;
                        time_struct->tm_mday = time_struct->tm_mday + 7 - 31;
                }
                else {
                        time_struct->tm_mday += 7;
                }
                break;
                /* 30 days has September, April, June, and November */
        case 3: case 5: case 8: case 10:
                if (time_struct->tm_mday > 23) {
                        time_struct->tm_mon++;
                        time_struct->tm_mday = time_struct->tm_mday + 7 - 30;
                }
                else {
                        time_struct->tm_mday += 7;
                }
                break;
                /* All the rest have 31, except for February */
        case 0: case 2: case 4: case 6: case 7: case 9:
                if (time_struct->tm_mday > 24) {
                        time_struct->tm_mon++;
                        time_struct->tm_mday = time_struct->tm_mday + 7 - 31;
                }
                else {
                        time_struct->tm_mday += 7;
                }
                break;
                /* Except for February, which has 28 (or 29!) */
        case 1:
                if (is_a_leap_year (time_struct->tm_year)) {
                        if (time_struct->tm_mday > 22) {
                                time_struct->tm_mon++;
                                time_struct->tm_mday = time_struct->tm_mday + 7 - 29;
                        }
                        else {
                                time_struct->tm_mday += 7;
                        }
                }
                else {
                        if (time_struct->tm_mday > 21) {
                                time_struct->tm_mon++;
                                time_struct->tm_mday = time_struct->tm_mday + 7 - 28;
                        }
                        else {
                                time_struct->tm_mday += 7;
                        }
                }
                break;
        }
}                     

static void
move_time_struct_a_month_into_the_past (struct tm *time_struct)
{
        if (time_struct->tm_mon == 0) {
                time_struct->tm_year--;
                time_struct->tm_mon = 11;
        }
        else {
                time_struct->tm_mon--;
        }
}


static void
move_time_struct_a_month_into_the_future (struct tm *time_struct)
{
        if (time_struct->tm_mon == 11) {
                time_struct->tm_year++;
                time_struct->tm_mon = 0;
        }
        else {
                time_struct->tm_mon++;
        }
}

time_t       
medusa_file_info_get_first_unix_time_occurring_on_date (const char *date)
{
        struct tm *time_struct;
        time_t numerical_time;

        time_struct = get_time_struct_for_beginning_of_date (date);
        numerical_time = mktime (time_struct);
        g_free (time_struct);

        return numerical_time;
}

time_t       
medusa_file_info_get_last_unix_time_occurring_on_date (const char *date)
{
        struct tm *time_struct;
        time_t numerical_time;

        time_struct = get_time_struct_for_end_of_date (date);
        numerical_time = mktime (time_struct);
        g_free (time_struct);

        return numerical_time;
}

time_t       
medusa_file_info_get_unix_time_a_week_before_date (const char *date)
{
        struct tm *time_struct;
        time_t numerical_time;

        time_struct = get_time_struct_for_beginning_of_date (date);
        move_time_struct_a_week_into_the_past (time_struct);
        numerical_time = mktime (time_struct);

        g_free (time_struct);

        return numerical_time;
}

time_t       
medusa_file_info_get_unix_time_a_week_after_date (const char *date)
{
        struct tm *time_struct;
        time_t numerical_time;
        
        time_struct = get_time_struct_for_beginning_of_date (date);
        move_time_struct_a_week_into_the_future (time_struct);
        numerical_time = mktime (time_struct);

        g_free (time_struct);

        return numerical_time;
}

time_t       
medusa_file_info_get_unix_time_a_month_before_date (const char *date)
{
        struct tm *time_struct;
        time_t numerical_time;
        
        time_struct = get_time_struct_for_beginning_of_date (date);
        move_time_struct_a_month_into_the_past (time_struct);
        numerical_time = mktime (time_struct);

        g_free (time_struct);
        
        return numerical_time;
}

time_t       
medusa_file_info_get_unix_time_a_month_after_date (const char *date)
{
        struct tm *time_struct;
        time_t numerical_time;
        
        time_struct = get_time_struct_for_beginning_of_date (date);
        move_time_struct_a_month_into_the_future (time_struct);
        numerical_time = mktime (time_struct);

        g_free (time_struct);

        return numerical_time;

}

gboolean
medusa_file_is_newer_than_time (const char *file_name,
                                time_t time_to_check)
{
        struct stat stat_buffer;

        g_return_val_if_fail (file_name != NULL, FALSE);
        lstat (file_name, &stat_buffer);

        return stat_buffer.st_mtime > time_to_check;
        
}

gboolean
medusa_group_contains (int gid, int uid)
{
        struct group *group;
        struct passwd *password_entry;
        int i;
        group = getgrgid (gid);
        if (group == NULL) {
                return FALSE;
        }
        for (i = 0; group->gr_mem[i] != NULL; i++) {
                password_entry = getpwnam (group->gr_mem[i]);
                g_return_val_if_fail (password_entry != NULL, FALSE);
                if (password_entry->pw_uid == uid) {
                        return TRUE;
                }
        }
        return FALSE;
  
}

/* uri_is_directory
 *
 * Returns TRUE if the file is a directory, FALSE if not.
 * It's an error to ask whether a non-existent uri is a 
 * directory or not. 
 */

static gboolean
uri_is_directory (const char *uri)
{
        GnomeVFSFileInfo file_info;
        GnomeVFSResult result;
        
        result = gnome_vfs_get_file_info (uri, &file_info, 
                                          GNOME_VFS_FILE_INFO_DEFAULT);
        if (result != GNOME_VFS_OK) {
                medusa_log_error ("Error getting information about uri %s "
                                  "in uri_is_directory.  Error was %s",
                                  uri, 
                                  gnome_vfs_result_to_string (result));
                return FALSE;
        }

        return file_info.type == GNOME_VFS_FILE_TYPE_DIRECTORY;
}

                          

/* medusa_foreach_file_in_directory_recursive
 *
 * Takes a directory, and a function with closure data.
 * Calls the function once on each file in the directory,
 * recursively.  It's an error to pass a uri that is not a 
 * directory.
 * 
 * directory_uri       The uri to recurse over, for example "file:///var/tmp"
 * func                The function to call
 * function_data       Data passed as the function's second argument
 *                     (the file's uri is passed as the first)
 *
 * Return value        Returns TRUE if all I/O associated with opening the
 *                     directory and reading the recursive over its children
 *                     is successful; FALSE if a stdio error occurs
 */
gboolean
medusa_foreach_uri_in_directory_recursive (const char *directory_uri,
                                           MedusaForeachURIFunc func,
                                           gpointer function_data)
{
        char *directory_path;
        char *file_uri;
        DIR *directory_stream;
        struct dirent *entry;

        g_return_val_if_fail (directory_uri != NULL, FALSE);
        g_return_val_if_fail (func != NULL, FALSE);

        directory_path = gnome_vfs_get_local_path_from_uri (directory_uri);
        if (directory_path == NULL) {
                medusa_log_error ("Invalid URI %s passed to %s",
                                  directory_uri, __FUNCTION__);
                return FALSE;
        }
        directory_stream = opendir (directory_path);
        if (directory_stream == NULL) {
                medusa_log_error ("Failed to open directory %s "
                                  "in %s",
                                  directory_path, __FUNCTION__);
                return FALSE;
        }

        func (directory_uri,
              function_data);
        
        while ((entry = readdir (directory_stream)) != NULL) {
                if (strcmp (entry->d_name, ".") == 0 ||
                    strcmp (entry->d_name, "..") == 0) {
                        continue;
                }
                file_uri = medusa_full_uri_from_directory_uri_and_file_name (directory_uri,
                                                                              entry->d_name);

                if (uri_is_directory (file_uri)) {
                        if (!medusa_foreach_uri_in_directory_recursive (file_uri,
                                                                        func,
                                                                        function_data)) {
                                return FALSE;
                        }
                }
                else {
                        func (file_uri,
                              function_data);
                }
                g_free (file_uri);
        }
        if (closedir (directory_stream) != 0) {
                medusa_log_error ("Failed to close directory stream for "
                                  "directory with uri %s",
                                  directory_uri);
                return FALSE;
        }

        return TRUE;
}

static void
uri_is_directory_self_check (void)
{
        char *medusa_dir_uri;
        char *test_file_name, *test_file_uri;
        char *unique_file_name;

        g_print ("Testing uri_is_directory...\n");
        MEDUSA_TEST_BOOLEAN_RESULT (uri_is_directory ("file:///"));

        medusa_dir_uri = 
                gnome_vfs_get_uri_from_local_path (medusa_get_index_path ());
        MEDUSA_TEST_BOOLEAN_RESULT (uri_is_directory (medusa_dir_uri));
        g_free (medusa_dir_uri);

        unique_file_name = g_strdup_printf ("%s-%lu", __FUNCTION__, (unsigned long) getpid ());
        test_file_name = 
                medusa_full_path_from_directory_and_file_name (medusa_get_index_path (),
                                                               unique_file_name);
        g_free (unique_file_name);
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_create_file (test_file_name));

        test_file_uri = gnome_vfs_get_uri_from_local_path (test_file_name);
        MEDUSA_TEST_BOOLEAN_RESULT (!uri_is_directory (test_file_uri));
        MEDUSA_TEST_INTEGER_RESULT (unlink (test_file_name), 0);
        g_free (test_file_name);
        g_free (test_file_uri);
}

static void
count_calls_to_foreach (const char *uri,
                        gpointer data)
{
        int *number_of_calls;

        g_assert (data != NULL);
        number_of_calls = (int *) data;
        (*number_of_calls)++;
}

static void
foreach_uri_in_directory_recursive_self_check (void)
{
        char *unique_file_name;
        char *test_directory, *test_directory_uri;
        char *first_child, *second_child;
        char *directory_child, *child_of_directory_child;
        int number_of_calls;

        g_print ("Testing medusa_foreach_uri_in_directory_recursive...\n");

        unique_file_name = g_strdup_printf ("%s-%lu", __FUNCTION__, (unsigned long) getpid ());
        test_directory = 
                medusa_full_path_from_directory_and_file_name (MEDUSA_SOURCEDIR,
                                                               unique_file_name);
        g_free (unique_file_name);
                                                               
        MEDUSA_TEST_INTEGER_RESULT (mkdir (test_directory, S_IRWXU), 0);
        test_directory_uri = gnome_vfs_get_uri_from_local_path (test_directory);

        /* Test that the foreach function gets called just once for a
           directory with no children */
        number_of_calls = 0;
        medusa_foreach_uri_in_directory_recursive (test_directory_uri,
                                                   count_calls_to_foreach,
                                                   &number_of_calls);
        MEDUSA_TEST_INTEGER_RESULT (number_of_calls, 1);

        /* Test function is executed once for every file uri for a
           directory with children that are all simple files */
        first_child =
                medusa_full_path_from_directory_and_file_name (test_directory,
                                                               "first-child");
        second_child =
                medusa_full_path_from_directory_and_file_name (test_directory,
                                                               "second-child");
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_create_file (first_child));
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_create_file (second_child));
        number_of_calls = 0;
        medusa_foreach_uri_in_directory_recursive (test_directory_uri,
                                                   count_calls_to_foreach,
                                                   &number_of_calls);
        MEDUSA_TEST_INTEGER_RESULT (number_of_calls, 3);

        /* Test function is executed once for every file uri for a
           directory with children that are also directories */
        directory_child =
                medusa_full_path_from_directory_and_file_name (test_directory,
                                                               "directory-child");
        MEDUSA_TEST_INTEGER_RESULT (mkdir (directory_child, S_IRWXU), 0);
        child_of_directory_child = 
                medusa_full_path_from_directory_and_file_name (directory_child,
                                                               "first-file");
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_create_file (child_of_directory_child));
        number_of_calls = 0;
        medusa_foreach_uri_in_directory_recursive (test_directory_uri,
                                                   count_calls_to_foreach,
                                                   &number_of_calls);
        MEDUSA_TEST_INTEGER_RESULT (number_of_calls, 5);
        
        MEDUSA_TEST_INTEGER_RESULT (unlink (child_of_directory_child), 0);
        MEDUSA_TEST_INTEGER_RESULT (rmdir (directory_child), 0);
        MEDUSA_TEST_INTEGER_RESULT (unlink (first_child), 0);
        MEDUSA_TEST_INTEGER_RESULT (unlink (second_child), 0);
        MEDUSA_TEST_INTEGER_RESULT (rmdir (test_directory), 0);


        g_free (child_of_directory_child);
        g_free (directory_child);
        g_free (first_child);
        g_free (second_child);
        g_free (test_directory);
}

void
medusa_file_info_utilities_self_check (void)
{
        g_print ("Self checking medusa's file info utilities:\n");
        
        uri_is_directory_self_check ();
        foreach_uri_in_directory_recursive_self_check ();
}
