/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  Copyright (C) 2000 Eazel, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@eazel.com>
 *
 *  medusa-stdio-extensions.c - Functions to do standard local I/O
 *  tasks.
 */

#include "errno.h"
#include <fcntl.h>
#include <glib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <libgnomevfs/gnome-vfs-uri.h>

#include "medusa-log.h"
#include "medusa-stdio-extensions.h"
#include "medusa-test.h"

/* Reads a whole file into a new buffer.
   Returns the number of bytes read */
int
medusa_read_whole_file (const char *file_name,
                        char **file_contents)
{
        FILE *stream;
        struct stat stat_info;
        int stat_return_code, bytes_read;
        char *buffer;
        
        stat_return_code = stat (file_name, &stat_info);
        
        if (stat_return_code == -1) {
                *file_contents = NULL;
                return -1;
        }
        
        stream = fopen (file_name, "r");
        if (stream == NULL) {
                *file_contents = NULL;
                return -1;
        }

        buffer = g_malloc (stat_info.st_size + 1);
        
        bytes_read = fread (buffer, 1, stat_info.st_size, stream);
        buffer[bytes_read] = 0;

        fclose (stream);

        if (bytes_read != stat_info.st_size) {
                g_free (buffer);
                *file_contents = NULL;
                return -1;
        }

        *file_contents = buffer;
        return bytes_read;
}

off_t
medusa_get_file_size (const char *file_name)
{
        int stat_return_code;
        struct stat stat_information;

        g_return_val_if_fail (file_name != NULL, -1);

        stat_return_code = stat (file_name, &stat_information);

        if (stat_return_code == -1) {
                medusa_log_error ("Could not stat file %s", file_name);
                return -1;
        }

        return stat_information.st_size;
}

gboolean
medusa_unlink_if_present (const char *file_name)
{
        g_return_val_if_fail (file_name != NULL, FALSE);
        
        if (access (file_name, F_OK) != 0) {
                medusa_log_event (MEDUSA_DB_LOG_EVERYTHING,
                                  "File %s does not exist; not unlinking it.", file_name);
                return TRUE;
        }

        if (unlink (file_name) != 0) {
                medusa_log_error ("File %s could not be removed", file_name);
                return FALSE;
        }

        return TRUE;
}

#define READLINE_BUFFER_SIZE 512

char *
medusa_readline_from_stream (FILE   *stream)
{
        int next_character;
        char buffer[READLINE_BUFFER_SIZE];
        int read_count;
        char *line;

        g_return_val_if_fail (stream != NULL, NULL);

        memset (buffer, 0, READLINE_BUFFER_SIZE);
        line = NULL;
        
        while (TRUE) {
                for (read_count = 0; read_count < READLINE_BUFFER_SIZE - 1; read_count++) {
                        next_character = fgetc (stream);
                        
                        if (next_character == EOF ||
                            next_character == '\n') {
                                if (line != NULL) {
                                        line = g_strconcat (line, buffer, NULL);
                                }
                                else if (read_count > 0) {
                                        line = g_strdup (buffer);
                                }
                                else {
                                        line = NULL;
                                }
                                return line; 
                        }
                        
                        buffer[read_count] = (char) next_character;
                }
                
                if (line != NULL) {
                        line = g_strconcat (line, buffer, NULL);
                }
                else {
                        line = g_strdup (buffer);
                }

                return line;
        }
}

gboolean
medusa_make_file_writeable (const char *filename)
{
        struct stat stat_buffer;
        
        if (!medusa_stat_error_cover (filename, &stat_buffer) ||
            !medusa_chmod_error_cover (filename, stat_buffer.st_mode & S_IWUSR) != 0) {
                return FALSE;
        }

        return TRUE;
}

static char *
get_parent_directory (const char *file_name)
{
        GnomeVFSURI *uri, *parent_uri;
        const char *parent;
        char *parent_copy;

        uri = gnome_vfs_uri_new (file_name);
        if (uri == NULL) {
                return FALSE;
        }

        parent_uri = gnome_vfs_uri_get_parent (uri);
        if (parent_uri == NULL) {
                return FALSE;
        }
        
        parent = gnome_vfs_uri_get_path (parent_uri);
        parent_copy = g_strdup (parent);

        gnome_vfs_uri_unref (uri);
        gnome_vfs_uri_unref (parent_uri);

        return parent_copy;
}

gboolean
medusa_create_directory_recursive (const char *path,
                                   mode_t      mode)
{
        char *parent_directory;

        if (access (path, X_OK) == 0) {
                return TRUE;
        }
        
        if (mkdir (path, mode) == 0) {
                return TRUE;
        }
        else {
                parent_directory = get_parent_directory (path);
                if (parent_directory == NULL) {
                        return FALSE;
                }

                medusa_create_directory_recursive (parent_directory,
                                                   mode);
                
                return medusa_mkdir_error_cover (path, mode);
        }
}

FILE *          
medusa_fopen_error_cover (const char  *path,
                          const char  *mode)
{
        FILE *stream;

        g_return_val_if_fail (path != NULL, NULL);
        g_return_val_if_fail (mode != NULL, NULL);
        g_return_val_if_fail (strcmp (mode, "r") == 0  ||
                              strcmp (mode, "r+") == 0 ||
                              strcmp (mode, "w") == 0  ||
                              strcmp (mode, "w+") == 0 ||
                              strcmp (mode, "a") == 0  ||
                              strcmp (mode, "a+") == 0, NULL);

        stream = fopen (path, mode);

        if (stream == NULL) {
                medusa_log_error ("fopen failed for file %s", path == NULL ? "" : path);
                return NULL;
        }
        
        return stream;
}

gboolean        
medusa_create_file (const char *path)
{
        int new_file_descriptor;

        g_return_val_if_fail (path != NULL, FALSE);

        new_file_descriptor = creat (path, S_IRUSR | S_IRGRP | S_IWUSR | S_IWGRP | S_IROTH | S_IWOTH);

        if (new_file_descriptor == -1) {
                medusa_log_error ("creat failed for file %s", path);
                return FALSE;
        }

        close (new_file_descriptor);

        return TRUE;
}


gboolean
medusa_chmod_error_cover (const char *path,
                          mode_t      mode)
{
        if (chmod (path, mode) != 0) {
                medusa_log_error ("chmod failed for file %s "
                                  "with permissions %d", path,
                                  mode);
                return FALSE;
        }
        
        return TRUE;
}

gboolean        
medusa_fseek_error_cover (FILE        *stream,
                          long         offset,
                          int          whence,
                          const char  *file_name)
{
        int fseek_return_value;

        g_return_val_if_fail (stream != NULL, FALSE);
        g_return_val_if_fail (whence == SEEK_SET ||
                              whence == SEEK_CUR ||
                              whence == SEEK_END, FALSE);
        
        fseek_return_value = fseek (stream, offset, whence);

        if (fseek_return_value == -1) {
                medusa_log_error ("fseek failed on file %s", file_name == NULL ? "" : file_name);
                return FALSE;
        }
        
        return TRUE;
}

gboolean
medusa_fread_error_cover (void         *buffer,
                          size_t        item_size,
                          size_t        number_of_items,
                          FILE         *stream,
                          const char   *file_name,
                          gboolean      partial_read_is_an_error)
{
        int fread_return_value;

        g_return_val_if_fail (buffer != NULL, FALSE);
        g_return_val_if_fail (item_size > 0, FALSE);
        g_return_val_if_fail (number_of_items > 0, FALSE);
        g_return_val_if_fail (stream != NULL, FALSE);

        fread_return_value = fread (buffer, item_size, number_of_items, stream);

        if (fread_return_value == -1 ||
            (partial_read_is_an_error && fread_return_value < number_of_items)) {
                medusa_log_error ("fread failed on file %s.  %d items were read instead of the %d requested", 
                                  file_name == NULL ? "" : file_name, fread_return_value, number_of_items);
                return FALSE;
        }

        return TRUE;
}

gboolean
medusa_fwrite_error_cover (const void         *buffer,
                           size_t              item_size,
                           size_t              number_of_items,
                           FILE               *stream,
                           const char         *file_name)
{
        int fwrite_return_value;

        g_return_val_if_fail (buffer != NULL, FALSE);
        g_return_val_if_fail (item_size > 0, FALSE);
        g_return_val_if_fail (number_of_items > 0, FALSE);
        g_return_val_if_fail (stream != NULL, FALSE);

        fwrite_return_value = fwrite (buffer, item_size, number_of_items, stream);

        if (fwrite_return_value == -1 ||
            fwrite_return_value < number_of_items) {
                medusa_log_error ("fwritten failed on file %s.  %d items were written instead of the %d requested", 
                                  file_name == NULL ? "" : file_name, fwrite_return_value, number_of_items);
                return FALSE;
        }

        return TRUE;
}


gboolean        
medusa_fclose_error_cover (FILE        *stream,
                           const char  *file_name)
{
        int fclose_return_value;

        g_return_val_if_fail (stream != NULL, FALSE);

        fclose_return_value = fclose (stream);

        if (fclose_return_value == -1) {
                medusa_log_error ("fclose failed on file %s",
                                  file_name == NULL ? "" : file_name);
                return FALSE;
        }

        return TRUE;
}

gboolean
medusa_mkdir_error_cover (const char *path,
                          mode_t  mode)
{
        g_return_val_if_fail (path != NULL, FALSE);

        if (mkdir (path, mode) != 0) {
                medusa_log_error ("Mkdir of %s with mode %d failed", path, mode);
                return FALSE;
        }

        return TRUE;
}

gboolean
medusa_rename_error_cover (const char *old_path,
                           const char *new_path)
{
        if (rename (old_path, new_path) != 0) {
                medusa_log_error ("Rename of %s to %s failed",
                                  old_path, new_path);
                return FALSE;
        }

        return TRUE;
}

gboolean
medusa_stat_error_cover (const char  *file_name,
                         struct stat *stat_buffer)
{
        if (stat (file_name, stat_buffer) != 0) {
                medusa_log_error ("Stat failed for file %s", file_name);
                return FALSE;
        }

        return TRUE;
}

void
medusa_stdio_extensions_self_check (void)
{
        char *test_dir, *recursive_test_dir_1, *recursive_test_dir_2, *recursive_test_dir_3;
        
        MEDUSA_TEST_STRING_RESULT (get_parent_directory (""), NULL);
        MEDUSA_TEST_STRING_RESULT (get_parent_directory ("/foo"), "/");
        MEDUSA_TEST_STRING_RESULT (get_parent_directory ("/foo/bar"), "/foo");
        MEDUSA_TEST_STRING_RESULT (get_parent_directory ("/foo/bar/baz"), "/foo/bar");
        MEDUSA_TEST_STRING_RESULT (get_parent_directory ("/foo/bar/baz/"), "/foo/bar");

        /* Tests for medusa_create_directory_recursive */
        test_dir = g_strdup_printf ("/tmp/medusa_create_dir_test.%lu", (long unsigned) getpid ());

        MEDUSA_TEST_BOOLEAN_RESULT (medusa_create_directory_recursive (test_dir, S_IRWXU));
        MEDUSA_TEST_SYSTEM_CALL (access (test_dir, W_OK));
        MEDUSA_TEST_SYSTEM_CALL (rmdir (test_dir));
        MEDUSA_TEST_BOOLEAN_RESULT (access (test_dir, X_OK) != 0);
       
        recursive_test_dir_1 = g_strdup_printf ("%s/foo", test_dir);
        recursive_test_dir_2 = g_strdup_printf ("%s/foo/bar", test_dir);
        recursive_test_dir_3 = g_strdup_printf ("%s/foo/bar/baz", test_dir);
        
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_create_directory_recursive (recursive_test_dir_3, S_IRWXU));
        MEDUSA_TEST_SYSTEM_CALL (access (test_dir, W_OK));
        MEDUSA_TEST_SYSTEM_CALL (rmdir (recursive_test_dir_3));
        MEDUSA_TEST_SYSTEM_CALL (rmdir (recursive_test_dir_2));
        MEDUSA_TEST_SYSTEM_CALL (rmdir (recursive_test_dir_1));
        MEDUSA_TEST_SYSTEM_CALL (rmdir (test_dir));
        MEDUSA_TEST_BOOLEAN_RESULT (access (test_dir, X_OK) != 0);

}
