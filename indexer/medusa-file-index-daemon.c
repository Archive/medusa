/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  Copyright (C) 2000 Eazel, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@eazel.com>
 */

#include <config.h>

#include <errno.h>
#include <glib.h>
#include <glib-object.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-program.h>
#include <libgnome/gnome-init.h>
#include <libgnomevfs/gnome-vfs-init.h>
#include <libgnomevfs/gnome-vfs-types.h>
#include <libgnomevfs/gnome-vfs-uri.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <gconf/gconf.h>
#include <popt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>

#ifdef HAVE_SYS_VFS_H
#include <sys/vfs.h>
#endif

#ifdef HAVE_STATVFS
#include <sys/statvfs.h>
#else
#ifdef HAVE_SYS_MOUNT_H
#    ifdef MIN
#      undef MIN  /* Make FreeBSD <sys/param.h> happy */
#    endif
#    ifdef MAX
#      undef MAX  /* Make FreeBSD <sys/param.h> happy */
#    endif
#    include <sys/param.h>
#    include <sys/mount.h>
#else
#    if defined(HAVE_STATFS)
#      include <sys/statfs.h>
#    endif
#  endif
#endif /* HAVE_STATVFS */

#include <time.h>
#include <unistd.h>
#include <signal.h>

#include <libmedusa-internal/medusa-conf.h>
#include <libmedusa-internal/medusa-debug.h>
#include <libmedusa/medusa-index-filenames.h>
#include <libmedusa/medusa-lock.h>
#include <libmedusa-internal/medusa-master-db.h>
#include <libmedusa/medusa-search-service-private.h>
#include <libmedusa/medusa-index-progress.h>
#include <libmedusa/medusa-index-service.h>
#include <libmedusa/medusa-index-service-private.h>
#include <libmedusa/medusa-log.h>

#ifndef AF_LOCAL
#define AF_LOCAL AF_UNIX
#endif


#ifndef SUN_LEN
/* This system is not POSIX.1g.         */
#define SUN_LEN(ptr) ((size_t) (((struct sockaddr_un *) 0)->sun_path)  \
       + strlen ((ptr)->sun_path))
#endif

#define ACCOUNT_INDEX_NAME "account"

/* Creates a totally new file index, and writes it over the old one */
static void             do_full_indexing                  (const char *root_directory,
                                                           const char *index_name,
                                                           gboolean use_idle_service);

static void             do_index_accounting               (void);
/* Do clean up, etc. */
static void             exit_indexer                      (void);




extern int errno;
static gboolean create_index_even_if_disk_is_full;

/* Main index loop.  
   Creates three process to wait for signals, 
   do fast reindexing, and do slow reindexing */
int
main (int argc, char *argv[])
{
        GnomeProgram *program;
        GValue context_as_value = { 0 };
        poptContext popt_context;
        const char **unparsed_arguments;
        gboolean text_index_accounting;
        gboolean dont_use_idle_service;
        gboolean run_in_listener_debug_mode;
        const char *root_directory, *index_name;
        struct poptOption command_line_options[] = {
                { "folder", 'f', POPT_ARG_STRING, &root_directory, 
                  0, N_("Specify the root folder to start indexing at"),
                  N_("PATH") },
                { "named-index", 'n', POPT_ARG_STRING, &index_name,
                  0, N_("Create a named index"),
                  N_("NAME") },
                { "do-text-accounting", '\0', POPT_ARG_NONE, &text_index_accounting,
                  0, N_("Count the space taken in the text index by file name and mime type"),
                  NULL },
                { "without-idle-checks", '\0', POPT_ARG_NONE, &dont_use_idle_service,
                  0, N_("Don't sleep when medusa-idled reports user activity"),
                  NULL },
                { "force-indexing", '\0', POPT_ARG_NONE, &create_index_even_if_disk_is_full,
                  0, N_("Create index even if the disk looks too full to support it"),
                  NULL },
                { "debug-listener", '\0', POPT_ARG_NONE, &run_in_listener_debug_mode,
                  0, N_("Don't fork the listener. Run it in debugging mode"),
                  NULL },
                POPT_AUTOHELP
                { NULL, '\0', 0, NULL, 0, NULL, NULL }
        };

        if (g_getenv ("MEDUSA_DEBUG") != NULL) {
                medusa_make_warnings_and_criticals_stop_in_debugger
                        (G_LOG_DOMAIN,
                         "GLib",
                         "GLib-GObject",
                         "GModule",
                         "GThread",
                         "GnomeVFS",
                         "Medusa",
                         NULL);
        }

        /* Set these to defaults */
        root_directory = NULL;
        index_name = NULL;
        text_index_accounting = FALSE;
        dont_use_idle_service = FALSE;
        run_in_listener_debug_mode = FALSE;

        program = gnome_program_init ("medusa-indexd", VERSION,
                                      LIBGNOME_MODULE, argc, argv,
                                      GNOME_PARAM_POPT_TABLE, command_line_options,
                                      GNOME_PARAM_HUMAN_READABLE_NAME, _("Medusa Index Daemon"),
                                      NULL);
        
        g_object_get_property (G_OBJECT (program),
                               GNOME_PARAM_POPT_CONTEXT,
                               g_value_init (&context_as_value, G_TYPE_POINTER));

        popt_context = g_value_get_pointer (&context_as_value);

        nice (19);
        gnome_vfs_init ();
        gconf_init(argc, argv, NULL);
        
        /* Get the remaining arguments */
        unparsed_arguments = poptGetArgs (popt_context);
        if (unparsed_arguments != NULL) {
                fprintf (stderr, _("Invalid argument: %s\n"), unparsed_arguments[0]);
                exit_indexer ();
        }
        if (NULL == root_directory) {
                g_print (_("Using the default root folder for index.\n"));
                char *path;
                path = medusa_index_service_get_default_index_root ();
                root_directory = gnome_vfs_expand_initial_tilde (path);
                g_free (path);
        }
        if (NULL == root_directory) {
                g_error (_("The root folder of the index is not specified, nor is it set in GConf."));
                exit_indexer ();
        }
        if (index_name && strchr (index_name, '/') != NULL) {
                g_error (_("Invalid argument: Index name cannot contain the '/' character.\n"));
                exit_indexer ();
        }
        poptFreeContext (popt_context);

        medusa_log_setup ();
        
        if (text_index_accounting) {
                do_index_accounting ();
                exit_indexer ();
        }
        
        do_full_indexing (root_directory, index_name, dont_use_idle_service); 
        exit_indexer ();
        
        return 1;
}

void
do_index_accounting (void)
{
        char *root_uri;
  
        root_uri = gnome_vfs_get_uri_from_local_path (ROOT_DIRECTORY);

        medusa_master_db_erase_constructed_index (ACCOUNT_INDEX_NAME);

        medusa_log_enable_index_accounting ();
        medusa_master_db_create_index (root_uri,
                                       ACCOUNT_INDEX_NAME,
                                       FALSE);
        g_free (root_uri);
        g_print (_("Finished indexing"));

        medusa_master_db_erase_constructed_index (ACCOUNT_INDEX_NAME);
        g_print (_("Removed account files"));
}  
                     

static void
exit_if_disk_is_too_full ()
{
#ifdef HAVE_STATVFS
        struct statvfs stat_buffer;
#elif defined(HAVE_STATFS)
        struct statfs stat_buffer;
#else 
#warning "You don't have statfs or statfs installed on your system.  The indexer will not be able to determine "
        "if your disk is too full for indexing"
#endif
        int stat_result;
        unsigned long number_of_blocks, number_of_free_blocks, block_size;
        
#ifdef HAVE_STATVFS
        stat_result = statvfs (medusa_get_index_path (), &stat_buffer); 
#elif defined(HAVE_STATFS)
        stat_result = statfs (medusa_get_index_path (), &stat_buffer); 
#endif

        if (stat_result == -1) {
                medusa_log_error (_("Can not determine file system status for %s, the disk may be too full for an index."), 
                	          medusa_get_index_path ());
                medusa_log_error (_("Indexing is continuing"));
                return;
        }

        number_of_blocks = (unsigned long) stat_buffer.f_blocks;
        number_of_free_blocks = (unsigned long) stat_buffer.f_bfree;
        block_size = (unsigned long) stat_buffer.f_bsize;

        g_print (_("File system %s has %ld blocks, %ld free\n"), 
                 medusa_get_index_path (), number_of_blocks, number_of_free_blocks);
        /* We'll need 40 megs or so for a conservative index estimate */
        if ((number_of_blocks / number_of_free_blocks) > 20 ||
            number_of_free_blocks < (40<<20) / block_size) {
                medusa_log_error (_("Indexing aborted because disk is either more than 95%% full or 40 megs of space are not available at location %s"),
                                  medusa_get_index_path ());
                exit (1);
        }
        
}

static void
do_full_indexing (const char *root_directory,
                  const char *index_name,
                  gboolean dont_use_idle_service)
{
        char *root_uri;
        MedusaWriteLock *write_lock;

        medusa_create_index_path_if_necessary (); 

        /* Remove old constructed, incomplete indices */
        medusa_master_db_erase_constructed_index (index_name);
        
        /* Create the Medusa index directory if it does not exist */
        if (!g_file_test (medusa_get_index_path(), G_FILE_TEST_EXISTS)) {
                if (mkdir (medusa_get_index_path(), S_IRWXU | S_IRWXG) != 0) {
                        medusa_log_error (_("Indexing aborted because %s did not exist but an error occurred creating it"),
                                          medusa_get_index_path());
                }
        }

        if (!create_index_even_if_disk_is_full) {
                exit_if_disk_is_too_full ();
        }
  
        write_lock = medusa_write_lock_get (index_name);
        if (write_lock == NULL) {
                medusa_log_fatal_error (_("Failed to acquire lock file for indexing\n"));
                exit (1);
        }

        root_uri = gnome_vfs_get_uri_from_local_path (root_directory);
        
        GError *err = NULL;
        int pid = 0;
        char *child_argv[3];
        child_argv[0] = "medusa-idled";
        child_argv[1] = NULL;

        if (!g_spawn_async (NULL, child_argv, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL, &pid, &err)) {
                g_print ("%s\n", err->message);
                g_error_free (err);
        }
        
        g_print (_("Indexing...\n"));
        medusa_master_db_create_index (root_uri,
                                       index_name,
                                       dont_use_idle_service);
        g_free (root_uri);
        g_print (_("Finished indexing\n"));
        
        kill (pid, SIGKILL);

        medusa_master_db_move_completed_index_into_place (index_name);
        g_print (_("Done moving index files to correct locations\n"));

        medusa_write_lock_release (write_lock);
        g_print (_("Released lock\n"));

        g_print (_("Exiting\n"));
        exit_indexer ();
}

static void             
exit_indexer (void)
{
        gnome_vfs_shutdown ();
        exit (0);
}
