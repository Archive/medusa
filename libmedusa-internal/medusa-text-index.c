/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  medusa-text-index.c : Do the text indexing of the file system
 *
 *  Copyright (C) 2000 Eazel, Inc., 2001 Rebecca Schulman
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@eazel.com>
 */

#include <glib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <libgnomevfs/gnome-vfs-types.h>
#include <libgnomevfs/gnome-vfs-directory.h>
#include <libgnomevfs/gnome-vfs-file-info.h>
#include <libgnomevfs/gnome-vfs-ops.h>
#include <libgnomevfs/gnome-vfs-utils.h>

#include <libmedusa/medusa-file-info-utilities.h>
#include <libmedusa/medusa-log.h>
#include <libmedusa/medusa-test.h>
#include <medusa-idled/medusa-idled-client.h>
#include "medusa-conf.h"
#include "medusa-file-index.h"
#include "medusa-index-filenames.h"
#include "medusa-io-handler.h"
#include "medusa-lexicon.h"
#include "medusa-text-index.h"
#include "medusa-text-index-private.h"
#include "medusa-text-index-queries.h"
#include "medusa-text-index-plaintext-module.h"


#define LOCATION_FILE_PREFIX "text-index-location-file"
#define TEMP_FILE_PREFIX  "text-index-temp-file"

#define MEDUSA_TEXT_INDEX_TEMP_FILE_MAGIC_NUMBER         "9124"
#define MEDUSA_TEXT_INDEX_TEMP_FILE_VERSION_NUMBER       "0.1"
#define MEDUSA_TEXT_INDEX_LOCATIONS_INDEX_MAGIC_NUMBER   "9126"
#define MEDUSA_TEXT_INDEX_LOCATIONS_INDEX_VERSION_NUMBER "0.1"



/* ie text index structure is set up, and all files were
   found or made successfully */
static gboolean             text_index_files_are_ready                (MedusaTextIndex *text_index);
static char *               generate_temp_index_file_name             (const char *index_name, 
                                                                       int file_number);
static void                 array_of_words_free                       (char **array, 
                                                                       int number_of_words);
static void                 text_index_add_mime_modules               (MedusaTextIndex *text_index);

static void		    close_temp_index_files		      (MedusaTextIndex *index);

static void                 sort_temp_index_data_into_permanent_index (MedusaTextIndex *text_index,
                                                                       gboolean use_idle_service,
                                                                       MedusaIdledConnection *idled_connection);
static void                 add_word_to_real_index                    (const char *key,
                                                                       const char *value,
                                                                       gpointer user_data);

static gint32               get_uri_number_from_temp_index_cell   (MedusaTextIndex *text_index,
                                                                   int cell_number,
                                                                   int temp_index_number);
static gint32               get_last_cell_from_temp_index_cell    (MedusaTextIndex *text_index,
                                                                   int cell_number,
                                                                   int temp_index_number);

static void                 write_uri_number_to_location_file     (MedusaTextIndex *text_index,
                                                                   gint32 uri_number);
                                                                   
                                                                   
static void                 medusa_text_index_destroy             (MedusaTextIndex *text_index);

MedusaTextIndex *
medusa_text_index_open (const char *index_name)
{
        MedusaTextIndex *text_index;
        MedusaVersionedFileResult locations_index_open_result;
        char *locations_index_filename;

        text_index = g_new0 (MedusaTextIndex, 1);

        text_index->creating_index = FALSE;

        text_index->words = medusa_lexicon_open (index_name);

        if (text_index->words == NULL) {
                g_warning ("Couldn't create word hash file\n");
                /* FIXME bugzilla.eazel.com 4557: log errors here */
                g_free (text_index);
                return NULL;
        }

        locations_index_filename = medusa_generate_index_filename (LOCATION_FILE_PREFIX,
                                                                   index_name,
                                                                   FALSE);
        text_index->locations_index = medusa_versioned_file_open (locations_index_filename,
                                                                  MEDUSA_TEXT_INDEX_LOCATIONS_INDEX_MAGIC_NUMBER,
                                                                  MEDUSA_TEXT_INDEX_LOCATIONS_INDEX_VERSION_NUMBER,
                                                                  &locations_index_open_result);
        g_free (locations_index_filename);

        text_index->ref_count = 1;

        if (locations_index_open_result != MEDUSA_VERSIONED_FILE_OK) {
                medusa_log_error ("Problems opening medusa locations index with result %d\n", 
                                  locations_index_open_result);
                medusa_text_index_unref (text_index);
                return NULL;
        }

        return text_index;

}

MedusaTextIndex *
medusa_text_index_new (const char *index_name)
{
        MedusaTextIndex *text_index;
        int zero, i;
        MedusaVersionedFileResult locations_index_create_result;
        char *location_file_name;
        char *temp_file_root_name;
        
        
        location_file_name = medusa_generate_index_filename (LOCATION_FILE_PREFIX,
                                                             index_name,
                                                             TRUE);
        temp_file_root_name = medusa_generate_index_filename (TEMP_FILE_PREFIX,
                                                              index_name,
                                                              TRUE);
        text_index = g_new0 (MedusaTextIndex, 1);
        text_index->creating_index = TRUE;

        text_index->words = medusa_lexicon_new (index_name);

        zero = 0;
        /* Assume for now that if a location index file already exists,
           we are just here to read.  If it doesn't we are here to
           make an index, so set up the temporary index hardware */

        for (i = 0; i < NUMBER_OF_TEMP_INDEXES; i++) {
                text_index->temp_index_name[i] = generate_temp_index_file_name (index_name, i);
                text_index->temp_index_stream[i] = 
                        fopen_new_with_medusa_io_handler_header (text_index->temp_index_name[i],
                                                                 MEDUSA_TEXT_INDEX_TEMP_FILE_MAGIC_NUMBER,
                                                                 MEDUSA_TEXT_INDEX_TEMP_FILE_VERSION_NUMBER);
                /* Write initial cell, so that no one uses cell 0 */
                fwrite (&zero,
                        sizeof (int),
                        1,
                        text_index->temp_index_stream[i]);
                fwrite (&zero,
                        sizeof (int), 
                        1,
                        text_index->temp_index_stream[i]);
                        /* Start with cell number 1 */
                        text_index->current_cell_number[i] = 1;
        }

        text_index->reverse_index_position = 1;
        

        text_index->locations_index = medusa_versioned_file_create (location_file_name,
                                                                    MEDUSA_TEXT_INDEX_LOCATIONS_INDEX_MAGIC_NUMBER,
                                                                    MEDUSA_TEXT_INDEX_LOCATIONS_INDEX_VERSION_NUMBER,
                                                                    &locations_index_create_result);
        if (locations_index_create_result != MEDUSA_VERSIONED_FILE_OK) {
                medusa_log_error ("Could not create locations index file with error code "
                                  "%d\n", locations_index_create_result);
        }

        g_free (location_file_name);
        g_free (temp_file_root_name);

        medusa_versioned_file_write (text_index->locations_index, &zero, sizeof (gint32), 1);
        text_index_add_mime_modules (text_index);

        text_index->ref_count = 1;

        if (locations_index_create_result != MEDUSA_VERSIONED_FILE_OK) {
                medusa_text_index_unref (text_index);
                return NULL;
        }
                
        if (text_index_files_are_ready (text_index) == FALSE) {
                g_warning ("Cannot open text index for reading\n");
                medusa_text_index_unref (text_index);
                return NULL;
        }


        return text_index;
}

static gint32                  
get_uri_number_from_temp_index_cell (MedusaTextIndex *text_index,
                                     int cell_number,
                                     int temp_index_number)
{
        gint32 uri_number;
        char * data_region;
        gpointer cell_location;

	g_assert (text_index->temp_index_io_handler[temp_index_number] != NULL);
        g_assert ((2 * cell_number * sizeof (gint32)) < 
                  text_index->temp_index_io_handler[temp_index_number]->mapped_size);
        data_region = medusa_io_handler_get_data_region (text_index->temp_index_io_handler[temp_index_number]);
        cell_location = data_region + 2 * cell_number * sizeof (gint32);
        memcpy (&uri_number, cell_location, sizeof (gint32));

        return uri_number;

}
static gint32                  
get_last_cell_from_temp_index_cell (MedusaTextIndex *text_index,
                                    int cell_number,
                                    int temp_index_number)
{
        gint32 last_cell_number;
        char *data_region; 
        gpointer cell_location;

	g_assert (text_index->temp_index_io_handler[temp_index_number] != NULL);
        g_assert ((2 * cell_number + 1) * sizeof (gint32) <
                  text_index->temp_index_io_handler[temp_index_number]->mapped_size);
        data_region = medusa_io_handler_get_data_region (text_index->temp_index_io_handler[temp_index_number]);
        cell_location = data_region + (2 * cell_number + 1) * sizeof (gint32);
        memcpy (&last_cell_number, cell_location, sizeof (gint32));
        return last_cell_number;

}

static char *
generate_temp_index_file_prefix (int file_number)
{
        return g_strdup_printf ("%s-%d", TEMP_FILE_PREFIX, file_number);
}

static char *
generate_temp_index_file_name (const char *index_name,
                               int file_number)
{ 
        char *temp_file_prefix;
        char *temp_filename;
                
        temp_file_prefix = generate_temp_index_file_prefix (file_number);
        temp_filename = medusa_generate_index_filename (temp_file_prefix, index_name, TRUE);
        g_free (temp_file_prefix);

        return temp_filename;
}

static gboolean
text_index_files_are_ready (MedusaTextIndex *text_index)
{
        /* Make sure the indexes are valid */
        /* FIXME bugzilla.eazel.com 2994: 
           Should there be a check for temp index stuff here? */
        return  text_index->words != NULL &&
                text_index->locations_index != NULL;

}



void
medusa_text_index_index_file (MedusaTextIndex *text_index,
                              const char *uri,
                              gint32 uri_number,
                              GnomeVFSFileInfo *file_info)
{
        MedusaTextIndexMimeModule *module;
        MedusaTextParsingFunc read_words;
        char **results;
        int i, number_of_words, index_number;
        guint32 last_cell_number;

        g_return_if_fail (text_index != NULL);
        medusa_log_event (MEDUSA_DB_LOG_EVERYTHING,
                          "Trying to index file %s with mime_type %s\n", 
                          uri, 
                          file_info->mime_type);
        module = medusa_text_index_mime_module_first_valid_module (text_index->mime_modules,
                                                                   file_info->mime_type);
        if (module == NULL) {
                return;
        }
        
        read_words = medusa_text_index_mime_module_get_parser (module);
        number_of_words = read_words (uri,
                                      &results,
                                      (gpointer) NULL);

        for (i = 0; i < number_of_words; i++) {
                /* Find the last location where we recorded
                   information about this word in the temp
                   index */
                g_assert (results[i] != NULL);
		
		/* mime-module must make a reasonable effort to provide words */
		if (100 < strlen (results[i])) {
			medusa_log_error ("mime_module sent a non-word: %s\n", g_strndup (results[i], 100));
			continue;
		}
		
		g_strdown (results[i]);
		
                index_number = g_str_hash (results[i]) % NUMBER_OF_TEMP_INDEXES;

                if (!medusa_lexicon_get_last_occurrence (text_index->words,
                                                         results[i],
                                                         &last_cell_number)) {
                        medusa_log_error ("Problem trying to lookup the last occurrence of the word %s\nfound in %s\n",
                                          g_strndup (results[i], 100), uri);
			continue;
                }
                g_assert (last_cell_number == MEDUSA_WORD_HAS_NO_PREVIOUS_OCCURRENCE ||
                          last_cell_number < text_index->current_cell_number[index_number]);
                fwrite (&uri_number,
                        sizeof (gint32),
                        1,
                        text_index->temp_index_stream[index_number]);
                fwrite (&last_cell_number,
                        sizeof (gint32),
                        1,
                        text_index->temp_index_stream[index_number]);

                if (!medusa_lexicon_insert (text_index->words,
                                            results[i],
                                            text_index->current_cell_number[index_number])) {
			// this should not be fatal
                        // medusa_log_fatal_error ("Updating the text index lexicon with word %s failed", results[i]);
                }
                text_index->current_cell_number[index_number]++;
                
        }
        medusa_log_event (MEDUSA_DB_LOG_EVERYTHING,
                          "%s\t%s\t%d\t%d\n",uri,file_info->mime_type, number_of_words, (int) file_info->size);
        /* Free the array itself and its word contents */
        array_of_words_free (results, number_of_words);

}

void
medusa_text_index_finish_indexing (MedusaTextIndex *text_index,
                                   gboolean use_idle_service,
                                   MedusaIdledConnection *idled_connection)
{
        /* We memory map the temporary indices for this section
           of the processing, since there is high locality of reference */

        close_temp_index_files (text_index);

        sort_temp_index_data_into_permanent_index (text_index, use_idle_service, idled_connection);
}

void                     
medusa_text_index_erase_constructed_index (const char *index_name)
{
        char *current_temp_file_prefix;
        int i;

        medusa_erase_constructed_index_file (LOCATION_FILE_PREFIX,
                                             index_name);
        medusa_lexicon_erase_constructed_index (index_name,
                                                TRUE);
        for (i = 0; i < NUMBER_OF_TEMP_INDEXES; i++) {
                current_temp_file_prefix = generate_temp_index_file_prefix (i);
                medusa_erase_constructed_index_file (current_temp_file_prefix,
                                                     index_name);
                g_free (current_temp_file_prefix);
        }
        
}

void                     
medusa_text_index_move_completed_index_into_place (const char *index_name)
{
        char *current_temp_file_prefix;
        int i;

        medusa_move_completed_index_file_into_place (LOCATION_FILE_PREFIX,
                                                     index_name);
        medusa_lexicon_move_completed_index_into_place (index_name);
        for (i = 0; i < NUMBER_OF_TEMP_INDEXES; i++) {
                current_temp_file_prefix = generate_temp_index_file_prefix (i);
                medusa_erase_constructed_index_file (current_temp_file_prefix,
                                                     index_name);
                g_free (current_temp_file_prefix);
        }
        
}

gboolean                 
medusa_text_index_files_are_still_valid (const char *index_name,
                                         time_t newest_valid_modified_time)
{
        /* FIXME: Should we inquire about the lexicon mod date here ? */
        return medusa_index_file_is_newer_than_time (LOCATION_FILE_PREFIX,
                                                      index_name,
                                                     newest_valid_modified_time);
}

static void
close_temp_index_files (MedusaTextIndex *index)
{
        int i;

        for (i = 0; i < NUMBER_OF_TEMP_INDEXES; i++) {
		if (index->temp_index_stream != NULL) {
			if (fclose (index->temp_index_stream[i]) == -1) {
				g_warning ("Could not close temp index file %s\n", index->temp_index_name[i]);
			}
			index->temp_index_stream[i] = NULL;
		}
        }
}

static void
mmap_temp_index_io_handler (MedusaTextIndex *index, int i)
{
	if (index->temp_index_io_handler[i] == NULL) {
                index->temp_index_io_handler[i] = medusa_io_handler_open (index->temp_index_name[i],
                                                                          MEDUSA_TEXT_INDEX_TEMP_FILE_MAGIC_NUMBER,
                                                                          MEDUSA_TEXT_INDEX_TEMP_FILE_VERSION_NUMBER,
									  MEDUSA_IO_HANDLER_PROT_READ);
	}
}

static void
munmap_temp_index_io_handler (MedusaTextIndex *index, int i)
{
	if (index->temp_index_io_handler != NULL) {
		medusa_io_handler_free (index->temp_index_io_handler[i]);
		index->temp_index_io_handler[i] = NULL;
	}
}

/* Interval is in seconds */
#define IDLE_CHECK_TIME_INTERVAL 2

static void
sleep_until_idle_when_idle_check_interval_has_passed (MedusaIdledConnection *idled_connection,
                                                      time_t *time_of_last_idle_check)
{
        time_t current_time;
        current_time = time (NULL);
        if ((current_time - *time_of_last_idle_check) > IDLE_CHECK_TIME_INTERVAL) {
                medusa_idle_service_sleep_until_idle (idled_connection);
                *time_of_last_idle_check = time (NULL);
        }

        return;
}

typedef struct {
        MedusaTextIndex *index;
        gboolean use_idled_service;
        MedusaIdledConnection *idled_connection;
        time_t time_of_last_idle_check;

} ForeachData;

static void                 
sort_temp_index_data_into_permanent_index (MedusaTextIndex *index,
                                           gboolean use_idled_service,
                                           MedusaIdledConnection *idled_connection)
{
        int j;
        ForeachData foreach_data;

        foreach_data.index = index;
        foreach_data.use_idled_service = use_idled_service;
        foreach_data.idled_connection = idled_connection;
	foreach_data.time_of_last_idle_check = 0;
        for (j = 0; j < NUMBER_OF_TEMP_INDEXES; j++) {
		mmap_temp_index_io_handler (index, j);
                medusa_lexicon_foreach_in_segment (index->words,
                                                   j, NUMBER_OF_TEMP_INDEXES,
                                                   add_word_to_real_index, &foreach_data);
		munmap_temp_index_io_handler (index, j);
        }
}


static void
add_word_to_real_index (const char *word,
                        const char *value,
                        gpointer user_data)
{
        MedusaTextIndex *text_index;
        guint32 last_cell_number;
        gint32 uri_number;
        int temp_index_number;
        guint32 start_location, end_location;
        ForeachData *foreach_data;

        medusa_lexicon_decode_last_occurrence (value, 
                                               &last_cell_number);
        g_assert (user_data != NULL);
        foreach_data = (ForeachData *) user_data;
        text_index = foreach_data->index;

        /* FIXME: We should precompute this number and store it in the text index */
        temp_index_number = g_str_hash (word) % NUMBER_OF_TEMP_INDEXES;

        medusa_log_event (MEDUSA_DB_LOG_EVERYTHING,
                          "going on to word %s at cell number %d\n", word, last_cell_number);
        start_location = text_index->reverse_index_position;
        medusa_log_event (MEDUSA_DB_LOG_EVERYTHING,
                          "inserting starting point for word %s at position %d\n",
                          word, text_index->reverse_index_position);
        uri_number = get_uri_number_from_temp_index_cell (text_index, 
                                                          last_cell_number, 
                                                          temp_index_number);
        while (last_cell_number != MEDUSA_WORD_HAS_NO_PREVIOUS_OCCURRENCE) {

                uri_number = get_uri_number_from_temp_index_cell (text_index, 
                                                                  last_cell_number, 
                                                                  temp_index_number);
                medusa_log_event (MEDUSA_DB_LOG_EVERYTHING,
                                  "Next occurrence of word %s is cell number %d, uri number %d\n", 
                                  word, last_cell_number, uri_number);
                write_uri_number_to_location_file (text_index, uri_number);
                text_index->reverse_index_position++;
                last_cell_number = get_last_cell_from_temp_index_cell (text_index, 
                                                                       last_cell_number,
                                                                       temp_index_number);
                         
        }
        medusa_log_event (MEDUSA_DB_LOG_EVERYTHING,
                          "inserting ending point for word %s at position %d\n",
                          word, text_index->reverse_index_position);
        end_location = text_index->reverse_index_position;
        medusa_lexicon_set_index_locations (text_index->words, 
                                            word, 
                                            start_location, 
                                            end_location);

        if (foreach_data->use_idled_service) {
                sleep_until_idle_when_idle_check_interval_has_passed (foreach_data->idled_connection,
                                                                      &foreach_data->time_of_last_idle_check);
                medusa_idle_service_sleep_until_idle (foreach_data->idled_connection);
        }
}

static void                 
write_uri_number_to_location_file (MedusaTextIndex *text_index,
                                   gint32 uri_number)
{
        MedusaVersionedFileResult result;
        result = medusa_versioned_file_write (text_index->locations_index, &uri_number, sizeof (gint32), 1);
        if (result != MEDUSA_VERSIONED_FILE_OK) {
                medusa_versioned_file_error_notify ("Error occurred while writing uri number to location file",
                                                    result);
        }
        
}


static void
array_of_words_free (char **array, int number_of_words)
{
        int i;
        for (i = 0; i < number_of_words; i++) {
                g_free (array[i]);
        }
        if (number_of_words > 0) {
                g_free (array);
        }
}

void
medusa_text_index_ref (MedusaTextIndex *text_index)
{
        text_index->ref_count++;
}

void
medusa_text_index_unref (MedusaTextIndex *text_index)
{
        g_assert (text_index->ref_count > 0);
        if (text_index->ref_count == 1) {
                medusa_text_index_destroy (text_index);
        }
        else {
                text_index->ref_count--;
        }
}

static void
text_index_add_mime_modules (MedusaTextIndex *text_index)
{
        MedusaTextIndexMimeModule *plaintext_indexer;
       
        /* Enable indexing of plain text files */
        plaintext_indexer = medusa_text_index_mime_module_new (medusa_text_index_parse_plaintext);
        medusa_text_index_mime_module_add_mime_pattern (plaintext_indexer,
                                                        "text/");
        /* bugzilla.eazel.com bug 1690:
           add option to index source code here */
        text_index->mime_modules = g_list_prepend (text_index->mime_modules,
                                                   plaintext_indexer);
        

}



static void
medusa_text_index_destroy (MedusaTextIndex *text_index)
{
        int i;

        medusa_lexicon_free (text_index->words);

        if (text_index->creating_index) {
                for (i = 0; i < NUMBER_OF_TEMP_INDEXES; i++) {
                        g_free (text_index->temp_index_name[i]);
                        if (text_index->temp_index_io_handler[i] != NULL) {
                                medusa_io_handler_free (text_index->temp_index_io_handler[i]);
                        }
			if (text_index->temp_index_stream[i] != NULL) {
                                /* We exited early, close the file pointers */
				fclose (text_index->temp_index_stream[i]);
                        }

                }
                g_list_foreach (text_index->mime_modules, 
                               medusa_text_index_mime_module_unref_cover,
                                NULL);
                g_list_free (text_index->mime_modules);
        }

        
        medusa_versioned_file_destroy (text_index->locations_index);

        g_free (text_index);
}

typedef struct {
        gint32 uri_number;
        MedusaTextIndex *text_index;
} IndexFileData;

static void 
index_file_cover (const char *file_uri,
                  gpointer data)
{
        GnomeVFSFileInfo file_info;
        GnomeVFSResult get_file_info_result;
        IndexFileData *index_file_data;
        
        g_assert (data != NULL);
        index_file_data = (IndexFileData *) data;

        get_file_info_result = gnome_vfs_get_file_info (file_uri,
                                                        &file_info,
                                                        GNOME_VFS_FILE_INFO_GET_MIME_TYPE);
        MEDUSA_TEST_INTEGER_RESULT (get_file_info_result, GNOME_VFS_OK);
        medusa_text_index_index_file (index_file_data->text_index,
                                      file_uri,
                                      index_file_data->uri_number++,
                                      &file_info);
}

void
medusa_text_index_self_check (void)
{
        MedusaTextIndex *text_index;
        IndexFileData index_file_data;
        char *directory_uri;
        int i, j;
        gint32 last_uri_number, new_uri_number, back_pointer;

        g_print ("Self checking text index...\n");
        medusa_text_index_erase_constructed_index ("text-index-self-check");

        /* Build a text index and get the locations of each word */
        text_index = medusa_text_index_new ("text-index-self-check");
        index_file_data.text_index = text_index;

        index_file_data.uri_number = 0;

        directory_uri = gnome_vfs_get_uri_from_local_path (MEDUSA_SOURCEDIR);
        MEDUSA_TEST_BOOLEAN_RESULT (directory_uri != NULL);
        medusa_foreach_uri_in_directory_recursive (directory_uri,
                                                   index_file_cover,
                                                   &index_file_data);
        g_free (directory_uri);

        
        g_print ("Testing representation invariants of the temporary indices are satisfied");
	close_temp_index_files (text_index);
        for (i = 0; i < NUMBER_OF_TEMP_INDEXES; i++) {
		mmap_temp_index_io_handler (text_index, i);
                g_print (".");
                last_uri_number = 0;
                for (j = 1; j < text_index->current_cell_number[i]; j++) {
                        /* Check that the uri numbers go in increasing
                           order */
                        new_uri_number = 
                                get_uri_number_from_temp_index_cell (text_index,
                                                                     j, 
                                                                     i);
                        MEDUSA_TEST_BOOLEAN_RESULT (new_uri_number >= 
                                                    last_uri_number);
                        last_uri_number = new_uri_number;
                        
                        /* Check that each last cell (back pointer)
                           points to a cell smaller than the current
                           one */
                        back_pointer = 
                                get_last_cell_from_temp_index_cell (text_index,
                                                                    j,
                                                                    i);
                        MEDUSA_TEST_BOOLEAN_RESULT (back_pointer == MEDUSA_WORD_HAS_NO_PREVIOUS_OCCURRENCE ||
                                                    back_pointer < j);
                }
		munmap_temp_index_io_handler (text_index, i);
        }
        g_print ("\n");

        /* Check for a few unique words? */ 
}
