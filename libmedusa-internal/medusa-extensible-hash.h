/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  medusa-extensible-hash.h - Medusa implementation of an on-disk
 *  extensible hash.
 *
 *  Copyright (C) 2001 Eazel, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@eazel.com>
 * 
 *  medusa-extensible-hash.h - Implementation of an on disk extensible
 *  hash table, based on the description in G.H. Gonnet and R. Baeza-Yates'
 * "Handbook of Algorithms and Data Structures"
 */

#ifndef MEDUSA_EXTENSIBLE_HASH_H
#define MEDUSA_EXTENSIBLE_HASH_H

typedef struct MedusaExtensibleHash MedusaExtensibleHash;

typedef void (*MedusaExtensibleHashForeachFunc) (const char *key,
                                                 const char *value,
                                                 gpointer    user_data);

MedusaExtensibleHash  *  medusa_extensible_hash_new               (const char             *index_name,
                                                                   const char             *hash_name);
MedusaExtensibleHash  *  medusa_extensible_hash_open              (const char             *index_name,
                                                                   const char             *hash_name);

/* Inserts a new item, or replaces the value for the item if the entry already
   exists in the hash.  Because the hash is disk based, no reference is kept
   to either the inserted key or the inserted value */
gboolean                 medusa_extensible_hash_insert            (MedusaExtensibleHash   *hash,
                                                                   const char             *key,
                                                                   const char             *data);
gboolean                 medusa_extensible_hash_lookup            (MedusaExtensibleHash   *hash,
                                                                   const char             *key,
                                                                   gboolean               *key_exists,
                                                                   char                  **data);
gboolean                 medusa_extensible_hash_remove            (MedusaExtensibleHash   *hash,
                                                                   const char             *key);
/* Frees the in memory reference to the on disk data structure, and writes sections of the
 hash kept only in memory to disk */
gboolean                 medusa_extensible_hash_free              (MedusaExtensibleHash   *hash);
/* Removes the on disk portion of the hash */
gboolean                 medusa_extensible_hash_erase_constructed_index            (const char             *index_name,
                                                                                    const char             *hash_name,
                                                                                    gboolean               unlink_in_progress_index);
gboolean                 medusa_extensible_hash_move_completed_index_into_place    (const char             *index_name,
                                                                                    const char             *hash_name);


/* Iterate over 1 nth of the words, where n is the total number of segments.
   We use this to go over the temporary text index files at the end of the text indexing process */
gboolean                 medusa_extensible_hash_foreach           (MedusaExtensibleHash                    *lexicon,
                                                                   MedusaExtensibleHashForeachFunc          function,
                                                                   gpointer                                 function_data);


void                     medusa_extensible_hash_self_check        (void);

#endif /* MEDUSA_EXTENSIBLE_HASH_H */
