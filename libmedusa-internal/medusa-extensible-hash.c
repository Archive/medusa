/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  medusa-extensible-hash.c - Medusa implementation of an on-disk
 *  extensible hash.
 *
 *  Copyright (C) 2001 Eazel, Inc., Rebecca Schulman
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@eazel.com>
 *
 *  medusa-extensible-hash.c - Implementation of an on disk extensible
 *  hash table, based on the description in G.H. Gonnet and R. Baeza-Yates'
 * "Handbook of Algorithms and Value Structures"
 */

#ifdef HAVE_GETPAGESIZE
#include <unistd.h>
#endif

#include <config.h>
#include <dirent.h>
#include <fcntl.h>
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* For memcpy */
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>


#include <libmedusa/medusa-log.h>
#include <libmedusa/medusa-stdio-extensions.h>
#include <libmedusa/medusa-string.h>
#include <libmedusa/medusa-test.h>
#include <libmedusa/medusa-utils.h>
#include <libmedusa/medusa-index-filenames.h>
#include "medusa-io-handler.h"

#include "medusa-extensible-hash.h"

#define DIRECTORY_DESCRIPTION     "directory"
#define DIRECTORY_MAGIC_NUMBER    "912\124"
#define DIRECTORY_VERSION_NUMBER  "0.5"


#define PAGE_FILE_DESCRIPTION     "page-file"
#define PAGE_FILE_MAGIC_NUMBER    "912\125"
#define PAGE_FILE_VERSION_NUMBER  "0.5"

#define BUCKET_SIZE_DEFAULT 1024
#define KEY_AND_VALUE_SEPARATOR '|'
#define BUCKET_FILE_HEADER_SIZE 4096

#define KEEP_BUCKET_DATA_IN_MEMORY 1
#define BUCKETS_TO_MAP_AT_ONCE 32

static guint bucket_size = 0;


typedef struct {
        char *value;
} Bucket;

typedef gboolean  (* BucketForeachFunc)     (const char *key,
                                             const char *value,
                                             gpointer    user_value,
                                             gpointer    additional_user_value);

typedef gboolean (* BucketKeyValueTestFunc)  (const char *key,
                                              const char *value,
                                              gconstpointer    user_value);


typedef struct {
        char *filename;
        guint *entries;
        guint depth;
        gboolean is_empty;
        gboolean data_is_in_memory;
        FILE *entries_file;
} Directory;

struct MedusaExtensibleHash {
        Directory *directory;
        char *bucket_filename;
        FILE *bucket_file_pointer;
        GSList *bucket_data; /* Of char * */
        int bucket_file_descriptor;
        gboolean is_empty;
        guint32 number_of_buckets;
        gboolean buckets_are_memory_mapped;
};

static char *
directory_generate_basename (const char *hash_name)
{
        if (hash_name) {
                return g_strdup_printf ("%s-%s", hash_name, 
                                        DIRECTORY_DESCRIPTION);
        }
        else {
                return g_strdup (DIRECTORY_DESCRIPTION);
        }
}


/* directory_new
 * 
 * Returns a new directory object that stores value at the path given by 
 * the directory file name.  
 *
 * @index_name                     The name of the index that this extensible hash is 
 *                                 being used for
 * @hash_name                      The name of the extensible hash within this index
 *
 * @Return value                   Returns a new directory object.  If an I/O error
 *                                 occurred during object creation, returns NULL
 */
static Directory *
directory_new (const char *index_name,
               const char *hash_name)
{
        Directory *directory;
        char *directory_file_base;
        
        directory = g_new0 (Directory, 1);
        
        directory->entries = NULL;        
        directory->is_empty = TRUE;
        directory_file_base = directory_generate_basename (hash_name);
        directory->filename = medusa_generate_index_filename (directory_file_base,
                                                              index_name,
                                                              TRUE);
        g_free (directory_file_base);
        directory->data_is_in_memory = TRUE;
        return directory;
}

/* directory_free
 *
 * Frees a directory allocated as part of an extensible hash
 *
 * @directory         The directory to be freed
 */
static void
directory_free (Directory *directory)
{
        g_assert (directory != NULL);

        g_free (directory->filename);
        g_free (directory->entries);
        g_free (directory);
}


/* directory_open
 *
 * Returns a directory object pointed to an opened directory.
 * Assumes directory data should be kept on disk.
 *
 * @index_name                     The name of the index that this extensible hash is 
 *                                 being used for
 * @hash_name                      The name of the extensible hash within this index
 *
 * @Return value                   Returns a new directory object.  If an I/O error
 *                                 occurred while opening the directory, returns NULL
 */
static Directory *
directory_open (const char      *index_name,
                const char      *hash_name)
{
        Directory *directory;
        char *directory_file_base;
        
        directory = g_new0 (Directory, 1);
        
        directory->entries = NULL;        

        directory_file_base = directory_generate_basename (hash_name);
        directory->filename = medusa_generate_index_filename (directory_file_base,
                                                              index_name,
                                                              FALSE);

        g_free (directory_file_base);
        
        
        directory->data_is_in_memory = FALSE;
        directory->entries_file  = medusa_fopen_error_cover (directory->filename,
                                                             "r+");        
        if (directory->entries_file == NULL) {
                directory_free (directory);
                return NULL;
        }

        directory->is_empty = medusa_get_file_size (directory->filename) == 0;

        if (!directory->is_empty) {
                medusa_fread_error_cover (&directory->depth, 
                                          sizeof (guint), 1,
                                          directory->entries_file,
                                          directory->filename,
                                          TRUE);
        }

        return directory;
}

/* directory_move_completed_into_place
 * 
 * Moves a directory file over an old index when it is done being created
 *
 * @index_name                The name of the index the directory is located in
 * @hash_name                 The name of the particular hash within the index
 *
 * @Return Value              Returns TRUE if all I/O related to unlinking was
 *                            completed successfully, FALSE otherwise.
 */
static gboolean
directory_move_completed_into_place (const char *index_name,
                                     const char *hash_name)
{
        char *directory_file_base;
        
        directory_file_base = directory_generate_basename (hash_name);
        /* FIXME: this should return an error if there are I/O problems */
        medusa_move_completed_index_file_into_place (directory_file_base,
                                                     index_name);
        g_free (directory_file_base);

        return TRUE;
}



/* directory_erase_constructed_index
 *
 * Unlinks disk based structures attached to this directory.
 *
 * @index_name                The name of the index the directory is located in
 * @hash_name                 The name of the particular hash within the index
 * @unlink_in_progress_index  Pass TRUE if the directory to be removed was part
 *                            of an index in progress, FALSE otherwise
 *
 * @Return Value         Returns TRUE if all I/O related to unlinking was
 *                       completed successfully, FALSE otherwise.
 */
static gboolean
directory_erase_constructed_index (const char   *index_name,
                                   const char   *hash_name,
                                   gboolean      unlink_in_progress_index)
{
        char *directory_file_base, *directory_filename;
        gboolean unlink_result;

        directory_file_base = directory_generate_basename (hash_name);
        directory_filename = medusa_generate_index_filename (directory_file_base,
                                                             index_name,
                                                             unlink_in_progress_index);

        unlink_result = medusa_unlink_if_present (directory_filename);

        g_free (directory_file_base);
        g_free (directory_filename);

        return unlink_result;
}

static gboolean
directory_is_empty (Directory *directory)
{
        return directory->is_empty;
} 

static gboolean
directory_write_to_disk (Directory *directory)
{
        FILE *directory_file_stream;

        /* Write out just the directory depth if the directory was never loaded into memory */
        if (!directory->data_is_in_memory) {
                if (!medusa_fseek_error_cover (directory->entries_file,
                                               0, SEEK_SET,
                                               directory->filename)) {
                        return FALSE;
                }
                if (!medusa_fwrite_error_cover (&directory->depth,
                                                sizeof (guint), 1,
                                                directory->entries_file,
                                                directory->filename)) {
                                return FALSE;
                }
                return medusa_fclose_error_cover (directory->entries_file,
                                                  directory->filename);
        }

        if (access (directory->filename, F_OK) == 0) {
                medusa_log_error ("File where extensible hash directory file %s should be written already exists\n",
                                  directory->filename);
                return FALSE;
        }

        /* FIXME: Is this a race condition?  Maybe we should just claim to
           overwrite the directory file if it is already there.. */
        if (directory_is_empty (directory)) {
                /* Create a 0 length file */
                return medusa_create_file (directory->filename) && 
                        (chmod (directory->filename, S_IRUSR | S_IWUSR) == 0);
        }
        
        directory_file_stream = medusa_fopen_error_cover (directory->filename, "w");
        if (directory_file_stream == NULL) {
                return FALSE;
        }
        
        if (!medusa_fwrite_error_cover (&directory->depth, sizeof (guint), 
                                        1, directory_file_stream, directory->filename)) {
                return FALSE;
        }
        if (!medusa_fwrite_error_cover (directory->entries, sizeof (guint), 
                                        1 << directory->depth, directory_file_stream,
                                        directory->filename)) {
                return FALSE;
        }


        if (!medusa_fclose_error_cover (directory_file_stream,
                                        directory->filename)) {
                return FALSE;
        }

        return TRUE;
}

/* directory_get_depth 
 *
 * Returns the depth of the directory argument; 
 * assume directory is not empty
 *
 * @directory         The directory to get depth for
 *
 * @Return value      The depth
 */
/* FIXME: Should this return a gboolean like the other functions? */
static guint
directory_get_depth (Directory *directory)
{
        g_assert (!directory_is_empty (directory));

        return directory->depth;
}

static guint
directory_get_number_of_entries (Directory *directory)
{
        g_assert (!directory_is_empty (directory));

        return 1 << directory->depth;
}

static void
directory_depth_increment (Directory *directory)
{
        directory->depth++;
}

/* directory_entry_set
 *
 * Sets the directory entry number to point to a specific bucket.
 * Buckets are also numbered, so the directory 
 * is a mapping of integer directory entry numbers to integer
 * identifiers for buckets.  Aborts if the directory entry
 * to set is larger than the number of entries in the directory.
 *
 * @directory         The directory to change an entry for
 * @entry_number      The directory entry number
 * @bucket_number     The bucket identifier that the entry number should point to
 *
 * @Return value      Returns TRUE if no I/O was necesssary or if all I/O
 *                    associated with the operation was successful
 */
/* FIXME: This function is inconsistent with other directory
   functions -- should it return a boolean? */
static gboolean
directory_entry_set (Directory    *directory,
                     guint         entry_number,
                     guint         bucket_number)
{
        if (directory->data_is_in_memory) {
                directory->entries[entry_number] = bucket_number;
        }
        else {
                if (!medusa_fseek_error_cover (directory->entries_file, 
                                               (entry_number + 1) * sizeof (guint), 
                                               SEEK_SET,
                                               directory->filename)) {
                        return FALSE;
                }
                if (!medusa_fwrite_error_cover (&bucket_number,
                                                sizeof (guint),
                                                1,
                                                directory->entries_file,
                                                directory->filename)) {
                        return FALSE;
                }
                
        }

        return TRUE;
}

/* directory_key_hash_to_entry_number
 *
 * Takes a directory and a key, and returns the entry number in the directory
 * that the key would correspond to.
 *
 * @directory      The directory the key should be hashed into
 * @key            The key
 *
 * @Return value   The entry number for the key in this directory
 */
static guint
directory_key_hash_to_entry_number (Directory    *directory,
                                    const char   *key)
{
        g_assert (directory != NULL);

        return g_str_hash (key) & (directory_get_number_of_entries (directory) - 1);
}


static void
set_bucket_size (void)
{
        if (bucket_size == 0) {
#ifdef HAVE_GETPAGESIZE
                bucket_size = getpagesize ();
#else
                /* FIXME: Might there be a better algorithm, and/or alternative function that could be used? */
                medusa_log_event (MEDUSA_DB_LOG_ABBREVIATED,
                                  "No getpagesize function available, using default bucket size of %d for extensible hash buckets",
                                  BUCKET_SIZE_DEFAULT);
                bucket_size = BUCKET_SIZE_DEFAULT;
#endif
        }
 
}

/* generate_bucket_filename_without_number
 * 
 * Returns the file name where the buckets are stored
 *
 * @hash_name                      The name of the extensible hash within this index
 * @index_name                     The name of the index that this extensible hash is 
 *                                 being used for
 * @index_is_in_progress           Whether the name should indicate that the file is part
 *                                 of an index under construction
 * 
 */
static char *
generate_bucket_filename (const char *hash_name,
                          const char *index_name,
                          gboolean    index_is_in_progress)
{
        char *page_file_base, *bucket_filename;

        if (hash_name) {
                page_file_base = g_strdup_printf ("%s-%s", hash_name, 
                                                  PAGE_FILE_DESCRIPTION);
        }
        else {
                page_file_base = g_strdup (PAGE_FILE_DESCRIPTION);
        }

        bucket_filename = medusa_generate_index_filename (page_file_base,
                                                          index_name,
                                                          index_is_in_progress);
        g_free (page_file_base);
        
        return bucket_filename;
}


/* buckets_erase_file
 *
 * Erase bucket file
 *
 * @index_name                The name of the index the directory is located in
 * @hash_name                 The name of the particular hash within the index
 * @unlink_in_progress_index  Pass TRUE if the directory to be removed was part
 *                            of an index in progress, FALSE otherwise
 *
 * @Return Value              Returns TRUE if all I/O related to unlinking was
 *                            completed successfully, FALSE otherwise.
 */
static gboolean
buckets_erase_file (const char   *index_name,
                    const char   *hash_name,
                    gboolean      unlink_in_progress_index)
{
        char *buckets_filename;
        gboolean unlink_result;

        buckets_filename = generate_bucket_filename (hash_name,
                                                     index_name,
                                                     unlink_in_progress_index);
        unlink_result = medusa_unlink_if_present (buckets_filename);

        g_free (buckets_filename);

        return unlink_result;
}


/* medusa_extensible_hash_erase_constructed_index
 *
 * Removes the files used by an extensible hash value structure from
 * disk. This is different than freeing the extensible hash value structure
 * which only frees the value structure containing references to the on
 * disk information, and any virtual memory used to map pieces of the on disk
 * value structure.
 *
 * @index_name              The name of the index the hash is being removed from
 * @hash_name               The name of the hash to remove
 * (These arguments are the same as those passed to extensible_hash_new)
 *
 * Return value             Returns TRUE if all I/O related to unlinking
 *                          related files was successful, FALSE otherwise
 *                          If FALSE is returned, it is not determined whether
 *                          none, some, or all of the index files have been
 *                          removed.
 */
gboolean
medusa_extensible_hash_erase_constructed_index (const char      *index_name,
                                                const char      *hash_name,
                                                gboolean         unlink_in_progress_index)
{
        if (!directory_erase_constructed_index (index_name, 
                                                hash_name, 
                                                unlink_in_progress_index)) {
                return FALSE;
        }

        if (!buckets_erase_file (index_name,
                                 hash_name,
                                 unlink_in_progress_index)) {
                return FALSE;
        }

        return TRUE;
}


/* get_bucket_free_space
 * Takes a bucket and finds the amount of free available in the read bucket.
 * A bucket for which a read has been successful is required 
 *
 * @bucket       The bucket to find free space for
 * @free_space   Receives the amount of free space the bucket contains 
 *
 * @return_value Returns TRUE if the bucket recorded a valid amount of 
 *               free space, FALSE otherwise
 * 
 */
static gboolean
get_bucket_free_space (Bucket    *bucket,
                       guint16   *free_space)
{
        g_assert (bucket != NULL);

        /* We store the bucket's free space in the first 2 bytes of the bucket */
        memcpy (free_space, bucket->value, sizeof (guint16));

        if (*free_space > bucket_size) {
                medusa_log_error ("Corrupt amount of free space discovered in an "
                                  "extensible hash bucket.  Bucket should have free "
                                  "space between 0 and %d, and has free space %d\n", 
                                  bucket_size, *free_space);
                return FALSE;
        }
                
        return TRUE;
}

/* initialize_bucket_free_space
 * 
 * Takes a newly created bucket and sets the free space
 * to be the size of the bucket, minus the size of the meta-information 
 * 
 * @bucket               The bucket to set the free space for 
 */
static void
initialize_bucket_free_space (Bucket *bucket)
{
        guint16 initial_free_space;

        initial_free_space = bucket_size - 2 * sizeof (guint16);
        
        memcpy (bucket->value, &initial_free_space, sizeof (guint16));
}
                              

/* adjust_bucket_free_space
 *
 * Takes a bucket, and the number of characters the unused space in the
 * bucket has changed by, up or down.  A negative argument will decrease the amount 
 * of free space in the bucket, and a positive value will increase the amount of 
 * free space by the magnitude of the size argument.  Will not accept a change that 
 * is not possible to have changed by (e.g. a negative value less than the
 * amount of space left in the bucket) 
 *
 * @bucket               The bucket to change free space for
 * @size_changed         The number of characters bucket free space has changed 
 *                       by, either positive (more free space now available), or 
 *                       negative (less free space now available). 
 */
static void
adjust_bucket_free_space (Bucket *bucket,
                          int     size_change)
{
        guint16 current_free_space;

        g_assert (bucket != NULL);
        g_assert (get_bucket_free_space (bucket, &current_free_space));

        g_assert (current_free_space + size_change >= 0);
        g_assert (size_change >= 0 || (current_free_space + size_change) < bucket_size);
        g_assert (size_change <= 0 || (current_free_space + size_change) >= 0);

        current_free_space += size_change;
        memcpy (bucket->value, &current_free_space, sizeof (guint16));
}

/* bucket_get_depth
 * Takes a bucket and returns its depth.
 *
 * Depth of the directory in an extensible hash is a representation of the number
 * of entries in the directory, where the number of entries is 1<<depth.
 *
 * Depth of a bucket is always less than or equal to the depth of the
 * directory.  The depth of a bucket is inversely proportional to the
 * number of entries in the directory that point to it. The larger the
 * difference between the depth of the directory and the depth of a
 * bucket, the more directory entries that point to this particular
 * bucket.
 *
 * @bucket       The bucket to find free space for
 * @depth        Receives the depth of the current bucket
 *
 * @return_value Returns TRUE if the bucket recorded a valid depth
 *               FALSE otherwise
 */
static gboolean
bucket_get_depth (Bucket    *bucket,
                  guint16   *depth,
                  guint      directory_depth)
{
        /* We store the bucket's free space in bytes 3-4 of the bucket */
        memcpy (depth, bucket->value + sizeof (guint16), sizeof (guint16));

        if (*depth > directory_depth) {
                medusa_log_error ("Corrupt bucket depth discovered in an "
                                  "extensible hash bucket.  Bucket should have depth "
                                  "between 0 and %d, and has depth %d\n", 
                                  directory_depth, *depth);
                return FALSE;
        }
                
        return TRUE;
}

/* bucket_set_depth
 *
 * Takes a bucket, and sets the depth of the bucket. Does not check whether the
 * the depth is being upped to an invalid value
 *
 * @bucket               The bucket to change depth for
 * @depth                The depth to set to
 *
 * No return value
 */
static void
bucket_set_depth (Bucket    *bucket,
                  guint16    depth)
{
        memcpy (bucket->value + sizeof (guint16), &depth, sizeof (guint16));
}


/* bucket_new
 * 
 * Returns a newly allocated bucket, containing no value
 *
 * @Return value    The new bucket 
 */
static Bucket *
bucket_new (void)
{
        Bucket *new_bucket;

        new_bucket = g_new (Bucket, 1);
        new_bucket->value = g_new0 (char, bucket_size);
        bucket_set_depth (new_bucket, 0);
        initialize_bucket_free_space (new_bucket);
        
        return new_bucket;
}


static void
bucket_free (Bucket *bucket)
{
        g_assert (bucket != NULL);

        g_free (bucket->value);
        g_free (bucket);
}

/* In the future it might be possible to use this to
   compress the value structure.  Note that compressing a 
   large on disk structure may improve performance, because
   I/O is more expensive than computation time.  However, for
   one block reads this probably doesn't matter. */
static char *
key_value_pair_new (const char *key,
                    const char *value)
{
        g_assert (key != NULL);
        
        if (value) {
                return g_strdup_printf ("%s%c%s", key, KEY_AND_VALUE_SEPARATOR, value);
        }
        
        return g_strdup (key);
}

/* key_value_pair_length
 *
 * Calculates the length of a stored element consisting of the argument key and 
 * value in a bucket.  The length includes the terminating '\0'.  Value may be 
 * NULL
 *
 * @key                 The argument key
 * @value               The argument value
 *
 * @Return Value        The length of the key + value pair's stored format, plus the
 *                      terminating '\0'
 */
static int
key_value_pair_length (const char *key,
                       const char *value)
{
        g_assert (key != NULL);

        if (value) {
                return strlen (key) + strlen (value) + 2;
        }
        
        return strlen (key) + 1;
}
                         
static char *
map_new_block (MedusaExtensibleHash *hash,
               int link_number)
{
        size_t offset;
        char *new_block;
        int i;
        Bucket *bucket;
        char zero = 0;

        offset = BUCKET_FILE_HEADER_SIZE + 
                link_number * BUCKETS_TO_MAP_AT_ONCE * bucket_size;
        if (lseek (hash->bucket_file_descriptor,
                   offset + BUCKETS_TO_MAP_AT_ONCE * bucket_size,
                   SEEK_SET) == -1) {
                medusa_log_error ("Seek in bucket file %s failed before "
                                  "writing single zero failed",
                                  hash->bucket_filename);
                return NULL;
        }
        if (write (hash->bucket_file_descriptor,
                   &zero, sizeof (char)) == -1) {
                medusa_log_error ("Write of 0 to bucket file %s failed",
                                  hash->bucket_filename);
                return NULL;
        }
        new_block = mmap (NULL,
                          bucket_size * 
                          BUCKETS_TO_MAP_AT_ONCE,
                          PROT_READ | PROT_WRITE,
                          MAP_SHARED,
                          hash->bucket_file_descriptor,
                          offset);
        
        if (GPOINTER_TO_INT (new_block) == -1) {
                medusa_log_fatal_error ("Bucket block mmap of bucket link %d "
                                        "failed", link_number);
        }

        bucket = bucket_new ();
        for (i = 0 ; i < BUCKETS_TO_MAP_AT_ONCE; i++) {
                memcpy (new_block + i * bucket_size,
                        bucket->value,
                        bucket_size);
        }
        bucket_free (bucket);


        return new_block;
}

/* bucket_read
 * Takes an extensible hash, and returns the value in
 * bucket n as a Bucket structure.
 *
 * @hash            The hash to read the bucket from
 * @bucket_number   The bucket number
 *
 * @return_value    The bucket, read from the disk structure, or NULL if an I/O 
 *                  error occurs that prevents reading the bucket correctly.
 */
static Bucket *
bucket_read (MedusaExtensibleHash *hash,
             int                   bucket_number)
{
        Bucket *bucket;

        if (hash->is_empty) {
                return bucket_new ();
        }

        bucket = bucket_new ();

        if (hash->buckets_are_memory_mapped) {
                GSList *block_link;
                char *mapped_bucket;

                block_link = g_slist_nth (hash->bucket_data,
                                          bucket_number / BUCKETS_TO_MAP_AT_ONCE);
                if (block_link == NULL) {
                        memset (bucket->value, 0, bucket_size);
                }
                else {
                        mapped_bucket = block_link->data;
                        g_assert (mapped_bucket != NULL);
                        memcpy (bucket->value, 
                                mapped_bucket +
                                bucket_size * 
                                (bucket_number % BUCKETS_TO_MAP_AT_ONCE),
                                bucket_size);
                }
        }
        else {
                if (medusa_fseek_error_cover (hash->bucket_file_pointer, 
                                              BUCKET_FILE_HEADER_SIZE + 
                                              bucket_number * bucket_size,
                                              SEEK_SET,
                                              hash->bucket_filename) == FALSE) {
                        goto error_return;
                }
                
                /* FIXME: Partial reads shouldn't be an error, because
                   buckets may be empty, in which case we won't read
                   anything.  But then this will not catch file errors
                   where say, 512 bytes only are read which should
                   never happen */
                
                if (medusa_fread_error_cover (bucket->value, 
                                              sizeof (char), 
                                              bucket_size, 
                                              hash->bucket_file_pointer, 
                                              hash->bucket_filename, 
                                              FALSE) == FALSE) {
                        goto error_return;
                }
        }
                
        return bucket;

 error_return:
        bucket_free (bucket);
        return NULL;
}

/* bucket_write 
 *
 * Write a bucket to disk, or to the memory mapped blocks
 * 
 * @hash            The hash the bucket belongs to
 * @bucket          The bucket to be written
 * @bucket_number   The bucket number of the bucket to be written to disk
 *
 * @Return value    Returns TRUE if the write succeeded, FALSE otherwise 
 */
static gboolean
bucket_write (MedusaExtensibleHash *hash,
              Bucket               *bucket,
              guint                 bucket_number)
{
        if (hash->buckets_are_memory_mapped) {
                GSList *block_link;
                char *block;
                
                block_link = g_slist_nth (hash->bucket_data,
                                          bucket_number / 
                                          BUCKETS_TO_MAP_AT_ONCE);
                if (block_link == NULL) {
                        /* Because we only add one bucket at a time
                           to the number of buckets, we should only
                           be one link in the list of bucket blocks 
                           short.  If we're more than this, we've
                           received a bogus bucket_number, or previous
                           buckets were never written.  Either way,
                           fail. */
                        g_assert (g_slist_length (hash->bucket_data) ==
                                  bucket_number / BUCKETS_TO_MAP_AT_ONCE);

                        block = map_new_block (hash,
                                               bucket_number / 
                                               BUCKETS_TO_MAP_AT_ONCE);

                        hash->bucket_data = 
                                g_slist_append (hash->bucket_data,
                                                block);
                }
                else {
                        block = block_link->data;
                }

                memcpy (block + 
                        bucket_size * 
                        (bucket_number % BUCKETS_TO_MAP_AT_ONCE),
                        bucket->value,
                        bucket_size);
        }
        else {
                if (!medusa_fseek_error_cover (hash->bucket_file_pointer, 
                                               BUCKET_FILE_HEADER_SIZE + 
                                               bucket_number * bucket_size,
                                               SEEK_SET,
                                               hash->bucket_filename)) {
                        return FALSE;
                }
                
                if (!medusa_fwrite_error_cover (bucket->value, 
                                                sizeof (char), 
                                        bucket_size, 
                                                hash->bucket_file_pointer, 
                                                hash->bucket_filename)) {
                        return FALSE;
                }
                
                
                if (fflush (hash->bucket_file_pointer) != 0) {
                        return FALSE;
                }
        }

        return TRUE;
}

 

/* get_bucket_value_start_and_end_points
 *
 * Given a bucket, return the points to the beginning and ending of the key / value
 * value itself.  The beginning and ending of this value may not be the beginning and
 * ending of the bucket itself because metavalue may be stored in the bucket as well.
 *
 * @bucket                      The bucket to find value in
 * @bucket_value_starting_point  The beginning of the bucket's key / value value
 * @bucket_value_ending_point    Receives the location of the end of the bucket's key / value value
 *
 */
static void
get_bucket_value_start_and_end_points (Bucket       *bucket,
                                      const char  **bucket_value_starting_point,
                                      const char  **bucket_value_ending_point)
{
        guint16 bucket_free_space;

        g_assert (get_bucket_free_space (bucket, &bucket_free_space));

        *bucket_value_ending_point = bucket->value + bucket_size - bucket_free_space;

        /* The first value in the bucket is two 16 bit integers: 
           the bucket free space and the bucket depth */
        *bucket_value_starting_point = bucket->value + 2 * sizeof (guint16);
}


/* bucket_contains_key
 * Takes a bucket and a key, and determines whether
 * a key / value pair with the given key is already stored in
 * the bucket.  Does not check for any level of bucket integrity
 * (rejects NULL buckets, and buckets containing no value)
 * Relies on "key|value" format of key/value pairs
 *
 * @bucket              Bucket to search
 * @key                 Key to search for
 * @value               Stores value corresponding to key, or NULL if no value
 *                      is stored for this key.  If passed value for value is NULL,
 *                      then the address of the value is not returned.
 * 
 * @return_value        Returns the location of the key in the bucket, as a const char *
 *                      the key if present, NULL otherwise.
 */
static char *
bucket_contains_key (Bucket      *bucket,
                     const char  *key,
                     char **value)
{
        const char *bucket_value;
        const char *bucket_value_ending_point;
        char *bucket_location;

        g_assert (bucket != NULL);
        g_assert (bucket->value != NULL);
        g_assert (key != NULL);
        
        get_bucket_value_start_and_end_points (bucket,
                                              &bucket_value,
                                              &bucket_value_ending_point);
        for (bucket_location = (char *) bucket_value ;
             bucket_location < bucket_value_ending_point ;
             bucket_location += strlen (bucket_location) + 1) {
                if (medusa_str_has_prefix (bucket_location, key)) {
                        if (*(bucket_location + strlen (key)) == KEY_AND_VALUE_SEPARATOR) {
                                if (value) {
                                        *value = bucket_location + strlen (key) + 1;
                                }
                                return bucket_location;
                        }
                        else if (*(bucket_location + strlen (key)) == 0) {
                                if (value) {
                                        *value = NULL;
                                }
                                return bucket_location;
                        }
                }
        }

        return NULL;
}


/* medusa_extensible_hash_new
 *
 * Creates a new extensible hash object, for a hash in progress.
 * Creates an initial bucket for the hash on disk.
 * A non NULL index name and hash name must be given as arguments,
 * but either value may have length 0.
 *
 * @index_name      The name of the index (may be NULL)
 * @hash_name       The name of the extensible hash within the index (may be "")
 *
 * Return value     A new extensible hash object, or NULL if 
 *                  removing files that may conflict with the hash fails
 *                  or other necessary I/O fails
 *                  
 */
MedusaExtensibleHash  *  
medusa_extensible_hash_new               (const char *index_name,
                                          const char *hash_name)
{
        MedusaExtensibleHash *hash;

        if (medusa_extensible_hash_erase_constructed_index (index_name,
                                                            hash_name,
                                                            TRUE) == FALSE) {
                medusa_log_error ("Could not create new hash object, "
                                  "because files that would conflict with "
                                  "the new hash's on disk value could not "
                                  "be removed. "
                                  "This value could be an old hash owned "
                                  "by a different user.");
                return NULL;
        }

        hash = g_new0 (MedusaExtensibleHash, 1);
        hash->bucket_filename = generate_bucket_filename (hash_name,
                                                          index_name,
                                                          TRUE);

        if (!medusa_create_file (hash->bucket_filename) ||
            chmod (hash->bucket_filename, S_IRUSR | S_IWUSR) != 0) {
                g_free (hash->bucket_filename);
                g_free (hash);
                return NULL;
        }
        
        set_bucket_size ();

#ifdef KEEP_BUCKET_DATA_IN_MEMORY
        {
                char *first_bucket_block;

                hash->buckets_are_memory_mapped = TRUE;
                hash->bucket_file_descriptor = open (hash->bucket_filename,
                                                     O_RDWR);
                if (hash->bucket_file_descriptor == -1) {
                        medusa_log_fatal_error ("Opening bucket file failed");
                        g_free (hash->bucket_filename);
                        g_free (hash);
                        return NULL;
                }

                first_bucket_block = map_new_block (hash, 0);

                hash->bucket_data = g_slist_append (NULL,
                                                    first_bucket_block);
                
        }
#else
        hash->bucket_file_pointer = 
                medusa_fopen_error_cover (hash->bucket_filename,
                                          "r+");
        if (hash->bucket_file_pointer == NULL) {
                g_free (hash->bucket_filename);
                g_free (hash);
                return NULL;
        }
#endif

        
        hash->directory = directory_new (index_name,
                                         hash_name);
        hash->is_empty = TRUE;

        return hash;
}


/* medusa_extensible_hash_open
 *
 * Creates a new in memory object that references the on disk hash.
 * does not load anything on disk into memory, 
 *
 * @index_name      The name of the index (may be "")
 * @hash_name       The name of the extensible hash within the index (may be "")
 *
 * Return value     A new extensible hash object, or NULL if 
 *                  opening the on disk files fails
 */
MedusaExtensibleHash *
medusa_extensible_hash_open (const char *index_name,
                             const char *hash_name)
{
        MedusaExtensibleHash *hash;
        
        g_return_val_if_fail (hash_name != NULL, NULL);

        hash = g_new0 (MedusaExtensibleHash, 1);
        set_bucket_size ();
        
        
        hash->bucket_filename = generate_bucket_filename (hash_name,
                                                          index_name,
                                                          FALSE);
        hash->bucket_file_pointer = medusa_fopen_error_cover (hash->bucket_filename,
                                                              "r+");
        if (!medusa_fread_error_cover (&hash->number_of_buckets,
                                       sizeof (guint32),
                                       1,
                                       hash->bucket_file_pointer,
                                       hash->bucket_filename,
                                       TRUE)) {
                medusa_fclose_error_cover (hash->bucket_file_pointer,
                                           hash->bucket_filename);
                g_free (hash->bucket_filename);
                g_free (hash);
                return NULL;
        }

        hash->directory = directory_open (index_name,
                                          hash_name);

        return hash;
}

/* remove_old_key_value_pair
 *
 * Takes the locations key and value pair being removed in
 * the bucket itself, and erases them, so that the bucket contains
 * no "holes"
 *
 * @bucket                 The bucket to remove the pair from
 * @stored_key_value_pair  The location of the key to be removed in the bucket
 *
 * @Return value           The new location in the bucket of the end of value
 */
static char *
remove_old_key_value_pair (Bucket     *bucket,
                           char *stored_key_value_pair)
{
        int size_of_rest_of_bucket, key_value_pair_length;
        const char *end_of_stored_key_value_pair;
        const char *bucket_value_starting_point, *bucket_value_ending_point;

        /* Otherwise, move the rest of the memory,
           and write the new key and value to the end */
        get_bucket_value_start_and_end_points (bucket,
                                              &bucket_value_starting_point,
                                              &bucket_value_ending_point);
        g_assert (stored_key_value_pair >= bucket_value_starting_point);
        g_assert (stored_key_value_pair < bucket_value_ending_point);

        key_value_pair_length = strlen (stored_key_value_pair) + 1;
        end_of_stored_key_value_pair = stored_key_value_pair + key_value_pair_length;

        size_of_rest_of_bucket = bucket_value_ending_point - end_of_stored_key_value_pair;
        g_assert (size_of_rest_of_bucket >= 0);

        memmove (stored_key_value_pair, end_of_stored_key_value_pair, size_of_rest_of_bucket);
        memset (stored_key_value_pair + size_of_rest_of_bucket, 0, key_value_pair_length);
        

        return stored_key_value_pair + size_of_rest_of_bucket;
}


/* replace_value_for_key_in_bucket
 * 
 * Takes a bucket and a key and value.  Removes the 
 * key with its old value, if it exists, and adds the
 * new key and value.  Assumes that there is enough space in the
 * bucket for the new key and value.
 * Relies on "key|value" format of key/value pairs
 *
 * @bucket                The bucket to replace 
 * @key                   The key whose value is being replaced
 * @value                 The new value to be added to the bucket
 *
 */
static void
replace_value_for_key_in_bucket (Bucket         *bucket,
                                 const char     *key,
                                 const char     *value)
{
        char *stored_key_value_pair, *stored_value;
        char *new_key_value_pair, *new_end_of_bucket_value;
        int size_change_caused_by_value_switch;
        
        stored_key_value_pair = bucket_contains_key (bucket, key, &stored_value);
        g_assert (stored_key_value_pair != NULL);
        
        if (stored_value == NULL &&
            value == NULL) {
                return;
        }

        /* If the old value and new value have the same length,
           replace the old value in place */
        if (stored_value != NULL && value != NULL &&
            strlen (stored_value) == strlen (value)) {
                strcpy (stored_value, value);
                return;
        }

        /* Decrease the amount of free space by the differences in lengths of
           the old and new value */
        if (value != NULL) {
                if (stored_value == NULL) {
                        size_change_caused_by_value_switch = strlen (value) + 1;
                }
                else {
                        size_change_caused_by_value_switch = strlen (value) - strlen (stored_value);
                }
        }
        else {
                size_change_caused_by_value_switch = - strlen (stored_value);
        }
	
        new_end_of_bucket_value = remove_old_key_value_pair (bucket, stored_key_value_pair);
        new_key_value_pair = key_value_pair_new (key, value);
        strcpy (new_end_of_bucket_value, new_key_value_pair);
        g_free (new_key_value_pair);

        adjust_bucket_free_space (bucket, size_change_caused_by_value_switch);
}


/* add_key_value_pair_to_bucket
 *
 * Add a new key and value to an existing bucket.  Assumes the bucket has enough
 * space to hold the new key and value, encoded as a key value pair.
 * 
 * @bucket              The bucket to add the new key and value to
 * @key                 The new key
 * @value               The new value; may be NULL
 *
 * No return value 
 */
static void
add_key_value_pair_to_bucket (Bucket     *bucket,
                              const char *key,
                              const char *value)
{
        char *new_key_value_pair;
        char *location_for_new_pair_in_bucket;
        gboolean bucket_has_reasonable_free_space;
        guint16 bucket_free_space;

        bucket_has_reasonable_free_space = get_bucket_free_space (bucket, &bucket_free_space);
        g_return_if_fail (bucket_has_reasonable_free_space);
        g_assert (bucket_free_space >= key_value_pair_length (key, value));

        
        new_key_value_pair = key_value_pair_new (key, value);
        location_for_new_pair_in_bucket = bucket->value + bucket_size - bucket_free_space;
        strcpy (location_for_new_pair_in_bucket, new_key_value_pair);
        g_free (new_key_value_pair);

        adjust_bucket_free_space (bucket, - key_value_pair_length (key, value));
}

/* remove_key_value_pair_from_bucket
 *
 * Remove a key, and its value from the bucket.  Passing the value is not
 * necessary, because uniqueness of the key in the bucket is assumed.
 *
 * @bucket               The bucket to remove the key from
 * @key                  The key to remove
 *
 * No return value
 */
static void
remove_key_value_pair_from_bucket (Bucket      *bucket,
                                   const char  *key)
{
        char *stored_key_value_pair;

        stored_key_value_pair = bucket_contains_key (bucket, key, NULL);
        g_assert (stored_key_value_pair != NULL);

        remove_old_key_value_pair (bucket, stored_key_value_pair);
        adjust_bucket_free_space (bucket, strlen (stored_key_value_pair) + 1);
}

/* bucket_has_space_for_new_key_value_pair
 *
 * Determine whether the current bucket has space for a particular
 * new key value pair, passing the key and value separately.
 *
 * bucket              The bucket to check for space in
 * key                 The key that would be inserted
 * value               The value that would be inserted; can be NULL
 *
 * @Return value       TRUE if the bucket has sufficient space,
 *                     FALSE otherwise
 */
static gboolean
bucket_has_space_for_new_key_value_pair (Bucket      *bucket,
                                         const char  *key,
                                         const char  *value)
{
        int space_required_for_pair;
        guint16 bucket_free_space;

        space_required_for_pair = key_value_pair_length (key, value);
        
        get_bucket_free_space (bucket, &bucket_free_space);

        return (space_required_for_pair <= bucket_free_space);
}
   

/* insert_pair_into_bucket_if_it_fits
 *
 * Tries to fit the key and value into the argument bucket.
 * The key and value may be a replacement for an old earlier key.
 * Returns TRUE if the key and value could successfully be fit into
 * the bucket, FALSE otherwise.
 *
 * @bucket          The bucket to insert the key and value into
 * @key             The key to add
 * @value           The value to add
 *
 * @Return value    TRUE if the key and value were added to the bucket
 *                  FALSE otherwise
 */
static gboolean
insert_pair_into_bucket_if_it_fits (Bucket       *bucket,
                                    const char   *key,
                                    const char   *value)
{
        char *old_value;
        guint16 bucket_free_space;
        gboolean bucket_free_space_is_valid;

        bucket_free_space_is_valid = get_bucket_free_space (bucket, &bucket_free_space);
        g_return_val_if_fail (bucket_free_space_is_valid, FALSE);

        if (bucket_contains_key (bucket, key, &old_value)) {
                if (bucket_free_space >= (strlen (value) - strlen (old_value))) {
                        replace_value_for_key_in_bucket (bucket, key, value);
                        return TRUE;
                }
                else {
                        remove_key_value_pair_from_bucket (bucket, key);
                        return FALSE;
                }
        }
        else {
                if (bucket_has_space_for_new_key_value_pair (bucket, key, value)) {
                        add_key_value_pair_to_bucket (bucket, key, value);
                        return TRUE;
                }
        }

        return FALSE;
}


/* read_bucket_and_insert_pair_if_it_fits
 *  
 * Insert a string into a particular bucket. Since buckets are of fixed size, 
 * only do this if space is still available for the string.  If space is not
 * available, return FALSE 
 * Relies on "key|value" format of key/value pairs
 * 
 * @hash                  The hash to find the bucket for
 * @bucket_number         The bucket number to insert the string into
 * @string_for_insertion  The string
 * @bucket_read           If the bucket did not contain enough space for the key/value 
 *                        pair, receives the bucket that corresponds to the key/value,
 *                        otherwise remains unassigned
 * @io_succeeded          Receives TRUE if all I/O necessary to insert the key/value pair.
 *                        into the bucket was successful (or to determine whether
 *                        the string could be inserted), FALSE otherwise
 * 
 * @return_value          Returns TRUE if the key/value pair was successfully inserted
 *                        into the bucket; FALSE is the bucket did not contain enough
 *                        space for the key/value pair
 */

static gboolean
read_bucket_and_insert_pair_if_it_fits (MedusaExtensibleHash *hash,
                                        guint                 bucket_number,
                                        const char           *key,
                                        const char           *value,
                                        Bucket              **bucket_read_from_disk,
                                        gboolean             *io_succeeded)
{
        Bucket *bucket;
        gboolean item_was_inserted;

        g_assert (hash != NULL);
        g_assert (key != NULL);

        *io_succeeded = TRUE;

        bucket = bucket_read (hash, bucket_number);
        if (bucket == NULL) {
                *io_succeeded = FALSE;
                return FALSE;
        }
        
        item_was_inserted = insert_pair_into_bucket_if_it_fits (bucket, key, value);
        
        if (item_was_inserted) {
                *io_succeeded = bucket_write (hash, bucket, bucket_number);
                bucket_free (bucket);
                return *io_succeeded;
        }
        else {
                *bucket_read_from_disk = bucket;
                return FALSE;
        }
}

/* directory_double
 * Takes a directory, and makes the directory twice as large, by making the
 * new second half a copy of the first half.  Assumes that the directory is not empty.
 *
 * @directory     The directory to be copied
 *
 * @Return value  Returns TRUE if all relevant I/O succeeded, FALSE otherwise 
 */
static gboolean
directory_double (Directory *directory)
{
        guint *new_entries;
        int old_directory_entries_size;

        g_assert (!directory_is_empty (directory));

        new_entries = g_new (guint, 1 << (directory->depth + 1));
        old_directory_entries_size = directory_get_number_of_entries (directory);
        memcpy (new_entries, directory->entries, old_directory_entries_size * sizeof (guint));
        memcpy (new_entries + old_directory_entries_size, 
                directory->entries, old_directory_entries_size * sizeof (guint));
        g_free (directory->entries);
        directory_depth_increment (directory);
        directory->entries = new_entries;

        return TRUE;
}

#ifdef NEED_PRINT_DIRECTORY
static void
print_directory (Directory *directory)
{
        gint32 i;

        /* FIXME: This only works if the directory is in memory */
        g_print ("Directory information:\n");
        g_print ("Directory is %sempty\n", directory->is_empty ? "" : "not ");
        g_print ("Depth is %d\n", directory->depth);
        g_print ("Entries are:\t");
        for (i = 0; i < directory_get_number_of_entries (directory); i++) {
                printf ("%d\t", directory->entries[i]);
        }
        printf ("\n");
}
#endif

/* hash_directory_split
 * 
 * Doubles the size of the current directory, increasing its depth by one.
 * This operation basically involves copying the directory itself, and incrementing
 * the bucket depth.  Assumes that directory is not empty
 *
 * @hash         The hash to split the directory of
 * 
 * @Return value Returns TRUE if all relevant I/O succeeded, FALSE otherwise
 */
static gboolean
hash_directory_split (MedusaExtensibleHash    *hash)
{
        gboolean directory_was_doubled;

        g_assert (!directory_is_empty (hash->directory));
        directory_was_doubled = directory_double (hash->directory);
        if (!directory_was_doubled) {
                return FALSE;
        }
        return TRUE;
}
 

typedef struct {
        Bucket *new_bucket;
        guint new_bucket_number;
        Bucket *replacement_bucket;
        guint replacement_bucket_number;
} SplitBucket;

/* split_bucket_new
 *
 * Creates a "split bucket", to rehash elements from
 * an older, filled bucket into.  The initial depth of the
 * component buckets is set to be the depth of the older, filled
 * bucket + 1.  Returns in-memory buckets, that need to be
 * flushed to disk.
 *
 * @new_bucket_depth         The depth to set for the component buckets 
 *
 * @Return value             A newly constructed split bucket
 */
static SplitBucket *
split_bucket_new (guint16 new_bucket_depth,
                  guint new_bucket_number,
                  guint replacement_bucket_number)
{
        SplitBucket *split_bucket;

        g_assert (new_bucket_number > replacement_bucket_number);
        split_bucket = g_new0 (SplitBucket, 1);
        split_bucket->new_bucket = bucket_new ();
        split_bucket->new_bucket_number = new_bucket_number;
        split_bucket->replacement_bucket = bucket_new ();
        split_bucket->replacement_bucket_number = replacement_bucket_number;
        bucket_set_depth (split_bucket->replacement_bucket, new_bucket_depth);
        bucket_set_depth (split_bucket->new_bucket, new_bucket_depth);

        return split_bucket;
}

/* split_bucket_free
 *
 * Frees the split bucket value structure, and the buckets inside it
 *
 * @split_bucket   The split bucket to be freed
 */
static void
split_bucket_free (SplitBucket *split_bucket)
{
        bucket_free (split_bucket->replacement_bucket);
        bucket_free (split_bucket->new_bucket);
        
        g_free (split_bucket);
}

/* split_bucket_write_value_to_disk_and_free
 *
 * Flushes the components of a split bucket to the on-disk value
 * structure. 
 *
 * @split_bucket           The new buckets to flush to disk
 * @hash                   The hash to flush the buckets into
 * @old_bucket_number      The bucket identifier for the older, filled bucket
 *
 * @Return value   Returns TRUE if all I/O succeeded and the bucket was successfully inserted,
 *                 FALSE otherwise
 */
static gboolean
split_bucket_write_value_to_disk_and_free (SplitBucket          *split_bucket,
                                          MedusaExtensibleHash *hash,
                                          guint                 old_bucket_number)
{
        gboolean replacement_bucket_was_written, new_bucket_was_written;

        g_assert (old_bucket_number < directory_get_number_of_entries (hash->directory));

        replacement_bucket_was_written = bucket_write (hash,
                                                       split_bucket->replacement_bucket,
                                                       split_bucket->replacement_bucket_number);
        if (!replacement_bucket_was_written) {
                split_bucket_free (split_bucket);
                return FALSE;
        }

        new_bucket_was_written = bucket_write (hash,
                                               split_bucket->new_bucket,
                                               split_bucket->new_bucket_number);
        if (!new_bucket_was_written) {
                split_bucket_free (split_bucket);
                return FALSE;
        }

        split_bucket_free (split_bucket);

        return TRUE;
}

/* extract_key_and_value_from_bucket_value
 *
 * Takes bucket value, and extracts the first key and value
 * from the location of the bucket value pointer.  Assumes the value pointer
 * points to the beginning of an encoded key / value pair. 
 *
 * @bucket_value         The bucket value where the key and value are foind
 * @key                 Receives the decoded key
 * @value               Receives the decoded value
 */
static void
extract_key_and_value_from_bucket_value (const char  *bucket_value,
                                         char       **key,
                                         char       **value)
{
        const char *key_and_value_separator;

        g_assert (bucket_value != NULL);
        
        key_and_value_separator = strchr (bucket_value, KEY_AND_VALUE_SEPARATOR);
        if (key_and_value_separator == NULL) {
                *key = g_strdup (bucket_value);
                *value = NULL;
        }
        else {
                *key = g_strndup (bucket_value, key_and_value_separator - bucket_value);
                *value = g_strdup (key_and_value_separator + 1);
        }
}


/* read_bucket_number_from_directory_entry
 *
 * Opens the directory of the extensible hash and reads the bucket number
 * corresponding to the directory entry.  The directory is simply a mapping
 * of codes that strings are hashed to to a bucket number.  This mapping is
 * not one to one, and it is expected that many directory entries can point to the
 * same bucket.  
 *
 * @directory                   The directory to search for the bucket number in
 * @directory_entry_number      The directory entry number
 * @bucket_number               Receives the corresponding bucket number that the directory
 *                              entry contains.  If an I/O error occurs, this value is not
 *                              assigned
 *
 * @Return value                Returns TRUE if all I/O succeeded and the bucket was successfully inserted,
 *                              FALSE otherwise
 */

static gboolean
read_bucket_number_from_directory_entry (Directory     *directory,
                                         guint          directory_entry_number,
                                         guint         *bucket_number)
{
        g_assert (!directory_is_empty (directory));
        g_assert (directory_entry_number < directory_get_number_of_entries (directory));

        if (directory->data_is_in_memory) {
                *bucket_number = directory->entries[directory_entry_number];
        }
        else {
                if (!medusa_fseek_error_cover (directory->entries_file,
                                               (directory_entry_number + 1) * sizeof (guint),
                                               SEEK_SET,
                                               directory->filename)) {
                        return FALSE;
                }
                if (!medusa_fread_error_cover (bucket_number,
                                               sizeof (guint), 1,
                                               directory->entries_file,
                                               directory->filename,
                                               TRUE)) {
                        return FALSE;
                }
        }
        return TRUE;
}

/* rehash_key_value_pair
 *
 * As the function argument of bucket_foreach_key_value_pair,
 * takes a single key and value and reinserts them into the
 * extensible hash.  Assumes that the hash is supplied as "user_value"
 * and the new buckets are supplied as "additional_user_value"
 *
 * @key                   The key to be rehashed
 * @value                 The value to be rehashed (can be NULL)
 * @user_value             Pointer to the hash structure to rehash into
 * @additional_user_value  Pointer to the new split bucket, where the key and value should end up.
 *
 * @return value          TRUE if there was space in the new bucket to insert the key and value
 *                        FALSE otherwise
 */
static gboolean
rehash_key_value_pair (const char   *key,
                       const char   *value,
                       gpointer      user_value,
                       gpointer      additional_user_value)
{
        MedusaExtensibleHash *hash;
        SplitBucket *split_bucket;
        guint directory_entry_number, bucket_number;
        gboolean bucket_number_was_found;
        
        g_assert (key != NULL);
        g_assert (strchr (key, KEY_AND_VALUE_SEPARATOR) == NULL);
        
        hash = (MedusaExtensibleHash *) user_value;
        split_bucket = (SplitBucket *) additional_user_value;
        
        g_assert (hash != NULL);
        g_assert (split_bucket != NULL);

        directory_entry_number = directory_key_hash_to_entry_number (hash->directory, key);
        bucket_number_was_found = read_bucket_number_from_directory_entry (hash->directory, directory_entry_number, &bucket_number);

        g_assert (bucket_number == split_bucket->new_bucket_number ||
                  bucket_number == split_bucket->replacement_bucket_number);
        
        if (!bucket_number_was_found) {
                return FALSE;
        }
        
        if (bucket_number == split_bucket->new_bucket_number) {
                return insert_pair_into_bucket_if_it_fits (split_bucket->new_bucket,
                                                           key,
                                                           value);
        }
        
        else if (bucket_number == split_bucket->replacement_bucket_number) {
                return insert_pair_into_bucket_if_it_fits (split_bucket->replacement_bucket,
                                                           key,
                                                           value);
        }

        g_assert_not_reached ();
        return FALSE;
}


/* bucket_foreach_key_value_pair
 *
 * Takes a bucket and performs a function on each key value pair in the bucket.
 * The function argument should take both a key and value, and two pieces of 
 * associated environmental value
 *
 * @bucket                  The bucket to find the key value entries in
 # @function                The function to perform on each key / value pair
 * @user_value               Value to give to the passed in function
 * @additional_user_value    The second value pointer to give to the passed in function
 *
 * @return value            Returns TRUE if each invocation of the function returned TRUE
 *                          FALSE otherwise.
 */
static gboolean
bucket_foreach_key_value_pair (Bucket                *bucket,
                               BucketForeachFunc      function,
                               gpointer               user_value,
                               gpointer               additional_user_value)
{
        const char *bucket_value, *bucket_value_ending_point;
        char *key, *value;
        gboolean function_completed_successfully;

        get_bucket_value_start_and_end_points (bucket, 
                                              &bucket_value,
                                              &bucket_value_ending_point);
        g_assert (bucket_value_ending_point >= bucket_value);

        if (bucket_value_ending_point == bucket_value) {
                return TRUE; 
        }

        while (*bucket_value != 0) {
                extract_key_and_value_from_bucket_value (bucket_value,
                                                        &key,
                                                        &value);
                function_completed_successfully = function (key,
                                                            value,
                                                            user_value,
                                                            additional_user_value);
                if (!function_completed_successfully) {
                        return FALSE;
                }
                        
                g_free (key);
                g_free (value);
                
                bucket_value += strlen (bucket_value) + 1;
                if (bucket_value >= bucket_value_ending_point) {
                        break;
                }
        }

        return TRUE;
}

/* key_matches
 * 
 * Used as the test function passed to bucket_test_key_value_pairs_until_match
 * tests whether the key in the key value pair matches the key given as value
 * matching is case sensitive
 *
 * @key                  The key from the bucket
 * @value                The value from the bucket
 * @user_value            The key to try and match
 *
 * @Return Value         TRUE if the keys match, FALSE otherwise
 */
static gboolean
key_matches (const char *key,
             const char *value,
             gconstpointer    user_value)
{
        const char *key_to_match;
        
        g_assert (key != NULL);

        key_to_match = (const char *) user_value;
        g_assert (key_to_match != NULL);
        
        return strcmp (key, key_to_match) == 0;
}

/* bucket_test_key_value_pairs_until_match 
 *
 * Takes a bucket, and iterates over the key value pairs until one is
 * found that matches the test function.  If no key value pair in the
 * bucket satisfies the test function, FALSE is returned.
 *
 * @bucket           The bucket to iterate over
 * @test             The function that tests the key value pair
 * @user_value       The value to supply to the test function
 * @matching_pair    Receives the pair that satisfied the test function, or NULL
 *                   if none exist.  
 *
 * @return value     Returns TRUE if a pair was found that matched the test function,
 *                   FALSE otherwise
 */
static gboolean
bucket_test_key_value_pairs_until_match (Bucket                   *bucket,
                                         BucketKeyValueTestFunc    test,
                                         gconstpointer             user_value,
                                         char                    **matching_key,
                                         char                    **matching_value)
{
        const char *bucket_value, *bucket_value_ending_point;
        char *key, *value;

        get_bucket_value_start_and_end_points (bucket,
                                              &bucket_value,
                                              &bucket_value_ending_point);
        
        while (*bucket_value != 0) {
                extract_key_and_value_from_bucket_value (bucket_value,
                                                        &key,
                                                        &value);
                if (test (key, value, user_value)) {
                        *matching_key = key;
                        *matching_value = value;
                        return TRUE;
                }
                else {
                        g_free (key);
                        g_free (value);
                }
                
                bucket_value += strlen (bucket_value);
                if (bucket_value >= bucket_value_ending_point) {
                        break;
                }
                else {
                        bucket_value++;
                }
        }

        return FALSE;

}

/* split_directory_pointers_to_full_bucket
 *
 * When we split a bucket, we need to make half of the pointers that
 * used to point to the old bucket point to the new bucket.  It's not
 * hard to find all of the entries in the directory that point to the
 * new bucket since we know that all directory entries that share the
 * lowest n bits of the hash key will point to the old bucket, where
 * n is the difference between the depth of the directory, and the
 * depth of the bucket.
 *
 * @hash             The hash that contains the directory we will modify
 * @split_bucket     The split bucket, which contains the bucket numbers
 *                   of the new and replacement buckets
 * @key              The key we are hashing -- used to find at least 
 *                   one directory entry from that points to the old bucket,
 *                   as a "seed" (see algorithm in the description)
 *
 * @return value     Returns TRUE if directory I/O succeeded, FALSE otherwise
 */
static gboolean
split_directory_pointers_to_full_bucket (MedusaExtensibleHash   *hash,
                                         SplitBucket            *split_bucket,
                                         const char             *key)
{
        guint directory_entry_number;
        guint16 old_bucket_depth, bucket_depth;
        guint directory_depth, depth_of_pointers_to_bucket;
        guint low_bits, i;
        gboolean bucket_had_valid_depth;
        
        directory_entry_number = directory_key_hash_to_entry_number (hash->directory,
                                                                     key);
        directory_depth = directory_get_depth (hash->directory);
        bucket_had_valid_depth = bucket_get_depth (split_bucket->new_bucket,
                                                   &bucket_depth,
                                                   directory_depth);
        old_bucket_depth = bucket_depth - 1;
        g_assert (bucket_had_valid_depth);
        g_assert (directory_depth > old_bucket_depth);

        depth_of_pointers_to_bucket = directory_depth - old_bucket_depth;
        low_bits = directory_entry_number & ((1 << old_bucket_depth) - 1);
        g_assert (low_bits < 1 << old_bucket_depth);

        /* Switch the value of every other directory entry that points to
           the full bucket */
        for (i = 0; i < (1 << depth_of_pointers_to_bucket); i+=2) {
                directory_entry_number = i * (1 << old_bucket_depth) | low_bits;
                g_assert (directory_entry_number < directory_get_number_of_entries (hash->directory));
                if (!directory_entry_set (hash->directory, directory_entry_number, 
                                          split_bucket->new_bucket_number)) {
                        return FALSE;
                }
        }
        
        return TRUE;
}
 

/* hash_insert_with_bucket_split
 *
 * Inserts a key and value into the extensible hash, in the case where
 * the bucket number currently corresponding to the key and value is
 * full.  In this case, we do the following:
 * 1.  Create a new bucket
 * 2.  Increase the depth of the old and new buckets by one
 * 3.  If the depth of the new buckets exceeds the directory depth, split the directory
 * 4.  Rehash all of the objects currently in the old bucket, and the new object into the new buckets at the new depth.
 *
 * @hash                        The hash we are adding the new object to
 * @old_bucket                  The old bucket, read from disk
 * @old_bucket_number           The old bucket's number
 * @key                         The key to be inserted
 * @value                       The value to be inserted, may be NULL
 *
 * @return value                Returns TRUE if all I/O succeeded and the bucket was successfully inserted,
 *                              FALSE otherwise */
static gboolean
hash_insert_with_bucket_split (MedusaExtensibleHash *hash,
                               Bucket               *old_bucket,
                               guint                 old_bucket_number,
                               const char           *key,
                               const char           *value)
{
        guint16 old_bucket_depth, new_bucket_depth;
        gboolean bucket_depth_is_valid, directory_was_successfully_split;
        gboolean new_buckets_were_written_to_disk, old_value_fit_into_split_bucket;
        SplitBucket *split_bucket;
        guint16 new_bucket_free_space, replacement_bucket_free_space;

        bucket_depth_is_valid = bucket_get_depth (old_bucket, &old_bucket_depth, directory_get_depth (hash->directory));
        if (!bucket_depth_is_valid) {
                return FALSE;
        }
        new_bucket_depth = old_bucket_depth + 1;



        if (new_bucket_depth > directory_get_depth (hash->directory)) {
                directory_was_successfully_split = hash_directory_split (hash);
                if (!directory_was_successfully_split) {
                        return FALSE;
                }
        }
        
        split_bucket = split_bucket_new (new_bucket_depth,
                                         hash->number_of_buckets++,
                                         old_bucket_number);

        /* We know now that at least two entries in the directory are pointing to the
           replacement bucket. We should change half of the pointers to the existing
           bucket to point to the new bucket, so that the entries are rehashed evenly
           among the new buckets */
        split_directory_pointers_to_full_bucket (hash,
                                                 split_bucket,
                                                 key);
        /* Rehash all of the values in the old bucket */
        old_value_fit_into_split_bucket = bucket_foreach_key_value_pair (old_bucket,
                                                                        rehash_key_value_pair,
                                                                        hash,
                                                                        split_bucket);
        /* Check the free space on the new buckets */
        g_assert (get_bucket_free_space (split_bucket->new_bucket,
                                         &new_bucket_free_space));
        g_assert (get_bucket_free_space (split_bucket->replacement_bucket,
                                         &replacement_bucket_free_space));                  
                 
        /* Since the old value fit into one bucket, it should certainly fit into two */
        g_assert (old_value_fit_into_split_bucket);

        new_buckets_were_written_to_disk = split_bucket_write_value_to_disk_and_free (split_bucket,
                                                                                     hash,
                                                                                     old_bucket_number);
        if (!new_buckets_were_written_to_disk) {
                /* FIXME: Do we need to free the buckets here? */
                return FALSE;
        }

        medusa_extensible_hash_insert (hash,
                                       key, 
                                       value);

        return TRUE;
}
                  
/* create_first_directory_entry
 *
 * Takes an empty directory, creates an initial directory.
 *
 * @directory         The directory to create the first entry in
 *
 * @Return Value      Returns TRUE if all relevant I/O succeeded, FALSE otherwise
 *
 */
static gboolean
create_first_directory_entry (Directory    *directory)
{
        directory->is_empty = FALSE;
        directory->entries = g_new0 (guint, 1);

        return TRUE;
}

/* medusa_extensible_hash_insert
 *  
 * Insert a string into the extensible hash.  The name and value may be treated as 
 * separate, as in a normal in memory hash table, but they are stored on disk concatenated
 * and separated by a separator character ('|').  For this reason, the '|' character may not
 * be present in keys inserted into the table.  Additionally, no value is necessary.  If 
 * value is NULL, only the entry value will be added to the hash table.
 *
 * @hash                  The hash to find the bucket for
 * @bucket_for_insertion  The bucket number to insert the string into
 * @string_for_insertion  The string
 *
 * @return_value          Whether the operation succeeded or not */

gboolean
medusa_extensible_hash_insert (MedusaExtensibleHash   *hash,
                               const char             *key,
                               const char             *value)
{
        guint bucket_number;
        guint directory_entry_number;
        gboolean io_succeeded, bucket_number_was_found, bucket_had_space_for_key;
        Bucket *bucket_read_from_disk;

        g_return_val_if_fail (hash != NULL, FALSE);
        g_return_val_if_fail (key != NULL, FALSE);
        g_return_val_if_fail (strchr (key, KEY_AND_VALUE_SEPARATOR) == NULL, FALSE);

        if (strlen (key) > bucket_size) {
                medusa_log_error ("Attempt to insert key %s that is longer "
                                  "than bucket size %d.  Word will be "
                                  "ignored.", key, bucket_size);
                return FALSE;
        }
        if (value && 
            strlen (key) + strlen (value) + 1 > bucket_size) {
                medusa_log_error ("Combined length of key %s and value %s "
                                  " is too long to fit in a bucket of size "
                                  "%d.  Ignoring pair.", key, value, 
                                  bucket_size);
		return FALSE;
        }

        if (hash->is_empty) {
                hash->is_empty = FALSE;
                hash->number_of_buckets++;
                g_assert (directory_is_empty (hash->directory));
                directory_entry_number = 0;
                create_first_directory_entry (hash->directory);
        }
        
        directory_entry_number = directory_key_hash_to_entry_number (hash->directory,
                                                                     key);

        bucket_number_was_found = read_bucket_number_from_directory_entry (hash->directory, directory_entry_number, &bucket_number);
        if (!bucket_number_was_found) {
                return FALSE;
        }

        bucket_had_space_for_key = read_bucket_and_insert_pair_if_it_fits (hash,
                                                                           bucket_number, 
                                                                           key,
                                                                           value,
                                                                           &bucket_read_from_disk,
                                                                           &io_succeeded);
        
        if (io_succeeded == FALSE) {
                return FALSE;
        }

        if (!bucket_had_space_for_key) {
                io_succeeded = hash_insert_with_bucket_split (hash, 
                                                              bucket_read_from_disk, 
                                                              bucket_number,
                                                              key, value);
                bucket_free (bucket_read_from_disk);
                return io_succeeded;
                
        }

        return TRUE;
}

/* medusa_extensible_hash_lookup
 *
 * Looks for a key in an extensible hash.  If the key is stored
 * in the hash, sets key_found to TRUE.  Stores the corresponding value for
 * the key, if it exists. 
 *
 * @hash          The hash to look for the key in
 * @key           The key to look for
 * @key_exists    Receives TRUE if the key is found in the hash, FALSE otherwise
 * @value         Receives the value corresponding to the key in the hash, if one exists
 *
 * @Return Value  Returns TRUE if all necessary I/O completed successfully, FALSE otherwise
 */
gboolean
medusa_extensible_hash_lookup (MedusaExtensibleHash     *hash,
                               const char               *key,
                               gboolean                 *key_exists,
                               char                    **value)
{
        gboolean bucket_number_was_found;
        guint directory_entry_number, bucket_number;
        Bucket *bucket;
        char *matching_key, *matching_value;

        g_return_val_if_fail (hash != NULL, FALSE);
        g_return_val_if_fail (key != NULL, FALSE);
        g_return_val_if_fail (value != NULL, FALSE);

        if (strlen (key) > bucket_size) {
                medusa_log_error ("Cannot look for key %s because it is "
                                  "larger than the bucket size %d.",
                                  g_strndup (key, 100), bucket_size);
                return FALSE;
        }

        *key_exists = FALSE;
        *value = NULL;

        if (directory_is_empty (hash->directory)) {
                return TRUE;
        }

        directory_entry_number = directory_key_hash_to_entry_number (hash->directory,
                                                                     key);
        bucket_number_was_found = read_bucket_number_from_directory_entry (hash->directory,
                                                                           directory_entry_number,
                                                                           &bucket_number);
        if (!bucket_number_was_found) {
                return FALSE;
        }
        
        bucket = bucket_read (hash, bucket_number);
        if (bucket == NULL) {
                return FALSE;
        }

        if (bucket_test_key_value_pairs_until_match (bucket,
                                                     key_matches,
                                                     key,
                                                     &matching_key,
                                                     &matching_value)) {
                *key_exists = TRUE;
                g_assert (strcmp (key, matching_key) == 0);
                g_free (matching_key);
                *value = matching_value;
        }

        bucket_free (bucket);
        return TRUE;
}

/* medusa_extensible_hash_remove
 *
 * Looks for a key in an extensible hash.  If the key is found, removes the
 * key and its corresponding value from the hash.
 *
 * @hash          The hash to look for the key in
 * @key           The key to look for
 *
 * @Return Value  Returns TRUE if all necessary I/O completed successfully, FALSE otherwise
 */
gboolean
medusa_extensible_hash_remove (MedusaExtensibleHash *hash,
                               const char           *key)
{
        guint directory_entry_number, bucket_number;
        gboolean bucket_number_was_found;
        Bucket *bucket;

        g_return_val_if_fail (hash != NULL, FALSE);
        g_return_val_if_fail (key != NULL, FALSE);

        /* FIXME: Refactor this code, up until bucket_read */
        if (directory_is_empty (hash->directory)) {
                return TRUE;
        }

        directory_entry_number = directory_key_hash_to_entry_number (hash->directory,
                                                                     key);
        bucket_number_was_found = read_bucket_number_from_directory_entry (hash->directory,
                                                                           directory_entry_number,
                                                                           &bucket_number);
        if (!bucket_number_was_found) {
                return FALSE;
        }
        
        bucket = bucket_read (hash, bucket_number);
        if (bucket == NULL) {
                return FALSE;
        }
        
        remove_key_value_pair_from_bucket (bucket, key);
        bucket_write (hash,
                      bucket,
                      bucket_number);
        
        return TRUE;
}
/* call_user_function
 *
 * Utility function used by medusa_extensible_hash_foreach.
 * It marshals the single function data argument expected by a GHFunc
 * for each bucket_Foreach call, which expects a function that has two slots
 * for data.
 *
 * @key             Hash key being marshalled
 * @value           Hash value being marshalled 
 * @function        function passed by the caller of medusa_extensible_hash_foreach
 * @function_data   data to be passed to the caller's function 
 */
static gboolean
call_user_function (const char  *key,
                    const char  *value,
                    gpointer     function,
                    gpointer     function_data)
{
        g_assert (function != NULL);

        ((MedusaExtensibleHashForeachFunc) function) (key, value, function_data);

        return TRUE;
}

/* medusa_extensible_hash_foreach
 *
 * Calls a function on each of the items currently stored in the
 * hash.  The function is executed for its side effects.
 * Stops executing if there is an I/O problem, and returns FALSE
 *
 * @hash               The hash to iterate over
 * @user_function      The function to call. 
 * @user_function_data Data passed to the function.  May be NULL
 *
 * @Return value       Returns TRUE if iteration completed with no
 *                     I/O errors. FALSE otherwise 
 */
gboolean                 
medusa_extensible_hash_foreach (MedusaExtensibleHash  *hash,
                                MedusaExtensibleHashForeachFunc user_function,
                                gpointer user_function_data)
{
        int i;
        Bucket *bucket;

        g_return_val_if_fail (user_function != NULL, FALSE);

        for (i = 0; i <= hash->number_of_buckets; i++) {
                bucket = bucket_read (hash, i);
                if (bucket_foreach_key_value_pair (bucket,
                                                   call_user_function,
                                                   user_function,
                                                   user_function_data) == FALSE) {
                        return FALSE;
                }
                bucket_free (bucket);
                                               
        }

        return TRUE;
}

/* unmap_bucket_block
 *
 * Take one element of the list of bucket blocks that
 * have been mapped, and unmap it, so that the data is written
 * back to disk.
 *
 * @data         The block, as data from a GSList
 * @user_data    Flag denoting whether all blocks have been
 *               successfully unmapped
 */
static void
unmap_bucket_block (gpointer data,
                    gpointer user_data)
{
        int munmap_result;
        gboolean *block_was_successfully_unmapped;

        munmap_result = munmap (data,
                                bucket_size * BUCKETS_TO_MAP_AT_ONCE);

        if (munmap_result == -1) {
                medusa_log_error ("Failed to unmap block of an extensible "
                                  "hash page file");
                block_was_successfully_unmapped = (gboolean *) user_data;
                *block_was_successfully_unmapped = FALSE;
        }
}

/* medusa_extensible_hash_free
 *
 * Frees the in memory pointers to the extensible hash, and
 * writes the directory to disk.
 *
 * @hash         The hash to free
 *
 * @return value Returns TRUE if all I/O completed successfully, FALSE otherwise
 */
gboolean
medusa_extensible_hash_free  (MedusaExtensibleHash *hash)
{
        g_return_val_if_fail (hash != NULL, FALSE);

        if (!directory_write_to_disk (hash->directory)) {
                return FALSE;
        }
        directory_free (hash->directory);

        if (hash->buckets_are_memory_mapped) {
                gboolean buckets_were_successfully_unmapped = TRUE;

                g_slist_foreach (hash->bucket_data,
                                 unmap_bucket_block,
                                 &buckets_were_successfully_unmapped);

                if (!buckets_were_successfully_unmapped) {
                        return FALSE;
                }

                if (lseek (hash->bucket_file_descriptor,
                           0, SEEK_SET) == -1) {
                        medusa_log_error ("Could not seek to location "
                                          "where number of buckets is stored "
                                          "in file %s, function %s", 
                                          hash->bucket_filename,
                                          __FUNCTION__);
                        return FALSE;
                }

                if (write (hash->bucket_file_descriptor,
                           &hash->number_of_buckets,
                           sizeof (guint32)) == -1) {
                        medusa_log_error ("Could not write number of "
                                          "buckets to file %s in function %s",
                                          hash->bucket_filename,
                                          __FUNCTION__);
                        return FALSE;
                }
                
                if (close (hash->bucket_file_descriptor) == -1) {
                        medusa_log_error ("Could not close bucket file for "
                                          "file %s, function %s",
                                          hash->bucket_filename, 
                                          __FUNCTION__);
                        return FALSE;
                }
        }
        else {
                medusa_fclose_error_cover (hash->bucket_file_pointer,
                                           hash->bucket_filename);
        }
        g_free (hash->bucket_filename);
        g_free (hash);

        return TRUE;
}
 
/* medusa_extensible_hash_move_completed_index_into_place
 *
 * Swap the current index marked in_progress over to be a production
 * index.  Erases the old index that was used.
 *
 * @index_name                   The name of the over all index the index to 
 *                               be moved is part of

 * @hash_name                    The particular name of the hash in the index
 *
 * @Return value                 Returns TRUE if all I/O was successful, FALSE
 * otherwise */
gboolean
medusa_extensible_hash_move_completed_index_into_place (const char *index_name,
                                                        const char *hash_name)
{
        char *directory_base;
        char *old_directory, *new_directory;
        char *old_bucket_file, *new_bucket_file;

        /* FIXME: Should we log moving files? */

        /* Move directory file */
        directory_base = directory_generate_basename (hash_name);
        old_directory = medusa_generate_index_filename (directory_base,
                                                        index_name,
                                                        TRUE);
        if (access (old_directory, F_OK) != 0) {
                medusa_log_error ("Cannot move the extensible hash %s in index"
                                  " %s into place.  Its old directory file %s "
                                  "does not exist.\n", 
                                  hash_name, index_name, old_directory);
                g_free (old_directory);
                return FALSE;
        }
        
        new_directory = medusa_generate_index_filename (directory_base,
                                                        index_name,
                                                        FALSE);

        if (!medusa_rename_error_cover (old_directory, new_directory)) {
                return FALSE;
        }

        old_bucket_file = generate_bucket_filename (hash_name,
                                                    index_name,
                                                    TRUE);
        new_bucket_file = generate_bucket_filename (hash_name,
                                                    index_name,
                                                    FALSE);
        if (!medusa_rename_error_cover (old_bucket_file, new_bucket_file)) {
                return FALSE;
        }

        g_free (old_bucket_file);
        g_free (new_bucket_file);

        return TRUE;
}

/* empty_hash_self_check
 *
 * Check that a newly gets initialized correctly,
 * and that the bucket_size constant is properly initialized
 */
static void
empty_hash_self_check (void)
{
        MedusaExtensibleHash *hash;
        gboolean foo_exists;
        char *value_corresponding_to_foo;
        
        hash = medusa_extensible_hash_new ("empty-hash-self-check",
                                           NULL);
        /* Test that bucket size was set by the constructor function */ 
        MEDUSA_TEST_BOOLEAN_RESULT (bucket_size != 0);
        MEDUSA_TEST_BOOLEAN_RESULT (hash->is_empty);
        MEDUSA_TEST_INTEGER_RESULT (hash->number_of_buckets, 0);

        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_lookup 
                                    (hash, "foo", &foo_exists, 
                                     &value_corresponding_to_foo));
        MEDUSA_TEST_BOOLEAN_RESULT (!foo_exists);
}


static void
empty_bucket_has_correct_free_space_and_depth_self_check (void)
{
        Bucket *bucket;
        MedusaExtensibleHash *hash;
        guint16 depth, free_space;

        hash = medusa_extensible_hash_new ("self-check",
                                           "bucket-read");
        bucket = bucket_read (hash, 10);
        MEDUSA_TEST_BOOLEAN_RESULT (bucket_get_depth (bucket, &depth, 1));
        MEDUSA_TEST_INTEGER_RESULT (depth, 0);
        MEDUSA_TEST_BOOLEAN_RESULT (get_bucket_free_space (bucket, &free_space));
        MEDUSA_TEST_INTEGER_RESULT (free_space, bucket_size - sizeof (guint16) * 2);

        medusa_extensible_hash_free (hash);
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_erase_constructed_index ("self-check",
                                                                   "bucket-read",
                                                                   TRUE));
                                    

}

/* insert_and_lookup_self_check
 *
 * Check that doing just a few inserts and lookups performs as expected
 */
static void
insert_and_lookup_self_check (void)
{
        MedusaExtensibleHash *hash;
        gboolean foo_exists, baz_exists;
        char *value_corresponding_to_foo, *value_corresponding_to_baz;

        hash = medusa_extensible_hash_new ("insert-and-lookup-self-check",
                                           NULL);
        /* Insert an initial element, and check that it exists, and that 
           a random element does not */
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_insert (hash, "foo", "bar"));
        MEDUSA_TEST_BOOLEAN_RESULT (!hash->is_empty);
        MEDUSA_TEST_INTEGER_RESULT (hash->number_of_buckets, 1);

        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_lookup (hash, "foo", &foo_exists, &value_corresponding_to_foo));
        MEDUSA_TEST_BOOLEAN_RESULT (foo_exists);
        MEDUSA_TEST_STRING_RESULT (value_corresponding_to_foo, "bar");
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_lookup (hash, "baz", &baz_exists, &value_corresponding_to_baz));
        MEDUSA_TEST_BOOLEAN_RESULT (!baz_exists);

        /* Insert a second element, and check that both it and the first element exist */
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_insert (hash, "baz", NULL));
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_lookup (hash, "baz", &baz_exists, &value_corresponding_to_baz));
        MEDUSA_TEST_BOOLEAN_RESULT (baz_exists);
        MEDUSA_TEST_POINTER_VALUE (value_corresponding_to_baz, NULL);
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_lookup (hash, "foo", &foo_exists, &value_corresponding_to_foo));
        MEDUSA_TEST_BOOLEAN_RESULT (foo_exists);
        MEDUSA_TEST_STRING_RESULT (value_corresponding_to_foo, "bar");
        
        /* Remove the first element, and check that it no longer exists, but the second element still does */
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_remove (hash, "foo"));
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_lookup (hash, "foo", &foo_exists, &value_corresponding_to_foo));
        MEDUSA_TEST_BOOLEAN_RESULT (!foo_exists);
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_lookup (hash, "baz", &baz_exists, &value_corresponding_to_baz));
        MEDUSA_TEST_BOOLEAN_RESULT (baz_exists);

        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_free (hash));
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_erase_constructed_index ("self-check", NULL, TRUE));

}

static void
key_value_self_check (void)
{
        g_print ("Testing creating key value pairs...\n");
        MEDUSA_TEST_STRING_RESULT (key_value_pair_new ("", NULL), "");
        MEDUSA_TEST_STRING_RESULT (key_value_pair_new ("foo", NULL), "foo");
        MEDUSA_TEST_STRING_RESULT (key_value_pair_new ("foo", ""), "foo|");
        MEDUSA_TEST_STRING_RESULT (key_value_pair_new ("foo", "bar"), "foo|bar");
        MEDUSA_TEST_STRING_RESULT (key_value_pair_new ("", "bar"), "|bar");
}


/* Test that bucket_has_space_for_new_key_value_pair works
   correctly */
static void
bucket_has_space_for_new_key_value_pair_self_check (void)
{
        Bucket *bucket;
        guint16 bucket_free_space;
        char *filler_string;

        bucket = bucket_new ();
        MEDUSA_TEST_BOOLEAN_RESULT (bucket_has_space_for_new_key_value_pair (bucket,
                                                                                 "",
                                                                                 ""));
        MEDUSA_TEST_BOOLEAN_RESULT (bucket_has_space_for_new_key_value_pair (bucket,
                                                                                 "",
                                                                                 NULL));
        MEDUSA_TEST_BOOLEAN_RESULT (bucket_has_space_for_new_key_value_pair (bucket,
                                                                                 "foo",
                                                                                 ""));
        MEDUSA_TEST_BOOLEAN_RESULT (bucket_has_space_for_new_key_value_pair (bucket,
                                                                                 "foo",
                                                                                 "bar"));
        /* Now fill up the bucket and see that the bucket still has space */
        while (bucket_has_space_for_new_key_value_pair (bucket, "foo", "bar")) {
                add_key_value_pair_to_bucket (bucket, "foo", "bar");
        }
        MEDUSA_TEST_BOOLEAN_RESULT (!bucket_has_space_for_new_key_value_pair (bucket,
                                                                              "foo",
                                                                              "bar"));
        MEDUSA_TEST_BOOLEAN_RESULT (!bucket_has_space_for_new_key_value_pair (bucket,
                                                                              "longer-foo",
                                                                              "bar"));
        MEDUSA_TEST_BOOLEAN_RESULT (!bucket_has_space_for_new_key_value_pair (bucket,
                                                                              "foo",
                                                                              "longer-bar"));
        get_bucket_free_space (bucket, &bucket_free_space);
        if (bucket_free_space != 0) {
                /* Fill up the rest of the free space with a custom length key, and NULL value */
                filler_string = medusa_fixed_length_test_string_new (bucket_free_space - 1);
                MEDUSA_TEST_BOOLEAN_RESULT (bucket_has_space_for_new_key_value_pair (bucket,
                                                                                     filler_string,
                                                                                     NULL));
                add_key_value_pair_to_bucket (bucket, filler_string, NULL);
        }

        get_bucket_free_space (bucket, &bucket_free_space);
        MEDUSA_TEST_INTEGER_RESULT (bucket_free_space, 0);
        MEDUSA_TEST_BOOLEAN_RESULT (!bucket_has_space_for_new_key_value_pair (bucket,
                                                                              "",
                                                                              ""));
        MEDUSA_TEST_BOOLEAN_RESULT (!bucket_has_space_for_new_key_value_pair (bucket,
                                                                              "foo",
                                                                              NULL));
        MEDUSA_TEST_BOOLEAN_RESULT (!bucket_has_space_for_new_key_value_pair (bucket,
                                                                              "foo",
                                                                              "bar"));
        MEDUSA_TEST_BOOLEAN_RESULT (!bucket_has_space_for_new_key_value_pair (bucket,
                                                                              "foo",
                                                                              "bar"));
        

        /* Now remove one key value pair, and there should be sufficient space again */
        remove_key_value_pair_from_bucket (bucket, "foo");
        get_bucket_free_space (bucket, &bucket_free_space);
        MEDUSA_TEST_INTEGER_RESULT (bucket_free_space, key_value_pair_length ("foo", "bar"));
        MEDUSA_TEST_BOOLEAN_RESULT (bucket_has_space_for_new_key_value_pair (bucket,
                                                                             "",
                                                                             ""));
        MEDUSA_TEST_BOOLEAN_RESULT (bucket_has_space_for_new_key_value_pair (bucket,
                                                                             "",
                                                                             NULL));
        MEDUSA_TEST_BOOLEAN_RESULT (bucket_has_space_for_new_key_value_pair (bucket,
                                                                             "foo",
                                                                             ""));
        MEDUSA_TEST_BOOLEAN_RESULT (bucket_has_space_for_new_key_value_pair (bucket,
                                                                             "foo",
                                                                             "bar"));
        MEDUSA_TEST_BOOLEAN_RESULT (!bucket_has_space_for_new_key_value_pair (bucket,
                                                                             "bigger_key",
                                                                             "bar"));
        
        bucket_free (bucket);
}

static gboolean
dont_call_this_bucket_iterator (const char *key,
                                const char *value,
                                gpointer data,
                                gpointer more_data)
{
        g_print ("Function passed to bucket_foreach_key_value_pair was called "
                 "even though there were no elements in the hash.\n");
        abort ();
}

static gboolean
count_calls_to_bucket_iterator (const char *key,
                                const char *value,
                                gpointer data,
                                gpointer more_data)
{
        int *counter;

        MEDUSA_TEST_BOOLEAN_RESULT (data != NULL);

        counter = (int *) data;
        *counter = *counter + 1;
        
        return TRUE;
}


#define UNITERATED GINT_TO_POINTER (0x02)
#define ITERATED GINT_TO_POINTER (0x01)


static gboolean
look_for_buckets_marked_twice (const char *key,
                               const char *value,
                               gpointer data,
                               gpointer more_data)
{
        GHashTable *g_hash_table;
        gpointer original_key, original_value;

        g_hash_table = (GHashTable *) data;

        g_hash_table_lookup_extended (g_hash_table,
                                      key,
                                      &original_key,
                                      &original_value);
        MEDUSA_TEST_POINTER_VALUE (original_value, UNITERATED);
        g_hash_table_remove (g_hash_table, key);
        g_free (original_key);
        g_hash_table_insert (g_hash_table,
                             g_strdup (key),
                             ITERATED);
        return TRUE;
}

 
static void
fail_if_uniterated_elements_exist (gpointer key,
                                   gpointer value,
                                   gpointer data)
{
        MedusaExtensibleHash *hash;

        hash = (MedusaExtensibleHash *) data;
        MEDUSA_TEST_POINTER_VALUE (value, ITERATED);
}

static void
test_bucket_foreach_with_full_bucket (gboolean use_value)
{
        Bucket *test_bucket;
        GHashTable *g_hash_table;
        char *key, *value;
        int calls_counter, i;

        for (i = 0; i < 100; i++) {
                test_bucket = bucket_new ();
                g_hash_table = g_hash_table_new (g_str_hash, g_str_equal);
                while (TRUE) {
                        key = medusa_random_word ();
                        if (g_hash_table_lookup (g_hash_table, key)) {
                                continue;
                        }
                        if (use_value) {
                                value = medusa_random_word ();
                        }
                        else {
                                value = NULL;
                        }
                        if (insert_pair_into_bucket_if_it_fits (test_bucket, key, value)) {
                                g_hash_table_insert (g_hash_table, g_strdup (key), UNITERATED);
                        }
                        else {
                                g_free (key);
                                g_free (value);
                                break;
                        }
                        g_free (key);
                        g_free (value);
                }
                calls_counter = 0;
                bucket_foreach_key_value_pair (test_bucket,
                                               count_calls_to_bucket_iterator,
                                               &calls_counter, NULL);
                MEDUSA_TEST_INTEGER_RESULT (g_hash_table_size (g_hash_table), calls_counter);
                
                
                bucket_foreach_key_value_pair (test_bucket,
                                               look_for_buckets_marked_twice,
                                               g_hash_table, NULL);
                g_hash_table_foreach (g_hash_table,
                                      fail_if_uniterated_elements_exist,
                                      NULL);
                bucket_free (test_bucket);
        }
}

static void
bucket_foreach_key_value_pair_self_check (void)
{
       
        Bucket *test_bucket;
        int calls_counter;
        
        /* Test that foreach with an empty function never gets called */
        test_bucket = bucket_new ();
        bucket_foreach_key_value_pair (test_bucket,
                                       dont_call_this_bucket_iterator,
                                       NULL, NULL);
        
        /* Test that with 1 item, it gets called once, with and without value */
        MEDUSA_TEST_BOOLEAN_RESULT (insert_pair_into_bucket_if_it_fits (test_bucket, "foo", NULL));
        calls_counter = 0;
        bucket_foreach_key_value_pair (test_bucket,
                                       count_calls_to_bucket_iterator,
                                       &calls_counter, NULL);
        MEDUSA_TEST_INTEGER_RESULT (calls_counter, 1);
        bucket_free (test_bucket);
        
        test_bucket = bucket_new ();
        MEDUSA_TEST_BOOLEAN_RESULT (insert_pair_into_bucket_if_it_fits (test_bucket, "foo", "bar"));
        calls_counter = 0;
        bucket_foreach_key_value_pair (test_bucket,
                                       count_calls_to_bucket_iterator,
                                       &calls_counter, NULL);
        MEDUSA_TEST_INTEGER_RESULT (calls_counter, 1);
        bucket_free (test_bucket);

        /* Test that with a full bucket, each pair is called, with and without value */
        test_bucket_foreach_with_full_bucket (TRUE);
        test_bucket_foreach_with_full_bucket (FALSE);
}

static void
bucket_self_check (void)
{
        Bucket *test_bucket;
        char *expected_name;
        guint16 free_space, free_space_to_assign, bucket_depth;

        g_print ("Testing bucket functions...\n");
        set_bucket_size ();
        test_bucket = bucket_new ();

        get_bucket_free_space (test_bucket, &free_space);
        MEDUSA_TEST_INTEGER_RESULT (free_space, bucket_size - 2 * sizeof (guint16));

        free_space_to_assign = 32;
        memcpy (test_bucket->value, &free_space_to_assign, sizeof (guint16));
        get_bucket_free_space (test_bucket, &free_space);
        MEDUSA_TEST_INTEGER_RESULT (free_space, 32);

        free_space_to_assign = 0;
        memcpy (test_bucket->value, &free_space_to_assign, sizeof (guint16));
        get_bucket_free_space (test_bucket, &free_space);
        MEDUSA_TEST_INTEGER_RESULT (free_space, 0);

        g_print ("Testing bucket depth functions...\n");
        bucket_get_depth (test_bucket, &bucket_depth, 10);
        MEDUSA_TEST_INTEGER_RESULT (bucket_depth, 0);
        
        bucket_set_depth (test_bucket, 5);
        bucket_get_depth (test_bucket, &bucket_depth, 10);
        MEDUSA_TEST_INTEGER_RESULT (bucket_depth, 5);

        g_print ("Testing bucket file name functions...\n");

        expected_name = g_strdup_printf ("%s/%s%s%s", medusa_get_index_path (),
                                         "", PAGE_FILE_DESCRIPTION, "");
        MEDUSA_TEST_STRING_RESULT (generate_bucket_filename 
                                   (NULL, NULL, FALSE), 
                                   expected_name);
        g_free (expected_name);

        expected_name = g_strdup_printf ("%s/%s%s%s", medusa_get_index_path (),
                                         "hash-",  PAGE_FILE_DESCRIPTION, "");
        MEDUSA_TEST_STRING_RESULT (generate_bucket_filename 
                                   ("hash", NULL, FALSE), 
                                   expected_name);
        g_free (expected_name);

        expected_name = g_strdup_printf ("%s/%s%s%s", medusa_get_index_path (),
                                         "", PAGE_FILE_DESCRIPTION, "-index");
        MEDUSA_TEST_STRING_RESULT (generate_bucket_filename (NULL, "index", FALSE), 
                                   expected_name);
        g_free (expected_name);

        expected_name = g_strdup_printf ("%s/%s%s%s", medusa_get_index_path (),
                                         "hash-", PAGE_FILE_DESCRIPTION, 
                                         "-index");
        MEDUSA_TEST_STRING_RESULT (generate_bucket_filename 
                                   ("hash", "index", FALSE), 
                                   expected_name);
        g_free (expected_name);

        expected_name = g_strdup_printf ("%s/%s%s%s%s", 
                                         medusa_get_index_path (),
                                         "hash-", PAGE_FILE_DESCRIPTION, 
                                         "-index", 
                                         "." INDEX_IN_PROGRESS_SUFFIX);
        MEDUSA_TEST_STRING_RESULT (generate_bucket_filename 
                                   ("hash", "index", TRUE), 
                                   expected_name);
        g_free (expected_name);

        empty_bucket_has_correct_free_space_and_depth_self_check ();
        bucket_has_space_for_new_key_value_pair_self_check ();        
        bucket_foreach_key_value_pair_self_check ();
}

static void
directory_self_check (void)
{
        Directory *test_directory;
        guint bucket_number;

        g_print ("Testing directory functions...\n");

        /* Test creating a new directory creates a file name of an in progress file */
        test_directory = directory_new ("in-progress-self-check",
                                        "directory");
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_str_has_suffix (test_directory->filename,
                                                           INDEX_IN_PROGRESS_SUFFIX));
        directory_free (test_directory);
        MEDUSA_TEST_BOOLEAN_RESULT (directory_erase_constructed_index ("in-progress-self-check",
                                                                       "directory",
                                                                       TRUE));

        /* Test empty directory, and that doubling
           allows the directory to return more entry numbers that beforehand */
        test_directory = directory_new ("self-check",
                                        "directory");
        MEDUSA_TEST_BOOLEAN_RESULT (directory_is_empty (test_directory));
        create_first_directory_entry (test_directory);
        MEDUSA_TEST_BOOLEAN_RESULT (!directory_is_empty (test_directory));
        MEDUSA_TEST_INTEGER_RESULT (directory_get_depth (test_directory), 0);
        directory_depth_increment (test_directory);
        MEDUSA_TEST_INTEGER_RESULT (directory_get_depth (test_directory), 1);
        test_directory->depth = 0;
        MEDUSA_TEST_INTEGER_RESULT (directory_key_hash_to_entry_number (test_directory,  "foo"), 0);
        MEDUSA_TEST_INTEGER_RESULT (directory_key_hash_to_entry_number (test_directory,  "bar"), 0);
        directory_double (test_directory);
        MEDUSA_TEST_INTEGER_RESULT (directory_key_hash_to_entry_number (test_directory,  "foo"), 0);
        MEDUSA_TEST_INTEGER_RESULT (directory_key_hash_to_entry_number (test_directory,  "bar"), 1);
        directory_double (test_directory);
        MEDUSA_TEST_INTEGER_RESULT (directory_key_hash_to_entry_number (test_directory,  "foo"), 2);
        MEDUSA_TEST_INTEGER_RESULT (directory_key_hash_to_entry_number (test_directory,  "bar"), 3);
        directory_free (test_directory);

        /* Test directory doubling */
        test_directory = directory_new ("doubling-self-check", "directory");
        create_first_directory_entry (test_directory);
        MEDUSA_TEST_BOOLEAN_RESULT (directory_entry_set (test_directory, 0, 1));
        /* Create a directory with a single entry: "1" */
        MEDUSA_TEST_BOOLEAN_RESULT (read_bucket_number_from_directory_entry (test_directory,
                                                                             0,
                                                                             &bucket_number));
        MEDUSA_TEST_INTEGER_RESULT (bucket_number, 1);

        /* Double it and make sure it becomes "1, 1" */
        directory_double (test_directory);
        MEDUSA_TEST_INTEGER_RESULT (directory_get_depth (test_directory), 1);
        MEDUSA_TEST_BOOLEAN_RESULT (read_bucket_number_from_directory_entry (test_directory,
                                                                             0,
                                                                             &bucket_number));
        MEDUSA_TEST_INTEGER_RESULT (bucket_number, 1);
        MEDUSA_TEST_BOOLEAN_RESULT (read_bucket_number_from_directory_entry (test_directory,
                                                                             1,
                                                                             &bucket_number));
        MEDUSA_TEST_INTEGER_RESULT (bucket_number, 1);

        /* Double the directory "0, 1" and make sure it becomes "0, 1, 0, 1" */
        MEDUSA_TEST_BOOLEAN_RESULT (directory_entry_set (test_directory, 0, 0));
        directory_double (test_directory);
        MEDUSA_TEST_INTEGER_RESULT (directory_get_depth (test_directory), 2);
        MEDUSA_TEST_BOOLEAN_RESULT (read_bucket_number_from_directory_entry (test_directory,
                                                                             0,
                                                                             &bucket_number));
        MEDUSA_TEST_INTEGER_RESULT (bucket_number, 0);
        MEDUSA_TEST_BOOLEAN_RESULT (read_bucket_number_from_directory_entry (test_directory,
                                                                             1,
                                                                             &bucket_number));
        MEDUSA_TEST_INTEGER_RESULT (bucket_number, 1);
        MEDUSA_TEST_BOOLEAN_RESULT (read_bucket_number_from_directory_entry (test_directory,
                                                                             2,
                                                                             &bucket_number));
        MEDUSA_TEST_INTEGER_RESULT (bucket_number, 0);
        MEDUSA_TEST_BOOLEAN_RESULT (read_bucket_number_from_directory_entry (test_directory,
                                                                             3,
                                                                             &bucket_number));
        MEDUSA_TEST_INTEGER_RESULT (bucket_number, 1);
        directory_write_to_disk (test_directory);
        directory_free (test_directory);

        /* Test that the directory still looks good when loaded off the disk */
        directory_move_completed_into_place ("doubling-self-check", "directory");
        test_directory = directory_open ("doubling-self-check", "directory");
        MEDUSA_TEST_BOOLEAN_RESULT (test_directory != NULL);
        MEDUSA_TEST_BOOLEAN_RESULT (!directory_is_empty (test_directory));
        MEDUSA_TEST_INTEGER_RESULT (directory_get_depth (test_directory), 2);
        MEDUSA_TEST_BOOLEAN_RESULT (read_bucket_number_from_directory_entry (test_directory,
                                                                             0,
                                                                             &bucket_number));
        MEDUSA_TEST_INTEGER_RESULT (bucket_number, 0);
        MEDUSA_TEST_BOOLEAN_RESULT (read_bucket_number_from_directory_entry (test_directory,
                                                                             1,
                                                                             &bucket_number));
        MEDUSA_TEST_INTEGER_RESULT (bucket_number, 1);
        MEDUSA_TEST_BOOLEAN_RESULT (read_bucket_number_from_directory_entry (test_directory,
                                                                             2,
                                                                             &bucket_number));
        MEDUSA_TEST_INTEGER_RESULT (bucket_number, 0);
        MEDUSA_TEST_BOOLEAN_RESULT (read_bucket_number_from_directory_entry (test_directory,
                                                                             3,
                                                                             &bucket_number));
        MEDUSA_TEST_INTEGER_RESULT (bucket_number, 1);
        MEDUSA_TEST_BOOLEAN_RESULT (directory_write_to_disk (test_directory));
        directory_free (test_directory);
        directory_erase_constructed_index ("doubling-self_check", "directory", FALSE);

        /* Test opening an empty directory */
        MEDUSA_TEST_BOOLEAN_RESULT (directory_erase_constructed_index ("empty-self-check", "directory", TRUE));        
        test_directory = directory_new ("empty-self-check", "directory");
        MEDUSA_TEST_BOOLEAN_RESULT (directory_is_empty (test_directory));
        MEDUSA_TEST_BOOLEAN_RESULT (directory_write_to_disk (test_directory));
        MEDUSA_TEST_BOOLEAN_RESULT (directory_move_completed_into_place ("empty-self-check", "directory"));
        directory_free (test_directory);
        test_directory = directory_open ("empty-self-check", "directory");
        MEDUSA_TEST_BOOLEAN_RESULT (test_directory != NULL);
        MEDUSA_TEST_BOOLEAN_RESULT (directory_is_empty (test_directory));

        /* Test that you can write to the empty directory and it is happy */
        create_first_directory_entry (test_directory);
        MEDUSA_TEST_BOOLEAN_RESULT (!directory_is_empty (test_directory));
        directory_free (test_directory);
        directory_erase_constructed_index ("doubling-self-check", 
                                           "directory", FALSE);

        /* Test directory_generate_basename */
        MEDUSA_TEST_STRING_RESULT (directory_generate_basename (NULL), 
                                   DIRECTORY_DESCRIPTION);
        MEDUSA_TEST_STRING_RESULT (directory_generate_basename ("foo"), 
                                   "foo-" DIRECTORY_DESCRIPTION);
}

/* Tests that medusa_extensible_hash_erase_constructed_index works */
static void
erase_constructed_index_self_check (void)
{
        /* FIXME: Implement */
        /* Erase an in progress index */
        /* Erase a not in progress index */
        /* FIXME-3: Test that directories are removed */
        /* FIXME-3: Test that buckets are removed */
        /* FIXME-3: Test that nothing is removed that shouldn't be. */
}

static void
directory_becomes_nonempty_during_first_insert_self_check (void)
{
        MedusaExtensibleHash *hash;

        hash = medusa_extensible_hash_new ("self-check", 
                                           "directory-becomes-nonempty-during-first-insert");

        MEDUSA_TEST_BOOLEAN_RESULT (directory_is_empty (hash->directory));
        medusa_extensible_hash_insert (hash, "a", "b");
        MEDUSA_TEST_BOOLEAN_RESULT (!directory_is_empty (hash->directory));

        medusa_extensible_hash_free (hash);
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_erase_constructed_index ("self-check", 
                                                                                    "directory-becomes-nonempty-during-first-insert", TRUE));
}

static void
first_insert_adds_data_to_bucket (void)
{
        MedusaExtensibleHash *hash;
        Bucket *bucket;

        hash = medusa_extensible_hash_new ("self-check",
                                           "first-insert-adds-data-to-bucket");
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_insert (hash, "foo", "bar"));
        bucket = bucket_read (hash, 0);
        MEDUSA_TEST_STRING_RESULT (bucket->value + 2 * sizeof (guint16), "foo|bar");
        
        bucket_free (bucket);
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_insert (hash, "baz", "boo"));
        bucket = bucket_read (hash, 0);
        MEDUSA_TEST_BOOLEAN_RESULT (bucket != NULL);
        MEDUSA_TEST_STRING_RESULT (bucket->value + 2 * sizeof (guint16) + strlen ("foo|bar") + 1, "baz|boo");
        bucket_free (bucket);
        medusa_extensible_hash_free (hash);
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_erase_constructed_index ("self-check", 
                                                                                    "first-index-adds-data-to-bucket", TRUE));
}

static void
first_insert_self_check (void)
{
        directory_becomes_nonempty_during_first_insert_self_check ();
        first_insert_adds_data_to_bucket ();
}

static void
directory_pointers_satisfy_rep_invariant_test (MedusaExtensibleHash *hash)
{
        int bucket_number, i, j, entry_number;
        Bucket *bucket;
        guint16 bucket_depth, low_bits;

        for (i = 0; i < directory_get_number_of_entries (hash->directory); i++) {
                read_bucket_number_from_directory_entry (hash->directory,
                                                         i,
                                                         &bucket_number);
                MEDUSA_TEST_BOOLEAN_RESULT (bucket_number <= hash->number_of_buckets);
                bucket = bucket_read (hash, bucket_number);
                MEDUSA_TEST_BOOLEAN_RESULT (bucket_get_depth (bucket, &bucket_depth, 
                                                              directory_get_depth (hash->directory)));
                bucket_free (bucket);
                low_bits = i & ((1 << bucket_depth) - 1);
                for (j = 0; j < (1 << (directory_get_depth (hash->directory) - bucket_depth)); j++) {
                        entry_number = j * (1 << bucket_depth) | low_bits;
                        MEDUSA_TEST_INTEGER_RESULT (hash->directory->entries[entry_number], bucket_number);
                }                
        }
}


static void
insert_many_elements_self_check (void)
{
#ifdef STANDARD_DICTIONARY_PATH
        /* We need to test enough elements so that buckets will fill up and split.
           For this reason, we'll use the dictionary */
        MedusaExtensibleHash *hash;
        FILE *dictionary_stream;
        char *key, *value;
        char *missing_key;
        char *value_inserted;
        char *other_key_to_test, *other_value_to_test;
        gboolean key_exists, missing_key_exists;
        int test_count;

        g_print ("Testing medusa_extensible_hashes by inserting the words in the dictionary");
        hash = medusa_extensible_hash_new ("self-check",
                                           "multi-bucket-insert-works");
        dictionary_stream = medusa_fopen_error_cover (STANDARD_DICTIONARY_PATH, "r");
        MEDUSA_TEST_BOOLEAN_RESULT (dictionary_stream != NULL);

        key = medusa_readline_from_stream (dictionary_stream);
        value = medusa_readline_from_stream (dictionary_stream);
        missing_key  = medusa_readline_from_stream (dictionary_stream);
        test_count = 0;
        other_key_to_test = NULL;
        other_value_to_test = NULL;
        /* This test inserts the first entry hashed to the second
           entry, and doesn't insert a third.  We then test that that
           this succeeded by looking up the first entry, expecting to
           get the second, and looking up the third entry expecting to
           get NULL.  We then test an older element that has been
           inserted previously to make sure we can still find it. */
        while (key != NULL &&
               value != NULL &&
               missing_key != NULL) {
                MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_insert (hash, key, value));
                MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_lookup (hash, key, &key_exists, &value_inserted));
                MEDUSA_TEST_BOOLEAN_RESULT (key_exists);
                MEDUSA_TEST_STRING_RESULT (value_inserted, value);

                MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_lookup (hash, missing_key, 
                                                                           &missing_key_exists, &value_inserted));
                MEDUSA_TEST_BOOLEAN_RESULT (!missing_key_exists);

                if ((test_count % 100) == 0) {
                        g_free (other_key_to_test);
                        g_free (other_value_to_test);
                        other_key_to_test = g_strdup (key);
                        other_value_to_test = g_strdup (value);
                        directory_pointers_satisfy_rep_invariant_test (hash);
                }
                
                if (other_key_to_test != NULL) {
                        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_lookup (hash, other_key_to_test, 
                                                                                   &key_exists, &value_inserted));
                        MEDUSA_TEST_BOOLEAN_RESULT (key_exists);
                        MEDUSA_TEST_STRING_RESULT (value_inserted, other_value_to_test);
                }



                test_count++;

                if ((test_count % 500) == 0) {
                        g_print (".");
                }
                    
                g_free (key);
                g_free (value);
                g_free (missing_key);

                key = medusa_readline_from_stream (dictionary_stream);
                value = medusa_readline_from_stream (dictionary_stream);
                missing_key  = medusa_readline_from_stream (dictionary_stream);
        }

        g_print ("\n");
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_fclose_error_cover (dictionary_stream,
                                                               STANDARD_DICTIONARY_PATH));
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_erase_constructed_index ("self-check",
                                                                   "multi-bucket-insert-works", TRUE));
        g_free (key);
        g_free (value);
        g_free (missing_key);
        g_free (other_key_to_test);
        g_free (other_value_to_test);
        
#else
        g_warning ("Not testing extensible hash capability to insert "
                   "multiple elements, because there is no /usr/dict/words. "
                   "If a dictionary exists on another path on your "
                   "configuration, please add a test for it to "
                   "configure.in, or send information about it to "
                   "rebecka@caltech.edu.");
#endif

        
}

/* insert_marginal_elements_self_check
 *
 * Used to check that bad elements (containing higher characters, too
 * long, etc) will be correctly added to the hash
 */
static void 
insert_marginal_elements_self_check (void) 
{
        MedusaExtensibleHash *hash;
        char *very_long_word, *very_long_words_value;
        gboolean very_long_word_exists;
        
        
        g_print ("Testing medusa_extensible_hash_ability_to_handle_marginal_elements...");
        hash = medusa_extensible_hash_new ("self-check",
                                           "insert-marginal-elements");
        very_long_word = medusa_fixed_length_test_string_new (bucket_size + 1);
        /* We should ignore a word that is too long, and return */
        MEDUSA_TEST_BOOLEAN_RESULT (!medusa_extensible_hash_insert (hash, 
                                                                    very_long_word, NULL));
        MEDUSA_TEST_BOOLEAN_RESULT (!medusa_extensible_hash_lookup (hash, 
                                                                    very_long_word,
                                                                    &very_long_word_exists,
                                                                    &very_long_words_value));
                                    
        g_print ("\n");
        
}

/* dont_call_this_function
 *
 * Used to test the medusa_extensible_hash_foreach function.
 * We pass this function to the foreach with an empty hash,
 * so this should never get called.  If it does, it asserts and dies.
 *
 * @key       Hypothetical hash key
 * @value     Hypothetical hash value
 * @user_data Hypothetical data passed by foreach */
static void
dont_call_this_function (const char *key,
                         const char *value,
                         gpointer    user_data)
{
        g_print ("Function passed to medusa_extensible_hash_foreach was called "
                 "even though there were no elements in the hash.\n");
        abort ();
}

/* verify_foreach_argument_marshalling
 *
 * Used as part of the tests for medusa_extensible_hash_foreach.
 * Checks that the arguments passed to the foreach function are 
 * correct, and in the right order.  
 *
 * @key       Key passed in by medusa_extensible_hash_foreach
 * @value     Value passed in by medusa_extensible_hash_foreach
 * @data      Data passed in by medusa_extensible_hash_foreach
 */
static void
verify_foreach_argument_marshalling (const char *key,
                                     const char *value,
                                     gpointer data)
{
       MEDUSA_TEST_STRING_RESULT (key, "single-element");
       MEDUSA_TEST_STRING_RESULT (value, "value");
       MEDUSA_TEST_POINTER_VALUE (data, NULL);
}

/* count_calls_to_foreach
 *
 * Used as part of the tests for medusa_extensible_hash_foreach.
 * Increments the counter passed
 * in with each call, so we can count the number of calls to the function 
 *
 * @key       Key passed in by medusa_extensible_hash_foreach
 * @value     Value passed in by medusa_extensible_hash_foreach
 * @data      Data passed in by medusa_extensible_hash_foreach
 */
static void
count_calls_to_foreach (const char *key,
                        const char *value,
                        gpointer data)
{
        int *counter;
        MEDUSA_TEST_BOOLEAN_RESULT (data != NULL);

        counter = (int *) data;
        *counter = *counter + 1;
}

/* mark_iterated_hash_elements
 *
 * Used by the extensible_hash_foreach test to keep track of which
 * elements were iterated over by medusa_extensible_hash_foreach.
 * Takes an extensible hash key, and a ghash table which should contain
 * all of the elements in the extensible hash structure, originally hashed to the
 * string "uniterated"  Fails if any string iterated over either
 * - is not in the ghashtable
 * - has already been iterated over (been marked)
 *
 * @key       Key passed in by medusa_extensible_hash_foreach
 * @value     Value passed in by medusa_extensible_hash_foreach
 * @data      Data passed in by medusa_extensible_hash_foreach
 */
static void
mark_iterated_hash_elements (const char *key,
                             const char *value,
                             gpointer data)
{
        GHashTable *marked_nodes_hash_table;
        char *old_key, *old_value;

        MEDUSA_TEST_BOOLEAN_RESULT (key != NULL);
        MEDUSA_TEST_BOOLEAN_RESULT (value == NULL);
        MEDUSA_TEST_BOOLEAN_RESULT (data != NULL);

        marked_nodes_hash_table = (GHashTable *) data;

        MEDUSA_TEST_BOOLEAN_RESULT (g_hash_table_lookup_extended (marked_nodes_hash_table, 
                                                                  key,
                                                                  (gpointer *) &old_key, 
                                                                  (gpointer *) &old_value));
        MEDUSA_TEST_POINTER_VALUE (old_value, UNITERATED);
        g_hash_table_remove (marked_nodes_hash_table, key);
        g_hash_table_insert (marked_nodes_hash_table, old_key, ITERATED);
}

static void
hash_foreach_self_check (void)
{
        /* Simple test with one element */
        MedusaExtensibleHash *hash;
        int counter;
        FILE *dictionary;
        char *key;
        GHashTable *g_hash_table;

        g_print ("Testing medusa_extensible_hash_foreach");
        hash = medusa_extensible_hash_new ("foreach-self-check", NULL);
        MEDUSA_TEST_BOOLEAN_RESULT (hash != NULL);

        /* Test that with zero elements, the function gets called 0 times */
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_foreach (hash, 
                                                                    dont_call_this_function,
                                                                    NULL));

        /* Test that with one element a function gets called exactly one time,
           and with the right arguments */
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_insert (hash, "single-element", "value"));

        counter = 0;
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_foreach (hash,
                                                                    count_calls_to_foreach,
                                                                    &counter));
        MEDUSA_TEST_INTEGER_RESULT (counter, 1);
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_foreach (hash,
                                                                    verify_foreach_argument_marshalling,
                                                                    NULL));
        medusa_extensible_hash_free (hash);

#ifdef STANDARD_DICTIONARY_PATH
        /* Create a hash of all the words in the dictionary to test that foreach works over
           many elements */
        hash = medusa_extensible_hash_new ("foreach-self-check", NULL);
        g_hash_table = g_hash_table_new (g_str_hash, g_str_equal);
        dictionary = medusa_fopen_error_cover (STANDARD_DICTIONARY_PATH, "r");
        MEDUSA_TEST_BOOLEAN_RESULT (dictionary != NULL);
        while ((key = medusa_readline_from_stream (dictionary)) != NULL) {
                MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_insert (hash,
                                                                           key,
                                                                           NULL));
                if (rand () % 1500 == 0) {
                        g_print (".");
                }
                g_hash_table_insert (g_hash_table, key, UNITERATED);
        }
        g_print ("\n");
        /* Test that the number of iterations equals the number of entries in the hash table */        
#if 0
        counter = 0;
        medusa_extensible_hash_foreach (hash,
                                        count_calls_to_foreach, 
                                        &counter);
        MEDUSA_TEST_INTEGER_RESULT (counter, g_hash_table_size (g_hash_table));
#endif

        /* This will fail if any item is iterated over twice, or if 
           any item is iterated over that isn't in the g_hash_table table */
        medusa_extensible_hash_foreach (hash,
                                        mark_iterated_hash_elements,
                                        g_hash_table);

        /* Test that every item gets found */
        g_hash_table_foreach (g_hash_table,
                              fail_if_uniterated_elements_exist,
                              hash);

#else
        g_warning ("Also not testing medusa_extensible_hash_foreach very well because no dictionary file "
                   "could be found on the system.\n");
#endif

}


static void
move_completed_index_into_place_self_check (void) {
        char *directory_filename_base;
        char *old_directory_filename, *new_directory_filename;
        char *old_bucket_filename, *new_bucket_filename;
        MedusaExtensibleHash *hash;

        g_print ("Testing medusa_extensible_hash_move_completed_index_into_place...\n");

        /* Move an empty index.  Test that the old files are gone and the new index can be opened correctly */
        directory_filename_base = directory_generate_basename ("empty-hash");
        old_directory_filename = medusa_generate_index_filename (directory_filename_base, "move-completed-index-self-check", TRUE);
        new_directory_filename = medusa_generate_index_filename (directory_filename_base, "move-completed-index-self-check", FALSE);
        g_free (directory_filename_base);

        old_bucket_filename = generate_bucket_filename ("empty-hash",
                                                        "move-completed-index-self-check",
                                                        TRUE);
        new_bucket_filename = generate_bucket_filename ("empty-hash",
                                                        "move-completed-index-self-check",
                                                        FALSE);

        /* Erase the old test index, if old tests failed */
        MEDUSA_TEST_BOOLEAN_RESULT 
                (medusa_extensible_hash_erase_constructed_index 
                 ("move-completed-index-self-check", 
                  "empty-hash",
                  FALSE));
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_erase_constructed_index 
                                    ("move-completed-index-self-check", 
                                     "empty-hash",
                                     TRUE));
        MEDUSA_TEST_INTEGER_RESULT (access (old_directory_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (old_bucket_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (new_directory_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (new_bucket_filename, F_OK), -1);

        /* Create a new in progress hash and write it to disk */
        hash = medusa_extensible_hash_new ("move-completed-index-self-check", 
                                           "empty-hash");
        MEDUSA_TEST_BOOLEAN_RESULT (hash != NULL);
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_free (hash));
        MEDUSA_TEST_INTEGER_RESULT (access (old_directory_filename, F_OK), 0);
        MEDUSA_TEST_INTEGER_RESULT (access (old_bucket_filename, F_OK), 0);
        MEDUSA_TEST_INTEGER_RESULT (access (new_directory_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (new_bucket_filename, F_OK), -1);

        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_move_completed_index_into_place 
                                    ("move-completed-index-self-check", 
                                     "empty-hash"));
        MEDUSA_TEST_INTEGER_RESULT (access (old_directory_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (old_bucket_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (new_directory_filename, F_OK), 0);
        MEDUSA_TEST_INTEGER_RESULT (access (new_bucket_filename, F_OK), 0);

        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_erase_constructed_index 
                                    ("move-completed-index-self-check", 
                                     "empty-hash", FALSE));
        MEDUSA_TEST_INTEGER_RESULT (access (old_directory_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (old_bucket_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (new_directory_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (new_bucket_filename, F_OK), -1);

        g_free (old_directory_filename);
        g_free (new_directory_filename);
        g_free (old_bucket_filename);
        g_free (new_bucket_filename);

        /* Move an index with words in it */
        directory_filename_base = directory_generate_basename ("bucket-with-words");
        old_directory_filename = medusa_generate_index_filename (directory_filename_base, 
                                                                 "move-completed-index-self-check", 
                                                                 TRUE);
        new_directory_filename = medusa_generate_index_filename (directory_filename_base, "move-completed-index-self-check", FALSE);
        g_free (directory_filename_base);

        old_bucket_filename = generate_bucket_filename ("bucket-with-words",
                                                        "move-completed-index-self-check", 
                                                        TRUE);
        new_bucket_filename = generate_bucket_filename ("bucket-with-words",
                                                        "move-completed-index-self-check",
                                                        FALSE);

        /* Erase the old test index, if old tests failed */
        MEDUSA_TEST_BOOLEAN_RESULT 
                (medusa_extensible_hash_erase_constructed_index 
                 ("move-completed-index-self-check", 
                  "bucket-with-words",
                  FALSE));
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_erase_constructed_index 
                                    ("move-completed-index-self-check", 
                                     "bucket-with-words",
                                     TRUE));
        MEDUSA_TEST_INTEGER_RESULT (access (old_directory_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (old_bucket_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (new_directory_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (new_bucket_filename, F_OK), -1);


        hash = medusa_extensible_hash_new ("move-completed-index-self-check",
                                           "bucket-with-words");
        MEDUSA_TEST_BOOLEAN_RESULT (hash != NULL);
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_insert (hash, "foo", NULL));
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_insert (hash, "bar", "baz"));
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_free (hash));

        MEDUSA_TEST_INTEGER_RESULT (access (old_directory_filename, F_OK), 0);
        MEDUSA_TEST_INTEGER_RESULT (access (old_bucket_filename, F_OK), 0);
        MEDUSA_TEST_INTEGER_RESULT (access (new_directory_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (new_bucket_filename, F_OK), -1);

        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_move_completed_index_into_place ("move-completed-index-self-check", 
                                                                                            "bucket-with-words"));
        MEDUSA_TEST_INTEGER_RESULT (access (old_directory_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (old_bucket_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (new_directory_filename, F_OK), 0);
        MEDUSA_TEST_INTEGER_RESULT (access (new_bucket_filename, F_OK), 0);

        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_erase_constructed_index 
                                    ("move-completed-index-self-check", 
                                     "bucket-with-words", FALSE));
        MEDUSA_TEST_INTEGER_RESULT (access (old_directory_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (old_bucket_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (new_directory_filename, F_OK), -1);
        MEDUSA_TEST_INTEGER_RESULT (access (new_bucket_filename, F_OK), -1);

        g_free (old_directory_filename);
        g_free (new_directory_filename);
        g_free (old_bucket_filename);
        g_free (new_bucket_filename);
}

static void
index_file_functions_self_check (void)
{
        move_completed_index_into_place_self_check ();
        erase_constructed_index_self_check ();
}

static void
open_existing_hash_self_check (void)
{
        MedusaExtensibleHash *hash;
        char *foos_value, *bazs_value, *oops_value;
        gboolean foo_exists, baz_exists, oops_exists;

        hash = medusa_extensible_hash_new ("self-check",
                                           "open-existing-hash");
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_insert (hash,
                                                                   "foo", 
                                                                   "bar"));
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_insert (hash,
                                                                   "baz", 
                                                                   NULL));
        medusa_extensible_hash_free (hash);

        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_move_completed_index_into_place ("self-check", 
                                                                                            "open-existing-hash"));

        hash = medusa_extensible_hash_open ("self-check",
                                            "open-existing-hash");
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_lookup (hash,
                                                                   "foo",
                                                                   &foo_exists,
                                                                   &foos_value));
        MEDUSA_TEST_BOOLEAN_RESULT (foo_exists);
        MEDUSA_TEST_STRING_RESULT (foos_value, "bar");
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_lookup (hash,
                                                                   "baz",
                                                                   &baz_exists,
                                                                   &bazs_value));
        MEDUSA_TEST_BOOLEAN_RESULT (baz_exists);
        MEDUSA_TEST_POINTER_VALUE (bazs_value, NULL);
        MEDUSA_TEST_BOOLEAN_RESULT (medusa_extensible_hash_lookup (hash,
                                                                   "oops",
                                                                   &oops_exists,
                                                                   &oops_value));

        MEDUSA_TEST_BOOLEAN_RESULT (!oops_exists);

        /* FIXME: Test opening an empty hash */
        
                                    
}

void
medusa_extensible_hash_self_check (void)
{
        g_print ("Self checking the extensible hash:\n");

        /* Unit tests */
        key_value_self_check ();
        bucket_self_check ();
        directory_self_check ();
        
        /* API/Overall functionality tests */
        empty_hash_self_check ();
        first_insert_self_check ();
        insert_and_lookup_self_check ();
        insert_many_elements_self_check ();
        insert_marginal_elements_self_check ();
        hash_foreach_self_check ();
        index_file_functions_self_check ();
        open_existing_hash_self_check ();
}
