/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  medusa-plaintext-utilities.h -- Text parsing utility
 *  functions used by several text indexing modules
 *
 *  Copyright (C) 2000 Eazel, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@eazel.com>
 */

#ifndef MEDUSA_PLAINTEXT_UTILITIES_H
#define MEDUSA_PLAINTEXT_UTILITIES_H

#include <glib.h>
#include <libgnomevfs/gnome-vfs-types.h>

/* Takes a file full of stoplist words, and turns it into a hash table */
GHashTable * medusa_stoplist_file_to_hash_table                   (char *word_file_name);


/* Reads a block of data and adds the new words to the word_hash naively */
char *       medusa_plaintext_read_data_block                     (GHashTable *word_hash,
								   GnomeVFSHandle *uri_handle,
								   int *words_read,
								   char *last_word_fragment,
								   gboolean *more_data_to_read);


/* Reads a block of data and adds the new words to the word_hash,
   excepting the words in the stoplist */
char *       medusa_plaintext_read_data_block_with_stoplist       (GHashTable *word_hash,
								   GnomeVFSHandle *uri_handle,
								   GHashTable *stop_list,
								   int *words_read,
								   char *last_word_fragment,
								   gboolean *more_data_to_read);

char *       medusa_plaintext_read_data_block_with_preprocessor   (GHashTable *word_hash,
								   GnomeVFSHandle *uri_handle,
								   MedusaPreprocessorFunc preprocess,
								   int *words_read,
								   char *last_word_fragment,
								   gboolean *more_data_to_read);

#endif /* MEDUSA_PLAINTEXT_UTILITIES_H */


