/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  medusa-lexicon.c - Implementation of a lexicon data structure, which stores
 *  words that exist in the text index
 *
 *  Copyright (C) 2001 Rebecca Schulman
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@ai.mit.edu>
 * 
 */

#include <glib.h>
#include <string.h>

#include <libmedusa/medusa-log.h>
#include "medusa-test.h"
#include "medusa-extensible-hash.h"

#include "medusa-lexicon.h"

#define MEDUSA_LEXICON_HASH_NAME "lexicon"
/* We can't use tokens where one of the bytes is 0, because
   the bytes are cast as chars to be inserted into the hash
   table.  Add a placeholder to the data that doesn't contain any zeroes,
   and is too big to be any other integer */
#define PLACEHOLDER_TOKEN 0xFFFFFFFF

/* The lexicon is built on top of the disk storage for 
   words, which is currently the extensible hash data structure.
   The lexicon should hopefully cache most values of the words */
   
typedef struct MedusaLexiconCache MedusaLexiconCache;

struct MedusaLexicon {
        MedusaExtensibleHash *disk_storage;
	MedusaLexiconCache *cache;
};

#define CACHE_SETS 2048
#define CACHE_ASSOC 8

struct MedusaLexiconCache {
	struct {
		char *key[CACHE_ASSOC];
		guint key_hash[CACHE_ASSOC];
		char *value[CACHE_ASSOC];
		guint32 age[CACHE_ASSOC];
		char dirty[CACHE_ASSOC];
	} sets[CACHE_SETS];
	int dirty_items;
	int hits, misses;
};

static guint32 cache_clock = 0;

/* We encode integers into 28 bytes so that there will be no
   0 characters in them, so they can be viewed as strings by the
   extensible hash data structure. 
   The number looks like this
   1 [bytes 28 - 22] 1 [bytes 21 - 15] 1 [bytes 14 - 8] 1 [bytes 7 - 1] */
static void
encode_integer_as_string (char *buffer, guint32 number)
{
        guint32 encoded_number;

        g_assert (buffer != NULL);

        if (number >= 1 << 28) {
                medusa_log_fatal_error ("Overflow trying to encode a "
                                        "large integer into the word "
                                        "lexicon.  Please tell "
                                        "rebecka@caltech.edu about "
                                        "this error.");
        }

        encoded_number = 0x80808080 +              /* The 1's */ 
                (number & 0x0000007F) +           /* bytes 1 - 7 */
                ((number & 0x00003F80) << 1) +    /* bytes 8 - 14 */
                ((number & 0x001FC000) << 2) +    /* bytes 15 - 21 */
                ((number & 0x0FE00000) << 3);     /* bytes 22 - 28 */

        memcpy (buffer, &encoded_number, sizeof(guint32));
}

static guint32
decode_integer (guint32 encoded_integer)
{
        return (encoded_integer & 0x0000007F) +
                ((encoded_integer & 0x00007F00) >> 1) +
                ((encoded_integer & 0x007F0000) >> 2) + 
                ((encoded_integer & 0x7F000000) >> 3);
        
}

static void
decode_string_into_integer (const char *string, guint32 *integer)
{
        guint32 encoded_integer;
 
        g_assert (string != NULL);
        g_assert (integer != NULL);
        g_assert (strlen (string) >= sizeof (guint32));

        memcpy (&encoded_integer, string, sizeof (guint32));

        *integer = decode_integer (encoded_integer);
}
static void
encode_two_integers_as_string (char *buffer, 
                               guint32 first, 
                               guint32 second, 
                               gboolean second_integer_should_be_encoded)
{
        g_assert (buffer != NULL);
        
        encode_integer_as_string (buffer, first);
        if (second_integer_should_be_encoded) {
                encode_integer_as_string (&buffer[sizeof (guint32)], second);
        }
        else {
                memcpy (&buffer[sizeof (guint32)], &second, sizeof (guint32));
        }

        buffer[ 2 * sizeof (guint32) / sizeof (char)] = 0;
}

static void
decode_string_into_two_integers (const char *string, guint32 *first, guint32 *second)
{
        guint32 encoded_integer;

        g_assert (string != NULL);
        g_assert (first != NULL);
        g_assert (second != NULL);
        g_assert (strlen (string) == 2 * sizeof (guint32));

        memcpy (&encoded_integer, string, sizeof (guint32));
        *first = decode_integer (encoded_integer);
        memcpy (&encoded_integer, 
                string + sizeof (guint32) / sizeof (char), 
                sizeof (guint32));
        *second = decode_integer (encoded_integer);
}


/* cache */

/* Look for a cached copy of KEY in CACHE. */
static const char *
cache_ref (MedusaLexicon *lexicon, const char *key)
{
	MedusaLexiconCache *cache = lexicon->cache;
	int set, i;
	guint hash;

	g_return_val_if_fail (key != NULL, NULL);

	if (cache == NULL) {
		return NULL;
	}

	hash = g_str_hash (key);
	set = hash % CACHE_SETS;
	for (i = 0; i < CACHE_ASSOC; i++) {
		if (cache->sets[set].key[i] == NULL) {
			continue;
		} else if (cache->sets[set].key_hash[i] == hash
			   && strcmp (cache->sets[set].key[i], key) == 0) {
			cache->sets[set].age[i] = ++cache_clock;
			cache->hits++;
			return cache->sets[set].value[i];
		}
	}
	cache->misses++;

	return NULL;
}

/* Add a copy of KEY->VALUE in CACHE. KEY and VALUE are copied */
static gboolean
cache_set (MedusaLexicon *lexicon, const char *key, const char *value)
{
	MedusaLexiconCache *cache = lexicon->cache;
	int set, i, empty = -1, oldest = -1;
	guint32 oldest_age = ~0;
	guint hash;

	g_return_val_if_fail (key != NULL, TRUE);

        /* FIXME: While this is technically an implementation detail in
         * medusa-extensible-hashtable.c, it does NOT appear to currently
         * handle the error case where key contains '|', which it uses as
         * the on-disk seperator between key|value pairs. Its easier to
         * debug problems like this if we see them when they're added here
         * rather than catching on the inevitable assert later.
         */
        if (strchr (key, '|') != NULL) {
                return FALSE;
        }

        g_assert (strchr (key, '|') == NULL);


	/* FIXME: value may be NULL in the future.. */
	g_return_val_if_fail (value != NULL, TRUE);

	if (cache == NULL) {
		return TRUE;
	}

	hash = g_str_hash (key);
	set = hash % CACHE_SETS;
	for (i = 0; i < CACHE_ASSOC; i++) {
		if (cache->sets[set].key[i] != NULL) {
			if (cache->sets[set].key_hash[i] == hash
			    && strcmp (cache->sets[set].key[i], key) == 0) {
				if (strlen (cache->sets[set].value[i]) >= strlen (value)) {
					strcpy (cache->sets[set].value[i], value);
				} else {
					g_free (cache->sets[set].value[i]);
					cache->sets[set].value[i] = g_strdup (value);
				}
				cache->sets[set].age[i] = ++cache_clock;
				if (!cache->sets[set].dirty[i]) {
					cache->dirty_items++;
				}
				cache->sets[set].dirty[i] = TRUE;
				return TRUE;
			} else if (cache->sets[set].age[i] < oldest_age) {
				oldest = i;
				oldest_age = cache->sets[set].age[i];
			}
		} else if (empty == -1) {
			empty = i;
		}
	}

	g_assert (empty != -1 || oldest != -1);

	if (empty == -1) {
		empty = oldest;
	}

	if (cache->sets[set].key[empty] != NULL) {
		if (cache->sets[set].dirty[empty]) {
			if (!medusa_extensible_hash_insert (lexicon->disk_storage,
							    cache->sets[set].key[empty],
							    cache->sets[set].value[empty])) {
				/* I/O failed; can't add the new item to the cache */
				return FALSE;
			}
			cache->sets[set].dirty[empty] = FALSE;
			cache->dirty_items--;
		}
		g_free (cache->sets[set].key[empty]);
		g_free (cache->sets[set].value[empty]);
	}

	cache->sets[set].key[empty] = g_strdup (key);
	cache->sets[set].key_hash[empty] = hash;
	cache->sets[set].value[empty] = g_strdup (value);
	cache->sets[set].age[empty] = ++cache_clock;
	cache->sets[set].dirty[empty] = TRUE;
	cache->dirty_items++;

	return TRUE;
}

/* Lose all cached data in CACHE */
static gboolean
cache_flush (MedusaLexicon *lexicon)
{
	MedusaLexiconCache *cache = lexicon->cache;
	int set, line;
	gboolean errors;

	if (cache == NULL || cache->dirty_items == 0) {
		return TRUE;
	}

	errors = FALSE;
	for (set = 0; set < CACHE_SETS; set++) {
		for (line = 0; line < CACHE_ASSOC; line++) {
			if (cache->sets[set].key[line] != NULL) {
				if (cache->sets[set].dirty[line]) {
					if (medusa_extensible_hash_insert (lexicon->disk_storage,
									   cache->sets[set].key[line],
									   cache->sets[set].value[line])) {
						cache->sets[set].dirty[line] = FALSE;
						cache->dirty_items--;
					} else {
						errors = TRUE;
					}
				}

				g_free (cache->sets[set].key[line]);
				g_free (cache->sets[set].value[line]);
				cache->sets[set].key[line] = 0;
				cache->sets[set].value[line] = 0;
			}
		}
		if (cache->dirty_items == 0) {
			break;
		}
	}
	g_assert (cache->dirty_items == 0);

	if (errors) {
		medusa_log_error ("Warning: lost dirty data while flushing lexicon!\n");
	}

	return !errors;
}

static MedusaLexiconCache *
cache_new (void)
{
	return g_new0 (MedusaLexiconCache, 1);
}

static void
cache_free (MedusaLexicon *lexicon)
{
	MedusaLexiconCache *cache = lexicon->cache;

	cache_flush (lexicon);

	if (cache == NULL) {
		return;
	}

	medusa_log_event (MEDUSA_DB_LOG_ABBREVIATED,
			  "Cache miss ratio: %g (%g accesses)\n",
			  cache->misses / ((double) cache->hits + cache->misses),
			  (double) cache->hits + cache->misses);

	g_free (cache);
	lexicon->cache = NULL;
}



MedusaLexicon *
medusa_lexicon_new (const char *index_name)
{
        MedusaLexicon *lexicon;

        lexicon = g_new (MedusaLexicon, 1);
        
        lexicon->disk_storage = medusa_extensible_hash_new (index_name,
                                                            MEDUSA_LEXICON_HASH_NAME);

	lexicon->cache = cache_new ();

        return lexicon;
}


MedusaLexicon *
medusa_lexicon_open (const char *index_name)
{
        MedusaLexicon *lexicon;

        lexicon = g_new (MedusaLexicon, 1);
        
        lexicon->disk_storage = medusa_extensible_hash_open (index_name,
                                                             MEDUSA_LEXICON_HASH_NAME);
        /* We probably won't need a cache in the "open" case, since we'll only be doing a few lookups. */
	lexicon->cache = NULL;

        return lexicon;
}


gboolean
medusa_lexicon_insert  (MedusaLexicon *lexicon,
                        const char    *word,
                        guint32        last_occurrence)
{
        char value_string[9];

        g_return_val_if_fail (lexicon != NULL, FALSE);
        g_return_val_if_fail (word != NULL, FALSE);
        g_assert (sizeof(guint32) == 4);

        encode_two_integers_as_string (value_string, last_occurrence, PLACEHOLDER_TOKEN, FALSE);

	if (lexicon->cache != NULL) {
		return cache_set (lexicon, word, value_string);
	} else {
		return medusa_extensible_hash_insert (lexicon->disk_storage, 
						      word,
						      value_string);
	}
}



typedef struct {
        MedusaExtensibleHashForeachFunc user_function;
        int segment_number;
        int total_segments;
        gpointer user_function_data;
} ForeachData;

static void
call_user_function_if_word_is_in_segment (const char *key,
                                          const char *value,
                                          gpointer data)
{
        ForeachData *foreach_data;

        g_assert (key != NULL);
        g_assert (data != NULL);

        foreach_data = (ForeachData *) data;


        if ((g_str_hash (key) % foreach_data->total_segments) ==
            foreach_data->segment_number) {
                foreach_data->user_function (key, value, foreach_data->user_function_data);
        }
}

gboolean
medusa_lexicon_foreach_in_segment (MedusaLexicon *lexicon,
                                   int segment_number,
                                   int total_segments,
                                   MedusaExtensibleHashForeachFunc user_function,
                                   gpointer user_function_data)
{
        ForeachData foreach_data;

        g_return_val_if_fail (lexicon != NULL, FALSE);
        g_return_val_if_fail (segment_number >= 0, FALSE);
        g_return_val_if_fail (segment_number < total_segments, FALSE);
        g_return_val_if_fail (user_function != NULL, FALSE);

	if (lexicon->cache != NULL) {
		cache_flush (lexicon);
	}

        /* We never want to speed this up using the cache */
        foreach_data.user_function = user_function;
        foreach_data.segment_number = segment_number;
        foreach_data.total_segments = total_segments;
        foreach_data.user_function_data = user_function_data;
        return medusa_extensible_hash_foreach (lexicon->disk_storage,
                                               call_user_function_if_word_is_in_segment,
                                               &foreach_data);
}
                                   
                                   
gboolean
medusa_lexicon_set_index_locations (MedusaLexicon *lexicon,
                                    const char    *word,
                                    guint32        start_location,
                                    guint32        end_location)
{
        char value_string[9];

        g_return_val_if_fail (lexicon != NULL, FALSE);
        g_return_val_if_fail (word != NULL, FALSE);
        g_assert (sizeof (guint32) == 4);

        encode_two_integers_as_string (value_string, start_location, end_location, TRUE);

        return medusa_extensible_hash_insert (lexicon->disk_storage, 
                                              word,
                                              value_string);
}


gboolean
medusa_lexicon_lookup_index_locations (MedusaLexicon   *lexicon,
                                       const char      *word,
                                       guint32         *start_location,
                                       guint32         *end_location)
{
        gboolean io_was_successful, word_exists;
        char *data;

        g_return_val_if_fail (lexicon != NULL, FALSE);
        g_return_val_if_fail (word != NULL, FALSE);
        g_return_val_if_fail (start_location != NULL, FALSE);
        g_return_val_if_fail (end_location != NULL, FALSE);

        io_was_successful = medusa_extensible_hash_lookup (lexicon->disk_storage, word, &word_exists, &data);

        if (!io_was_successful || !word_exists) {
                return FALSE;
        }
        
        /* FIXME: What does this do if the string doesn't actually contain two integers? */
        decode_string_into_two_integers (data, start_location, end_location);
	g_free (data);
        return TRUE;
}


gboolean
medusa_lexicon_get_last_occurrence (MedusaLexicon *lexicon,
                                    const char *word,
                                    guint32    *last_occurrence)
{
        gboolean io_was_successful, word_exists;
        char *data;

        g_return_val_if_fail (lexicon != NULL, FALSE);
        g_return_val_if_fail (word != NULL, FALSE);
        g_return_val_if_fail (last_occurrence != NULL, FALSE);

	if (lexicon->cache != NULL) {
		const char *tem;
		tem = cache_ref (lexicon, word);
		if (tem != NULL) {
			decode_string_into_integer (tem, last_occurrence);
			return TRUE;
		}
	}

        io_was_successful = medusa_extensible_hash_lookup (lexicon->disk_storage, word, &word_exists, &data);

        if (!io_was_successful) {
                return FALSE;
        }

        if (!word_exists) {
                *last_occurrence = MEDUSA_WORD_HAS_NO_PREVIOUS_OCCURRENCE;
                g_free (data);
                return TRUE;
        }

        /* FIXME: What does this do if the string doesn't actually
           contain two integers? */
        decode_string_into_integer (data, last_occurrence);
        g_free (data);

        return TRUE;
}

void
medusa_lexicon_decode_last_occurrence (const char *last_occurrence,
                                       guint32    *cell_number) 
{
        g_return_if_fail (last_occurrence != NULL);
        g_return_if_fail (strlen (last_occurrence) >= sizeof (guint32));

        decode_string_into_integer (last_occurrence, cell_number);
}

gboolean
medusa_lexicon_move_completed_index_into_place (const char *index_name)
{
        return medusa_extensible_hash_move_completed_index_into_place (index_name, MEDUSA_LEXICON_HASH_NAME);
}


gboolean
medusa_lexicon_erase_constructed_index (const char *index_name,
                                             gboolean erase_in_progress_index)
{
        return medusa_extensible_hash_erase_constructed_index (index_name, 
                                                               MEDUSA_LEXICON_HASH_NAME, 
                                                               erase_in_progress_index);
}


gboolean
medusa_lexicon_free (MedusaLexicon *lexicon)
{
	if (lexicon->cache != NULL) {
		cache_free (lexicon);
	}

        if (!medusa_extensible_hash_free (lexicon->disk_storage)) {
                return FALSE;
        }

        g_free (lexicon);
        return TRUE;
}


#include <stdlib.h>
#include <time.h>

#define NUMBER_OF_RANDOM_TESTS 1000

static void
integer_encoding_functions_self_check (void)
{
        char test_buffer[5];
        char long_test_buffer[9];
        int i;
        guint32 sample_value, decoded_sample_value;
        guint32 first_number, second_number;
        guint32 first_number_decoded, second_number_decoded;

        g_print ("Testing encoding and decoding functions...\n");


        srand (time (NULL));
         for (i = 0; i < NUMBER_OF_RANDOM_TESTS; i++) {
                 sample_value = rand () >> 4;
                 
                 memset (test_buffer, 0, 5);                 
                 encode_integer_as_string (test_buffer, sample_value);

                /* Test that there are no numbers that cause strings to be 
                   terminated early */
                MEDUSA_TEST_INTEGER_RESULT (strlen (test_buffer), sizeof (guint32));

                /* Test that encoding then decoding returns the original value */
                decode_string_into_integer (test_buffer, &decoded_sample_value);
                MEDUSA_TEST_INTEGER_RESULT (decoded_sample_value, sample_value);
                
        }        

         /* Test encoding with the placeholder token */
         for (i = 0; i < NUMBER_OF_RANDOM_TESTS; i++) {
                 sample_value = rand () >> 4;

                 memset (long_test_buffer, 0, 9);
                 encode_two_integers_as_string (long_test_buffer, sample_value, PLACEHOLDER_TOKEN, FALSE);
                 MEDUSA_TEST_INTEGER_RESULT (strlen (long_test_buffer), 2 * sizeof (guint32));
                 
                 decode_string_into_integer (long_test_buffer, &decoded_sample_value);
                 MEDUSA_TEST_INTEGER_RESULT (decoded_sample_value, sample_value);
         }

        /* Do the random tests for two numbers at once, too */
        for (i = 0; i < NUMBER_OF_RANDOM_TESTS; i++) {
                first_number = rand () >> 4;
                second_number = rand () >> 4;
                
                encode_two_integers_as_string (long_test_buffer, first_number, second_number, TRUE);
                
                MEDUSA_TEST_INTEGER_RESULT (strlen (long_test_buffer), 2 * sizeof (guint32));

                decode_string_into_two_integers (long_test_buffer, &first_number_decoded, &second_number_decoded);
                
                MEDUSA_TEST_INTEGER_RESULT (first_number, first_number_decoded);
                MEDUSA_TEST_INTEGER_RESULT (second_number, second_number_decoded);
        }

}

static void
foreach_self_check (void)
{
        /* FIXME: Implement */
        /* Test that going over one segment gives you all of the words that should be in the segment */
        /* Test that going over all segments gives you all the words */
        /* Test that going over all segments gives you no duplicates */
        /* Test that foreach works with several different "total segment" values */
        /* Test that for a large hash the segment sizes are about equal */
}

static void
start_and_end_locations_self_check (void)
{
        /* FIXME: Implement */
        /* Test that inserting a location's start and end location into the database and then retriveing it works */
        /* Test inserting and retrieving many items works */
        
        /* Test inserting, updating and then retrieving and item works */
        /* Test inserting, updating and then retrieving many items works */
}

void
medusa_lexicon_self_check (void)
{

        g_print ("Self checking lexicon:\n");
        integer_encoding_functions_self_check ();
        foreach_self_check ();
        start_and_end_locations_self_check ();
}




