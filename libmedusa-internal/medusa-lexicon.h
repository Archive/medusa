/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  medusa-lexicon.h - Implementation of a lexicon data structure, which stores
 *  words that exist in the text index
 *
 *  Copyright (C) 2001 Rebecca Schulman
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@ai.mit.edu>
 * 
 */

#ifndef MEDUSA_LEXICON_H
#define MEDUSA_LEXICON_H

#include <glib.h>

#include "medusa-extensible-hash.h"

#define MEDUSA_WORD_HAS_NO_PREVIOUS_OCCURRENCE 0xFFFFFFFE

typedef struct MedusaLexicon MedusaLexicon;

MedusaLexicon *            medusa_lexicon_new                      (const char            *index_name);
MedusaLexicon *            medusa_lexicon_open                     (const char            *index_name);
gboolean                   medusa_lexicon_insert                   (MedusaLexicon         *lexicon,
								    const char            *word,
								    guint32                words_last_occurrence_in_temp_file);

gboolean                   medusa_lexicon_foreach_in_segment       (MedusaLexicon                     *lexicon,
                                                                    int                                segment_number,
                                                                    int                                total_segments,
                                                                    MedusaExtensibleHashForeachFunc    function,
                                                                    gpointer                           function_data);
gboolean                   medusa_lexicon_set_index_locations      (MedusaLexicon *lexicon,
                                                                    const char    *word,
                                                                    guint32        start_location,
                                                                    guint32        end_location);

gboolean                   medusa_lexicon_get_last_occurrence      (MedusaLexicon         *lexicon,
                                                                    const char            *word,
                                                                    guint32               *last_occurrence);
void                       medusa_lexicon_decode_last_occurrence   (const char            *last_occurrence,
                                                                    guint32               *cell_number);
gboolean                   medusa_lexicon_lookup_index_locations   (MedusaLexicon         *lexicon,
                                                                    const char            *word,
                                                                    guint32               *start_location,
                                                                    guint32               *end_location);


gboolean                   medusa_lexicon_move_completed_index_into_place    (const char           *index_name);
gboolean                   medusa_lexicon_erase_constructed_index  (const char        *index_name,
                                                                    gboolean           erase_in_progress_index);
gboolean                   medusa_lexicon_free                     (MedusaLexicon         *lexicon);

void                       medusa_lexicon_self_check               (void);

#endif /* MEDUSA_LEXICON_H */

