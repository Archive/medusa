AC_INIT(medusa-2.0.pc.in)
AC_CONFIG_SRCDIR([medusa-2.0.pc.in])
AM_CONFIG_HEADER(config.h)

MEDUSA_MAJOR_VERSION=0
MEDUSA_MINOR_VERSION=6
MEDUSA_MICRO_VERSION=3
MEDUSA_VERSION="$MEDUSA_MAJOR_VERSION.$MEDUSA_MINOR_VERSION.$MEDUSA_MICRO_VERSION"

AM_INIT_AUTOMAKE(medusa, $MEDUSA_VERSION)

AC_SUBST(MEDUSA_MAJOR_VERSION)
AC_SUBST(MEDUSA_MINOR_VERSION)
AC_SUBST(MEDUSA_MICRO_VERSION)
AC_SUBST(MEDUSA_VERSION)

INTLTOOL_REQUIRED=0.30
GLIB_REQUIRED=2.4.0
GTK_REQUIRED=2.4.0
GNOME_REQUIRED=2.6.0
GNOMEUI_REQUIRED=2.6.0
GNOME_VFS_REQUIRED=2.0.0
XML_REQUIRED=2.2.8
GCONF_REQUIRED=2.0.0
LIBGLADE_REQUIRED=2.0.1

AC_SUBST(GLIB_REQUIRED)
AC_SUBST(GTK_REQUIRED)
AC_SUBST(GNOME_REQUIRED)
AC_SUBST(GNOMEUI_REQUIRED)
AC_SUBST(GNOME_VFS_REQUIRED)
AC_SUBST(XML_REQUIRED)
AC_SUBST(GCONF_REQUIRED)
AC_SUBST(LIBGLADE_REQUIRED)

AC_ISC_POSIX
AC_PROG_INTLTOOL(INTLTOOL_REQUIRED)
AM_PROG_LIBTOOL
AC_PROG_CC
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_MAKE_SET
AC_PATH_PROG(GCONFTOOL, gconftool-2)
AM_GCONF_SOURCE_2


dnl -------------
dnl Checks for headers, and typedefs
dnl -------------
AC_CHECK_HEADERS(sys/mnttab.h sys/vfs.h sys/mount.h)


dnl -------------
dnl Checks for functions
dnl -------------
AC_CHECK_FUNCS(getmntinfo getmntent setmntent endmntent statfs statvfs)
AC_PATH_XTRA

dnl =================================================
dnl Hacks from XScreenSaver for the X activity daemon
dnl =================================================

AC_DEFUN(AC_CHECK_X_HEADER, [
  ac_save_CPPFLAGS="$CPPFLAGS"
  if test \! -z "$includedir" ; then 
    CPPFLAGS="$CPPFLAGS -I$includedir"
  fi
  CPPFLAGS="$CPPFLAGS $X_CFLAGS"
  AC_CHECK_HEADER([$1], [$2])
  CPPFLAGS="$ac_save_CPPFLAGS"])


AC_DEFUN(AC_CHECK_X_LIB, [
  ac_save_CPPFLAGS="$CPPFLAGS"
  ac_save_LDFLAGS="$LDFLAGS"

  if test \! -z "$includedir" ; then 
    CPPFLAGS="$CPPFLAGS -I$includedir"
  fi
  # note: $X_CFLAGS includes $x_includes
  CPPFLAGS="$CPPFLAGS $X_CFLAGS"

  if test \! -z "$libdir" ; then
    LDFLAGS="$LDFLAGS -L$libdir"
  fi
  # note: $X_LIBS includes $x_libraries
  LDFLAGS="$LDFLAGS $X_LIBS $X_EXTRA_LIBS"

  AC_CHECK_LIB([$1], [$2], [$3], [$4], [$5])
  CPPFLAGS="$ac_save_CPPFLAGS"
  LDFLAGS="$ac_save_LDFLAGS"
  ])


#############################################################################
# Usage: HANDLE_X_PATH_ARG([variable_name],
#                          [--command-line-option],
#                          [descriptive string])
#
# All of the --with options take three forms:
#
#   --with-foo (or --with-foo=yes)
#   --without-foo (or --with-foo=no)
#   --with-foo=/DIR
#
# This function, HANDLE_X_PATH_ARG, deals with the /DIR case.  When it sees
# a directory (string beginning with a slash) it checks to see whether
# /DIR/include and /DIR/lib exist, and adds them to $X_CFLAGS and $X_LIBS
# as appropriate.
#############################################################################

AC_DEFUN(HANDLE_X_PATH_ARG, [
   case "$[$1]" in
    yes) ;;
    no)  ;;

    /*)
     AC_MSG_CHECKING([for [$3] headers])
     d=$[$1]/include
     if test -d $d; then
       X_CFLAGS="-I$d $X_CFLAGS"
       AC_MSG_RESULT($d)
     else
       AC_MSG_RESULT(not found ($d: no such directory))
     fi

     AC_MSG_CHECKING([for [$3] libs])
     d=$[$1]/lib
     if test -d $d; then
       X_LIBS="-L$d $X_LIBS"
       AC_MSG_RESULT($d)
     else
       AC_MSG_RESULT(not found ($d: no such directory))
     fi

     # replace the directory string with "yes".
     [$1]_req="yes"
     [$1]=$[$1]_req
     ;;

    *)
     echo ""
     echo "error: argument to [$2] must be \"yes\", \"no\", or a directory."
     echo "       If it is a directory, then \`DIR/include' will be added to"
     echo "       the -I list, and \`DIR/lib' will be added to the -L list."
     exit 1
     ;;
   esac
  ])


###############################################################################
#
#       Check for the MIT-SCREEN-SAVER server extension.
#
###############################################################################

have_mit=no
with_mit_req=unspecified
AC_ARG_WITH(mit-ext,
[  --with-mit-ext          Include support for the MIT-SCREEN-SAVER extension.],
  [with_mit="$withval"; with_mit_req="$withval"],[with_mit=yes])

HANDLE_X_PATH_ARG(with_mit, --with-mit-ext, MIT-SCREEN-SAVER)

if test "$with_mit" = yes; then
  AC_CHECK_X_HEADER(X11/extensions/scrnsaver.h, [have_mit=yes])
elif test "$with_mit" != no; then
  echo "error: must be yes or no: --with-mit-ext=$with_mit"
  exit 1
fi


###############################################################################
#
#       Check for the XIDLE server extension.
#
###############################################################################

have_xidle=no
with_xidle_req=unspecified
AC_ARG_WITH(xidle-ext,
[  --with-xidle-ext        Include support for the XIDLE extension.],
  [with_xidle="$withval"; with_xidle_req="$withval"],[with_xidle=yes])

HANDLE_X_PATH_ARG(with_xidle, --with-xidle-ext, XIDLE)

if test "$with_xidle" = yes; then
  AC_CHECK_X_HEADER(X11/extensions/xidle.h,
                    [have_xidle=yes
                     AC_DEFINE(HAVE_XIDLE_EXTENSION, 1, [Include support for the XIDLE extension])])
elif test "$with_xidle" != no; then
  echo "error: must be yes or no: --with-xidle-ext=$with_xidle"
  exit 1
fi


###############################################################################
#
#       Check for the SGI-VIDEO-CONTROL server extension.
#
###############################################################################

have_sgivc=no
with_sgivc_req=unspecified
AC_ARG_WITH(sgivc-ext,
[  --with-sgivc-ext        Include support for the SGI-VIDEO-CONTROL extension.],
  [with_sgivc="$withval"; with_sgivc_req="$withval"],[with_sgivc=yes])

HANDLE_X_PATH_ARG(with_sgivc, --with-sgivc-ext, SGI-VIDEO-CONTROL)

if test "$with_sgivc" = yes; then

  # first check for XSGIvc.h
  AC_CHECK_X_HEADER(X11/extensions/XSGIvc.h, [have_sgivc=yes])

  # if that succeeded, then check for the -lXsgivc
  if test "$have_sgivc" = yes; then
    have_sgivc=no
    AC_CHECK_X_LIB(Xsgivc, XSGIvcQueryGammaMap,
                  [have_sgivc=yes; SAVER_LIBS="$SAVER_LIBS -lXsgivc"], [true],
                  -lXext -lX11)
  fi

  # if that succeeded, then we've really got it.
  if test "$have_sgivc" = yes; then
    AC_DEFINE(HAVE_SGI_VC_EXTENSION, 1, [Use the SGI VC extension])
  fi

elif test "$with_sgivc" != no; then
  echo "error: must be yes or no: --with-sgivc-ext=$with_sgivc"
  exit 1
fi

AC_SUBST(X_CFLAGS)
AC_SUBST(X_LIBS)


###############################################################################
#
#       Check for /proc/interrupts.
#
###############################################################################

have_proc_interrupts=no
with_proc_interrupts_req=yes
AC_ARG_WITH(proc-interrupts,
[  --with-proc-interrupts  Include support for consulting the /proc/interrupts
                          file to notice keyboard activity.],
  [with_proc_interrupts="$withval"; with_proc_interrupts_req="$withval"],
  [with_proc_interrupts=yes])

if test "$with_proc_interrupts" = yes; then

   AC_CACHE_CHECK([whether /proc/interrupts contains keyboard data],
    ac_cv_have_proc_interrupts,
    [ac_cv_have_proc_interrupts=no
     if grep keyboard /proc/interrupts >/dev/null 2>&1 ; then
       ac_cv_have_proc_interrupts=yes
     fi
    ])
   have_proc_interrupts=$ac_cv_have_proc_interrupts

  if test "$have_proc_interrupts" = yes; then
    AC_DEFINE(HAVE_PROC_INTERRUPTS, 1, [Include support for consulting /proc/interrupts])
  fi

elif test "$with_proc_interrupts" != no; then
  echo "error: must be yes or no: --with-proc-interrupts=$with_proc_interrupts"
  exit 1
fi


dnl -------------
dnl Check for online dictionary for testing purposes
dnl -------------

AC_CHECK_FILES(/usr/dict/words, dictionary="/usr/dict/words")
AC_CHECK_FILES(/usr/share/dict/words, dictionary="/usr/share/dict/words")
if test "x$dictionary"
then
	STANDARD_DICTIONARY_PATH=$dictionary
	AC_DEFINE_UNQUOTED(STANDARD_DICTIONARY_PATH, "${STANDARD_DICTIONARY_PATH}", [Path to the dictionary use by medusa])
fi


dnl -------------
dnl compiler characteristics.
dnl -------------

AC_C_CONST
AM_MAINTAINER_MODE

if test x$MAINT = x; then
    GNOME_COMPILE_WARNINGS(error)
    DEPRECATED_FLAGS="-fno-strict-aliasing -DG_DISABLE_DEPRECATED -DGDK_DISABLE_DEPRECATED -DGTK_DISABLE_DEPRECATED -DGNOME_DISABLE_DEPRECATED -DBONOBO_DISABLE_DEPRECATED -DBONOBO_UI_DISABLE_DEPRECATED"
else
    GNOME_COMPILE_WARNINGS(maximum)
    DEPRECATED_FLAGS="-fno-strict-aliasing "
fi
AC_SUBST(DEPRECATED_FLAGS)


dnl -------------
dnl required libs
dnl -------------

dnl AM_DISABLE_STATIC

PKG_CHECK_MODULES(GLIB, glib-2.0 >= $GLIB_REQUIRED)
PKG_CHECK_MODULES(GTK, gtk+-2.0 >= $GTK_REQUIRED)
PKG_CHECK_MODULES(GNOME, libgnome-2.0 >= $GNOME_REQUIRED)
PKG_CHECK_MODULES(GNOMEUI, libgnomeui-2.0 >= $GNOMEUI_REQUIRED)
PKG_CHECK_MODULES(VFS, gnome-vfs-module-2.0 >= $GNOME_VFS_REQUIRED)
PKG_CHECK_MODULES(XML, libxml-2.0 >= $XML_REQUIRED)
PKG_CHECK_MODULES(BONOBO, bonobo-activation-2.0 libbonobo-2.0)
PKG_CHECK_MODULES(GCONF, gconf-2.0)
PKG_CHECK_MODULES(LIBGLADE, libglade-2.0 >= $LIBGLADE_REQUIRED)

AC_SUBST(GLIB_CFLAGS)
AC_SUBST(GLIB_LIBS)
AC_SUBST(GTK_CFLAGS)
AC_SUBST(GTK_LIBS)
AC_SUBST(GNOME_CFLAGS)
AC_SUBST(GNOME_LIBS)
AC_SUBST(GNOMEUI_CFLAGS)
AC_SUBST(GNOMEUI_LIBS)
AC_SUBST(VFS_CFLAGS)
AC_SUBST(VFS_LIBS)
AC_SUBST(XML_CFLAGS)
AC_SUBST(XML_LIBS)
AC_SUBST(BONOBO_CFLAGS)
AC_SUBST(BONOBO_LIBS)
AC_SUBST(GCONF_CFLAGS)
AC_SUBST(GCONF_LIBS)
AC_SUBST(LIBGLADE_CFLAGS)
AC_SUBST(LIBGLADE_LIBS)

APP_LIBS=""
AC_SUBST(APP_LIBS)


dnl -------------
dnl Add the languages which your application supports here.
dnl -------------

GETTEXT_PACKAGE=medusa-2.0
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE",[Gettext package])

ALL_LINGUAS="ar ca cs de dz el en_CA en_GB es fi fr hr it lv nb ne nl oc pa pt pt_BR rw sr sr@Latn sv tr uk vi zh_CN"
AM_GLIB_GNU_GETTEXT


dnl -------------
dnl Additional data
dnl -------------

if test "x${prefix}" = "xNONE"; then
  medusa_prefix=${ac_default_prefix}
else
  medusa_prefix=${prefix}
fi

MEDUSA_SOURCEDIR=`(cd $srcdir; pwd)`
AC_DEFINE_UNQUOTED(MEDUSA_SOURCEDIR, "${MEDUSA_SOURCEDIR}", [Path to the medusa source directory])

MEDUSA_DATA_DIR="${medusa_prefix}/share/${GETTEXT_PACKAGE}"
AC_DEFINE_UNQUOTED(MEDUSA_DATA_DIR, "${medusa_prefix}/share/${GETTEXT_PACKAGE}", [Path to medusa data files])
AC_SUBST(MEDUSA_DATA_DIR)

MEDUSA_GLADE_DIR="${MEDUSA_DATA_DIR}/interfaces"
AC_DEFINE_UNQUOTED(MEDUSA_GLADE_DIR, "${MEDUSA_DATA_DIR}/interfaces", [Path to medusa glade files])
AC_SUBST(MEDUSA_GLADE_DIR)

MEDUSA_ICON_DIR='${medusa_prefix}/share/pixmaps'
AC_DEFINE_UNQUOTED(MEDUSA_ICON_DIR, "${medusa_prefix}/share/pixmaps", [Path to medusa icons files])
AC_SUBST(MEDUSA_ICON_DIR)


AC_OUTPUT([ 
medusa.spec
medusa-2.0.pc
Makefile
libmedusa/Makefile
libmedusa-internal/Makefile
indexer/Makefile
msearch/Makefile
tests/Makefile
doc/Makefile
index-configuration/Makefile
gnome-vfs-module/Makefile
medusa-idled/Makefile
msearch-gui/Makefile
po/Makefile.in
])


echo "<= medusa configuration summary :"
dnl <= CFLAGS and LDFLAGS =>
echo "
CFLAGS                  : $CFLAGS
LDFLAGS                 : $LDFLAGS"
echo
