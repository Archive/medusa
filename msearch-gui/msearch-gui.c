/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*-
 *
 *  Medusa
 *
 *  msearch-gui.c -- a GUI medusa search app.  This is the main routine and
 *	  global variables.
 *
 *  Copyright (C) 2003 Curtis C. Hovey
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors: Curtis C. Hovey <sinzui@cox.net>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <libgnome/libgnome.h>
#include <libgnomevfs/gnome-vfs-init.h>
#include <glade/glade.h>
#include <libmedusa/medusa-index-service.h>

#include "interface.h"
#include "msearch-gui.h"
#include "medusa-treemvc.h"

#define TESTGLADEFILE "msearch-gui.glade"
#define REALGLADEFILE MEDUSA_GLADE_DIR "/msearch-gui.glade"

GladeXML *xml;
MedusaTreemvc *treemvc;

/*
 * main:
 * 
 * @argc: an int representing the number of the commandline arguments.
 * @argv: a string array of the commandline arguments.
 * 
 * Starts the app.
 * 
 * returns: an int--0 on success, 1 on failure.
 */
int
main (int argc, char *argv[])
{
	gchar *glade_file_name = REALGLADEFILE;
	GtkWidget *msearch_gui_app;
	GtkWidget *tree;

#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif

	gnome_program_init ("msearch-gui", VERSION, LIBGNOMEUI_MODULE,
		      argc, argv,
		      GNOME_PARAM_APP_DATADIR, PACKAGE_DATA_DIR,
		      NULL);
	
	gnome_vfs_init ();
	glade_gnome_init ();
	
	if (g_file_test (TESTGLADEFILE, G_FILE_TEST_EXISTS)) {
		glade_file_name = TESTGLADEFILE;
	}
	xml = glade_xml_new (glade_file_name, NULL, NULL);
	if (NULL == xml) {
		g_error(_("Cannot load the interface glade file."));
		return 1;
	}
	
	glade_xml_signal_autoconnect (xml);
			  
	msearch_gui_app = msearch_interface_init (xml);
	if (NULL == msearch_gui_app) {
		return 1;
	}
	
	tree = glade_xml_get_widget (xml, "results_treeview");
	if (NULL == tree) {
		g_error(_("Cannot get the results GtkTreeview."));
		return 1;
	}
	treemvc = medusa_treemvc_new_with_tree (tree);
	
	gtk_widget_show (msearch_gui_app);
	
	gtk_main ();

	return 0;
}


/*
 * exec_search:
 * 
 * @search_uri: a const gchar representing the search url.
 * 
 * Executes a Medusa search and displays the results.
 * 
 * returns: void.
 */
void
exec_search (const gchar *search_uri)
{
	/* I think we want a vfs uri */
	gboolean index_exists;
	MedusaMasterDB *master_db;
	MedusaParsedSearchURI *parsed_search_uri;
	GList *results;
	const char *error_message;
	GTimeVal start;
	GTimeVal finish;
	guint num_result;
	char *index_name;
	GtkWidget *msearch_appbar;
	
	g_return_if_fail (NULL != search_uri);
	
	msearch_appbar = glade_xml_get_widget (GLADE_XML (xml), "msearch_appbar");
	
	g_get_current_time (&start);
	
	index_exists = medusa_index_service_index_files_exist ();
	
	/* if the index is out of date, or must be built, we must gracefully
	   inform the user about how to correct this. */
	if (!index_exists) {
		gnome_appbar_set_status (GNOME_APPBAR (msearch_appbar), 
					 _("You run medusa-indexd to make an index first"));
		
		return;
	}
	
	index_name = NULL;
	master_db = medusa_master_db_open (ROOT_DIRECTORY, index_name);
	
	parsed_search_uri = medusa_search_uri_parse (search_uri, master_db);
	if (medusa_parsed_search_uri_error (parsed_search_uri, &error_message)) {
		g_print ("%s\n", error_message);
		return;
	}
	
	results = medusa_master_db_run_search (master_db, parsed_search_uri);
	
	g_get_current_time (&finish);
	num_result = g_list_length (results);
	
	medusa_parsed_search_uri_free (parsed_search_uri);
	
	medusa_treemvc_display_results (treemvc, results);
		
	
	gnome_appbar_set_status (GNOME_APPBAR (msearch_appbar), 
				 g_strdup_printf ("%i matching files found in %ld seconds and %ld milliseconds", 
				 num_result, 
				 finish.tv_sec - start.tv_sec, 
				 labs (finish.tv_usec - start.tv_usec) / 1000));

	medusa_g_list_free_deep (results);
	
	return;
}
