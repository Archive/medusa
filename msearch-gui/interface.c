/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*-
 *
 *  Medusa
 *
 *  interface.c -- Signal handler functions and some helpers for msearch-gui.
 *
 *  Copyright (C) 2003 Curtis C. Hovey
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors: Curtis C. Hovey <sinzui@cox.net>
 */
 

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <glib.h>
#include <gtk/gtk.h>
#include <glade/glade.h>

#include "interface.h"
#include "msearch-gui.h"


/* helper declarations */
static void	create_emblem_menu			(void);

void		show_index_dialog			(void);

void		clear_clause				(gpointer	in_clause,
						 	 gpointer       nothing);
						 
void		clear_clause_part			(gpointer	in_clause_part,
							 gpointer	nothing);
							 
void		find_clause				(gpointer	clause,
							 gpointer       query_string);
						 
void		find_clause_parts			(gpointer	in_clause_parts,
							 gpointer	clause_tripet);

/* callback declarations */
gboolean	on_complex_search_dialog_destroy	(GtkObject	*object,
							 gpointer	user_data);

void		on_clear_button_clicked			(GtkButton	*button,
							 gpointer	user_data);
							 
gboolean	on_sidebar_entry_key_press 		(GtkWidget 	*entry,
							 GdkEventKey 	*event, 
							 gpointer 	user_data);

void		on_find_buttton_clicked			(GtkWidget	*button,
							 gpointer	user_data);

gboolean	on_gsearch_app_destroy_event		(GtkEntry	*entry,
							 GdkEvent	*event,
							 gpointer	user_data);

void		on_menuitem_quit_activate		(GtkMenuItem	*menuitem,
							 gpointer	user_data);

void		on_menuitem_copy_activate		(GtkMenuItem	*menuitem,
							 gpointer	user_data);

void		on_complex_button_clicked		(GtkButton	*button,
							 gpointer	user_data);

void		on_simple_button_clicked		(GtkButton	*button,
							 gpointer	user_data);

void		on_msearch_gui_button_clicked		(GtkButton	*button,
							 gpointer	user_data);

gboolean	on_msearch_gui_app_destroy		(GtkObject	*object,
							 gpointer	user_data);

gboolean	on_simple_search_dialog_destroy		(GtkObject	*object,
							 gpointer	user_data);

void		on_simple_search_file_name_buttton_clicked	(GtkButton	*button,
								 gpointer	user_data);

void		on_close_buttton_clicked		(GtkButton	*button,
							 gpointer	user_data);

void		on_sidebar_clear_button_clicked		(GtkButton      *button,
							 gpointer	user_data);
				    
void		on_sidebar_find_button_clicked		(GtkWidget      *button,
							 gpointer	user_data);

void		on_msearch_quit_activate		(GtkImageMenuItem *imagemenuitem,
							 gpointer	user_data);
 
void		on_msearch_new_activate			(GtkImageMenuItem *imagemenuitem,
							 gpointer	user_data);
 
void		on_msearch_preferences_activate		(GtkImageMenuItem *imagemenuitem,
							 gpointer	user_data);
 
void		on_msearch_copy_activate		(GtkImageMenuItem *imagemenuitem,
							 gpointer	user_data);
 
void		on_msearch_save_activate		(GtkImageMenuItem *imagemenuitem,
							 gpointer	user_data);
 
void		on_msearch_properties_activate		(GtkImageMenuItem *imagemenuitem,
							 gpointer	user_data);
 
void		on_msearch_cut_activate			(GtkImageMenuItem *imagemenuitem,
							 gpointer	user_data);
 
void		on_msearch_open_activate		(GtkImageMenuItem *imagemenuitem,
							 gpointer	user_data);
 
void		on_msearch_save_as_activate		(GtkImageMenuItem *imagemenuitem,
							 gpointer	user_data);
 
void		on_msearch_paste_activate		(GtkImageMenuItem *imagemenuitem,
							 gpointer	user_data);
 
void		on_msearch_clear_activate		(GtkImageMenuItem *imagemenuitem,
							 gpointer	user_data);
 
void		on_msearch_about_activate		(GtkImageMenuItem *imagemenuitem,
							 gpointer	user_data);

void		on_msearch_index_status_activate	(GtkImageMenuItem *imagemenuitem,
							 gpointer	user_data);
							 
void		on_index_dialog_run_indexer_clicked	(GtkButton      *button,
							 gpointer	user_data);
							 
void		on_index_dialog_close_clicked		(GtkButton      *button,
							 gpointer	user_data);


typedef struct _clause_triplet ClauseTriplet;
	
struct _clause_triplet {
	gchar *subject;
	gchar *predicate;
	gchar *object;
};


enum string_list_columns {
	STRING_COLUMN,
	N_STRING_COLUMNS
};

enum emblem_list_columns {
	IMAGE_COLUMN,
	KEYWORD_COLUMN,
	N_EMBLEM_COLUMNS
};


#define ICON_SIZE 24.0


GladeXML *xml;

gboolean row_selected_by_button_press_event;


/*
 * msearch_interface_init:
 * 
 * @in_xml: the msearch-gui GladeXML.
 * 
 * Executes gets the msearch_gui_app, set's it up, and creates the alternate
 * label text for the toggle buttons.
 * 
 * returns: msearch_gui_app (top-level window) GtkWidget.
 */
GtkWidget *
msearch_interface_init (GladeXML *in_xml)
{
	GtkWidget *msearch_gui_app;
	gboolean state_on;
	GtkWidget *sidebar_file_name_vbox;
	GtkWidget *content_expander;
	GError *err;
	GtkIconTheme *icon_theme;
	GtkIconInfo *info;
	const gchar *icon_path;

	err = NULL;
	
	g_return_val_if_fail (NULL != in_xml, NULL);
	xml = in_xml;

	msearch_gui_app = glade_xml_get_widget (xml, "msearch_gui_app");
	if (NULL == msearch_gui_app) {
		g_error(_("Cannot get the application window."));
		return NULL;
	}
	
	// force broken signal to work
	g_signal_connect (G_OBJECT (msearch_gui_app), 
			  "delete_event",
			  G_CALLBACK (on_msearch_gui_app_destroy),
			  msearch_gui_app);

	icon_theme = gtk_icon_theme_get_default ();
	info = gtk_icon_theme_lookup_icon (icon_theme, MSEARCH_GUI_ICON, ICON_SIZE, 0);
	icon_path = gtk_icon_info_get_filename (info);
	gtk_window_set_icon_from_file (GTK_WINDOW (msearch_gui_app), icon_path, &err);
	if (NULL != err) {
		g_warning (_("Cannot set the application's default icon:"));
		g_warning (err->message);
		g_error_free (err);
	}
	
	create_emblem_menu ();
	
	on_sidebar_clear_button_clicked (NULL, NULL);
	
	content_expander = glade_xml_get_widget (xml, "sidebar_content_clause");
	gtk_expander_set_expanded (GTK_EXPANDER (content_expander), TRUE);

	return msearch_gui_app;
}


/*
 * create_emblem_menu:
 * 
 * Creates the emblem value optionmenu with pixbufs and text.
 * 
 * returns: void.
 */
static void				
create_emblem_menu (void)
{
	GtkIconTheme *icon_theme;
	GList *emblem_list;
	GtkListStore *store;
	GtkTreeIter iter;
	GtkCellRenderer *renderer;
	GtkWidget *combo_box;

	store = gtk_list_store_new (N_EMBLEM_COLUMNS, GDK_TYPE_PIXBUF, G_TYPE_STRING);

	icon_theme = gtk_icon_theme_get_default ();
	emblem_list = gtk_icon_theme_list_icons (icon_theme, "Emblems");
	
	while (NULL != emblem_list) {
		char *emblem;
		char *keyword;
		GError *err;
		GdkPixbuf *pixbuf;

		err = NULL;

		emblem = (char *) emblem_list->data;
		if (g_str_has_prefix (emblem, "emblem-")) {
			keyword = g_strdup (&emblem[7]);
		} 
		else {
			keyword = g_strdup (emblem);
		}

		if (strcmp (keyword, "erase") == 0) {
			g_free (emblem);
			g_free (keyword);
			
			emblem_list = emblem_list->next;
			continue;
		}
	
		pixbuf = gtk_icon_theme_load_icon (icon_theme, emblem, ICON_SIZE, 0, &err);
		if (!pixbuf) {
			g_warning (_("Cannot load emblem:"));
			g_warning (err->message);
			g_error_free (err);
		}
	
		if (pixbuf) {	
			gtk_list_store_append (store, &iter);
			gtk_list_store_set (store, &iter, 
					    IMAGE_COLUMN, pixbuf, 
					    KEYWORD_COLUMN, g_strdup (keyword), -1);
		}
		else {
			g_warning (_("NULL emblem pixbuf:"));
		}

		g_object_unref (pixbuf);
		g_free (keyword);
		g_free (emblem);
		
		emblem_list = emblem_list->next;
	}

	g_list_free (emblem_list);

	combo_box = glade_xml_get_widget (xml, "sidebar_keywords_object");
	gtk_combo_box_set_model (GTK_COMBO_BOX (combo_box), GTK_TREE_MODEL (store));
	
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo_box), renderer, FALSE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo_box), renderer,
					"pixbuf", IMAGE_COLUMN, NULL);

	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo_box), renderer, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo_box), renderer,
					"text", KEYWORD_COLUMN, NULL);
}


/*
 * show_index_dialog:
 * 
 * Show the index status dialog.
 * 
 * returns: void.
 */
void
show_index_dialog () 
{
	
}


/*
 * on_sidebar_clear_button_clicked:
 * 
 * @button: The GtkButton that was clicked.
 * @user_data: a gpointer that is not used.
 * 
 * The callback resets or clears the search clauses.
 * 
 * returns: void.
 */
void
on_sidebar_clear_button_clicked	(GtkButton *button, gpointer user_data)
{
	GtkWidget *sidebar_clauses_vbox;
	GList *clauses;
	
	sidebar_clauses_vbox = glade_xml_get_widget (xml, "sidebar_clauses_vbox");
	clauses = gtk_container_get_children (GTK_CONTAINER (sidebar_clauses_vbox));
	g_list_foreach (clauses, clear_clause, NULL);
}


/*
 * clear_clause:
 * 
 * @data: gpointer representing a GtkWidget that is a Medusa clause.
 * @user_data: a gpointer that is not used.
 * 
 * This is a g_list_foreach function that resets or clears all the children of 
 * GtkWidget passed to it.
 * 
 * returns: void.
 */
void
clear_clause (gpointer in_clause, gpointer nothing)
{
	GtkWidget *clause_expander;
	GList *expander_children;
	GtkWidget *clause_vbox;
	GList *clause_parts;
	
	g_return_if_fail (in_clause);
	
	clause_expander = (GtkWidget *) in_clause;
	expander_children = gtk_container_get_children (GTK_CONTAINER (clause_expander));
	clause_vbox =  (GtkWidget *) expander_children->data;
	clause_parts = gtk_container_get_children (GTK_CONTAINER (clause_vbox));
	
	g_list_foreach (clause_parts, clear_clause_part, NULL);
	
	gtk_expander_set_expanded (GTK_EXPANDER (clause_expander), FALSE);
}


/*
 * clear_clause_part:
 * 
 * @data: gpointer representing a GtkWidget that is a part of a Medusa clause.
 * @user_data: a gpointer that is not used.
 * 
 * This is a g_list_foreach function that changes the state of the GtkWidget
 * passed to it.  It resets or clears all the widgets, and updated the label of toggle
 * buttons to show 'more options'.
 * 
 * returns: void.
 */
void
clear_clause_part (gpointer in_clause_part, gpointer nothing)
{
	GtkWidget *clause_part;
	
	g_return_if_fail (in_clause_part);
	
	clause_part = (GtkWidget *) in_clause_part;
	
	if (GTK_IS_COMBO_BOX (clause_part)) {
		gtk_combo_box_set_active (GTK_COMBO_BOX (clause_part), 0);
	}
	else if (GTK_IS_ENTRY (clause_part)) {
		gtk_entry_set_text (GTK_ENTRY (clause_part), "");
	}
	else if (GNOME_IS_DATE_EDIT (clause_part)) {
		GTimeVal time_val;
		time_t time;
		
		g_get_current_time (&time_val);
		time = (time_t) time_val.tv_sec;
		
		gnome_date_edit_set_time (GNOME_DATE_EDIT (clause_part), time);
	}
}


/*
 * on_sidebar_entry_key_press:
 * 
 * @entry: the GtkEntry that was modified.
 * @GdkEventKey: the event state.
 * @user_data: a gpointer that is not used.
 * 
 * The callback calls the on_sidebar_find_button_clicked handler()
 * when %GDK_Return is entered into an GtkEntry to execute the search.
 * 
 * returns: signal state, gboolean TRUE or FALSE.
 */
gboolean
on_sidebar_entry_key_press (GtkWidget *entry, GdkEventKey *event, gpointer user_data)
{
	g_return_val_if_fail (entry, FALSE);
	g_return_val_if_fail (GTK_IS_ENTRY (entry), FALSE);
	
	if (event->keyval == GDK_Return) {
		on_sidebar_find_button_clicked (entry, user_data);
		return TRUE;
	}
	
	return FALSE;
}


/*
 * on_sidebar_find_button_clicked:
 * 
 * @button: The GtkButton that was clicked.
 * @user_data: a gpointer that is not used.
 * 
 * The callback constructs a search url and passes it to the
 * exec_search() function.
 * 
 * returns: void.
 */
void
on_sidebar_find_button_clicked (GtkWidget *button, gpointer user_data)
{
	GtkWidget *sidebar_clauses_vbox;
	GList *clauses;
	GString *query_string;

	query_string = g_string_new ("");
	
	sidebar_clauses_vbox = glade_xml_get_widget (xml, "sidebar_clauses_vbox");
	clauses = gtk_container_get_children (GTK_CONTAINER (sidebar_clauses_vbox));
	g_list_foreach (clauses, find_clause, (gpointer) query_string);

	g_string_erase (query_string, 0, 3);
	g_string_prepend (query_string, "search:[file:///]");
	
	/* g_message (g_strdup (query_string->str)); */
	exec_search (query_string->str);
	
	g_string_free (query_string, TRUE);
}


/*
 * find_clause:
 * 
 * @clause: gpointer representing a GtkWidget that is a Medusa clause.
 * @query_string: a gpointer to the query URL.
 * 
 * This is a g_list_foreach function that locates the active clauses and
 * pass them to find_clause_parts to construct the query_string.
 * 
 * returns: void.
 */
void
find_clause (gpointer clause, gpointer query_string)
{
	GtkWidget *clause_expander;
	GList *clause_parts;
	gboolean is_expanded;
	
	g_return_if_fail (clause);
	
	clause_expander = (GtkWidget *) clause;

	is_expanded = gtk_expander_get_expanded (GTK_EXPANDER (clause_expander));

	if (is_expanded) {
		const gchar *clause_name;
		gchar *sub_clause_name;
		gsize subject_len;
		gchar *subject;
		GtkWidget *label;
		GList *expander_children;
		GtkWidget *clause_vbox;
		ClauseTriplet triplet;
		
		triplet.subject = NULL;
		triplet.predicate = NULL;
		triplet.object = NULL;
		
		/* The label name of the expander is always the clause subject */
		label = gtk_expander_get_label_widget (GTK_EXPANDER (clause_expander));
		clause_name = gtk_widget_get_name (GTK_WIDGET (label));
		sub_clause_name = strstr (clause_name, "_") + 1;
		subject_len = g_strrstr (sub_clause_name, "_") - sub_clause_name;
		triplet.subject = g_strndup (sub_clause_name, subject_len);		
		
		/* the predicate and object are always the children of the expander's vbox */
		expander_children = gtk_container_get_children (GTK_CONTAINER (clause_expander));
		clause_vbox = (GtkWidget *) expander_children->data;
		clause_parts = gtk_container_get_children (GTK_CONTAINER (clause_vbox));
		
		find_clause_parts (clause_parts, &triplet);

		if (triplet.subject && triplet.predicate && triplet.object) {
			g_string_append ((GString *) query_string, 
				g_strdup_printf (" & %s %s %s", 
						 triplet.subject, 
						 triplet.predicate, 
						 triplet.object));
		}
		
		g_free (triplet.subject);
		g_free (triplet.predicate);
		g_free (triplet.object);
	}
}


/*
 * find_clause_parts:
 * 
 * @clause_parts: gpointer representing a GList of GtkWidgets comprising a Medusa clause.
 * @query_string: a gpointer to the query URL.
 * 
 * This function constucts a subject, predicate, and optional object clause and 
 * appends it to the query_string.
 * 
 * returns: void.
 */
void
find_clause_parts (gpointer in_clause_parts, gpointer clause_triplet)
{
	GList *clause_parts;
	ClauseTriplet *triplet;
	
	g_return_if_fail (in_clause_parts);
	
	clause_parts = (GList *) in_clause_parts;
	triplet = (ClauseTriplet *) clause_triplet;
	
	while (NULL != clause_parts) {
		GtkWidget *clause_part;
		
		clause_part = (GtkWidget *) clause_parts->data;
		
		if (NULL == clause_part) {
			break;
		}
		
		if (GTK_IS_COMBO_BOX (clause_part)) {
			const gchar *combo_box_name;
			GtkTreeModel *store;
			GtkTreeIter iter;
			gchar *selected_text;
			gchar *clause_text;
			
			combo_box_name = gtk_widget_get_name (GTK_WIDGET (clause_part));
			store = gtk_combo_box_get_model (GTK_COMBO_BOX (clause_part));
			if (! gtk_combo_box_get_active_iter (GTK_COMBO_BOX (clause_part), &iter)) {
				break;
			}
			
			if (g_str_has_suffix (combo_box_name, "_keywords_object")) {
				gtk_tree_model_get (store, &iter, 
						    KEYWORD_COLUMN, &selected_text, -1);
			}
			else {
				gtk_tree_model_get (store, &iter, 
						    STRING_COLUMN, &selected_text, -1);
			}
			
			g_strdelimit (selected_text, " ", '_');

			/* A combo_box may be the predicate or the object in the glade
			   data so the widgets name must be checked */
			
			if (g_str_has_suffix (combo_box_name, "_predicate")) {
				triplet->predicate = strdup (selected_text);
			}
			else {
				triplet->object = strdup (selected_text);
			}
			
			g_free (selected_text);
		}
		else if (GTK_IS_ENTRY (clause_part)) {
			gchar *string;
			
			/* An entry widget is always the clause object in the glade data */
			string = g_strdup (gtk_entry_get_text (GTK_ENTRY (clause_part)));
			
			if (string && strcmp ("", string)) {
				triplet->object = strdup (g_strstrip (string));
			}
			
			g_free (string);
		}
		else if (GNOME_IS_DATE_EDIT (clause_part)) {
			GDate *date;
			time_t time;
			GDateMonth month;
			GDateDay day;
			GDateYear year;
			
			/* A date widget is always the clause object in the glade data */
			date = g_date_new ();
			time = gnome_date_edit_get_time (GNOME_DATE_EDIT (clause_part));
			g_date_set_time (date, (GTime) time);
			
			month = g_date_get_month (date);
			day = g_date_get_day (date);
			year = g_date_get_year (date);
			
			triplet->object = g_strdup_printf ("%02i/%02i/%4i", month, day, year);
			
			g_date_free (date);
		}
		clause_parts = clause_parts->next;
	}
}


/*
 * on_msearch_gui_app_destroy:
 * 
 * @object: The GtkObject activated from the window.
 * @user_data: a gpointer that is not used.
 * 
 * The callback quits msearch-gui.
 * 
 * returns: void.
 */
gboolean
on_msearch_gui_app_destroy (GtkObject *object, gpointer user_data)
{	
	gtk_main_quit ();
	
	return TRUE;
}


/*
 * on_msearch_quit_activate:
 * 
 * @imagemenuitem: The GtkImageMenuItem activated from the menu.
 * @user_data: a gpointer that is not used.
 * 
 * The callback quits msearch-gui.
 * 
 * returns: void.
 */
void
on_msearch_quit_activate (GtkImageMenuItem *imagemenuitem, gpointer user_data)
{
	gtk_main_quit ();	
}


/*
 * on_msearch_new_activate:
 * 
 * @imagemenuitem: The GtkImageMenuItem activated from the menu.
 * @user_data: a gpointer that is not used.
 * 
 * Does nothing.  Will clear the clauses and start a new desktop item.
 * 
 * returns: void.
 */ 
void
on_msearch_new_activate	(GtkImageMenuItem *imagemenuitem, gpointer user_data)
{
	
}


/*
 * on_msearch_preferences_activate:
 * 
 * @imagemenuitem: The GtkImageMenuItem activated from the menu.
 * @user_data: a gpointer that is not used.
 * 
 * Does nothing.  Will open the Medusa and msearch preferences.
 * 
 * returns: void.
 */
void
on_msearch_preferences_activate	(GtkImageMenuItem *imagemenuitem, gpointer user_data)
{
	
}


/*
 * on_msearch_preferences_activate:
 * 
 * @imagemenuitem: The GtkImageMenuItem activated from the menu.
 * @user_data: a gpointer that is not used.
 * 
 * Does nothing.  Will copy the select results or entry text.
 * 
 * returns: void.
 */ 
void
on_msearch_copy_activate (GtkImageMenuItem *imagemenuitem, gpointer user_data)
{
	
}
 

/*
 * on_msearch_save_activate:
 * 
 * @imagemenuitem: The GtkImageMenuItem activated from the menu.
 * @user_data: a gpointer that is not used.
 * 
 * Does nothing.  Will save the search as a desktop item that can be opened
 * from the desktop.
 * 
 * returns: void.
 */
void
on_msearch_save_activate (GtkImageMenuItem *imagemenuitem, gpointer user_data)
{
	
}


/*
 * on_msearch_properties_activate:
 * 
 * @imagemenuitem: The GtkImageMenuItem activated from the menu.
 * @user_data: a gpointer that is not used.
 * 
 * Does nothing.  Will show the properties of the selected file in the results.
 * 
 * returns: void.
 */
void
on_msearch_properties_activate (GtkImageMenuItem *imagemenuitem, gpointer user_data)
{
	
}


/*
 * on_msearch_cut_activate:
 * 
 * @imagemenuitem: The GtkImageMenuItem activated from the menu.
 * @user_data: a gpointer that is not used.
 * 
 * Does nothing.  Will cut text from a entry.
 * 
 * returns: void.
 */
void
on_msearch_cut_activate	(GtkImageMenuItem *imagemenuitem, gpointer user_data)
{
	
}


/*
 * on_msearch_open_activate:
 * 
 * @imagemenuitem: The GtkImageMenuItem activated from the menu.
 * @user_data: a gpointer that is not used.
 * 
 * Does nothing.  Will open the selected file from results.
 * 
 * returns: void.
 */
void
on_msearch_open_activate (GtkImageMenuItem *imagemenuitem, gpointer user_data)
{
	
}


/*
 * on_msearch_save_as_activate:
 * 
 * @imagemenuitem: The GtkImageMenuItem activated from the menu.
 * @user_data: a gpointer that is not used.
 * 
 * Does nothing.  Will save an opened desktop item as a new item.
 * 
 * returns: void.
 */
void
on_msearch_save_as_activate (GtkImageMenuItem *imagemenuitem, gpointer user_data)
{
	
}
 

/*
 * on_msearch_paste_activate:
 * 
 * @imagemenuitem: The GtkImageMenuItem activated from the menu.
 * @user_data: a gpointer that is not used.
 * 
 * Does nothing.  Will paste text into an entry.
 * 
 * returns: void.
 */
void
on_msearch_paste_activate (GtkImageMenuItem *imagemenuitem, gpointer user_data)
{
	
}

/*
 * on_msearch_clear_activate:
 * 
 * @imagemenuitem: The GtkImageMenuItem activated from the menu.
 * @user_data: a gpointer that is not used.
 * 
 * Calls on_sidebar_clear_button_clicked () to clear the clauses.
 * 
 * returns: void.
 */
void
on_msearch_clear_activate (GtkImageMenuItem *imagemenuitem, gpointer user_data)
{
	on_sidebar_clear_button_clicked ((gpointer) imagemenuitem, user_data);
}


/*
 * on_msearch_index_status_activate:
 * 
 * @imagemenuitem: The GtkImageMenuItem activated from the menu.
 * @user_data: a gpointer that is not used.
 * 
 * Calls on_sidebar_clear_button_clicked () to clear the clauses.
 * 
 * returns: void.
 */
void
on_msearch_index_status_activate (GtkImageMenuItem *imagemenuitem, gpointer user_data)
{
	show_index_dialog ();
}


/*
 * on_msearch_about_activate:
 * 
 * @imagemenuitem: The GtkImageMenuItem activated from the menu.
 * @user_data: a gpointer that is not used.
 * 
 * Does nothing.  Will show the about dialog.
 * 
 * returns: void.
 */
void
on_msearch_about_activate (GtkImageMenuItem *imagemenuitem, gpointer user_data)
{
	
}


/*
 * on_index_dialog_run_indexer_clicked:
 * 
 * @button: The GtkImageMenuItem activated from the menu.
 * @user_data: a gpointer that is not used.
 * 
 * Does nothing.  Run the indexer.
 * 
 * returns: void.
 */
void
on_index_dialog_run_indexer_clicked (GtkButton *button, gpointer user_data)
{
	
}


/*
 * on_index_dialog_close_clicked:
 * 
 * @button: The GtkImageMenuItem activated from the menu.
 * @user_data: a gpointer that is not used.
 * 
 * Does nothing.  Closes the index_dialog.
 * 
 * returns: void.
 */
void
on_index_dialog_close_clicked (GtkButton *button, gpointer user_data)
{
	
}
