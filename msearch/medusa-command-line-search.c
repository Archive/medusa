/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/*
 *  Medusa
 *
 *  Copyright (C) 2000 Eazel, Inc., 2002 Rebecca Schulman
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Rebecca Schulman <rebecka@eazel.com>
 *          Maciej Stachowiak <mjs@eazel.com>
 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include <glib.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-program.h>
#include <libgnome/gnome-init.h>
#include <libgnomevfs/gnome-vfs-init.h>

#include <libmedusa-internal/medusa-conf.h>
#include <libmedusa-internal/medusa-debug.h>
#include <libmedusa-internal/medusa-search-uri.h>
#include <libmedusa-internal/medusa-master-db.h>
#include <libmedusa/medusa-string.h>
#include <libmedusa/medusa-utils.h>

#include <config.h>

static void
print_result (gpointer data,
              gpointer user_data)
{
        char *uri;

        g_return_if_fail (data != NULL);
        
        uri = (char *) data;
        printf ("%s\n", uri);
}

static MedusaParsedSearchURI *
construct_and_parse_search_uri (const char *file_name,
                                int        *mtime,
                                const char *path,
                                int        *size,
                                const char *type,
                                const char *mime_type,
                                MedusaMasterDB *master_db)
{
        char **criteria;
        int count;
        MedusaParsedSearchURI *parsed_uri;

        count = 0;
        criteria = g_new0 (char *, 5);

        if (file_name) {
                        criteria[count++] = g_strdup_printf ("%s %s %s",
                                                             "file_name",
                                                             "contains",
                                                             file_name);
        }
        if (mtime) {
                /* FIXME: Only having "=" some number utterly sucks */
                criteria[count++] = g_strdup_printf ("modified is %d",
                                                     *mtime);
                count++;
        }
        if (path) {
                criteria[count++] = g_strdup_printf ("%s %s %s",
                                                     "directory_name",
                                                     "contains",
                                                     path);
        }
        if (size) {
                /* FIXME: Only having "=" some number utterly sucks */
                criteria[count++] = g_strdup_printf ("size is %d",
                                                     *size);
                count++;
        }
        if (type) {
                criteria[count++] = g_strdup_printf ("%s %s %s",
                                                     "file_type",
                                                     "is",
                                                     type);
        }
        if (mime_type) {
                criteria[count++] = g_strdup_printf ("%s %s %s",
                                                     "mime_type",
                                                     "is",
                                                     mime_type);
        }

        criteria[count] = NULL;
        parsed_uri = medusa_search_uri_parse_criteria_and_free (criteria,
                                                                master_db);
        return parsed_uri;
}

int
main (int argc, char *argv[])
{
        /* popt auxiliaries */
        poptContext popt_context;
        const char **unparsed_arguments;
        const char *error_message;
        GnomeProgram *program;
        GValue context_as_value = {0, };
        GTimeVal start;
        GTimeVal finish;

        /* Arguments */
        char *file_name = NULL;
        char *index_name = NULL;
        int *mtime = NULL;
        char *path = NULL;
        char *search_uri = NULL;
        int *size = NULL;
        char *type = NULL;
        char *mime_type = NULL;

        /* Variables needed for searching */
        MedusaMasterDB *master_db;
        MedusaParsedSearchURI *parsed_search_uri;
        GList *results;
        
        struct poptOption command_line_options[] = {
                { "index", 'i', POPT_ARG_STRING, &index_name,
                  0, N_("Specify the index to search"), 
                  N_("INDEX NAME") },
		{ "name", 'n', POPT_ARG_STRING, &file_name,
		  0, N_("File name matches"),
		  N_("FILE NAME")},
		{ "mtime", 'm', POPT_ARG_INT, &mtime,
		  0, N_("Specify minimum file age in days"),
		  N_("DAYS")},
		{ "path", 'p', POPT_ARG_STRING, &path,
		  0, N_("Specify a path or path segment to match"),
		  N_("PATH")},
		{ "size", 's', POPT_ARG_INT, &size,
		  0, N_("Specify a file's size"),
		  N_("SIZE")},
		{ "type", 't', POPT_ARG_STRING, &type,
		  0, N_("Specify a file's type out of application, music, text file, file or directory"),
                  N_("FILE TYPE")},
		{ "mime", 'e', POPT_ARG_STRING, &mime_type,
		  0, N_("Specify a file's MIME type"),
                  N_("MIME TYPE")},
                { "uri", 'u', POPT_ARG_STRING, &search_uri,
                  0, N_("Specify a search by search uri"),
                  N_("SEARCH URI") },
                POPT_AUTOHELP 
                { NULL, '\0', 0, NULL, 0, NULL, NULL }
        };

 	program = gnome_program_init ("msearch", VERSION,
 				      LIBGNOME_MODULE, argc, argv,
 				      GNOME_PARAM_POPT_TABLE, command_line_options,
 				      GNOME_PARAM_HUMAN_READABLE_NAME, _("Medusa Search Program"),
 				      NULL);
  	g_object_get_property (G_OBJECT (program),
 			       GNOME_PARAM_POPT_CONTEXT,
 			       g_value_init (&context_as_value, G_TYPE_POINTER));
        popt_context = g_value_get_pointer (&context_as_value);

        gnome_vfs_init ();
        
        /* FIXME: Shouldn't this be if-def'd? */
        medusa_make_warnings_and_criticals_stop_in_debugger
                (G_LOG_DOMAIN,
		 "GLib",
		 "GLib-GObject",
		 "GModule",
		 "GThread",
                 "GnomeVFS",
                 "Medusa",
                 NULL);
        
        unparsed_arguments = poptGetArgs (popt_context);
        /* FIXME:  Need to check for URI + other arguments */
        if (unparsed_arguments != NULL) {
                poptPrintUsage (popt_context, stderr, 0);
                exit (1);
        }

        g_get_current_time (&start);
        master_db = medusa_master_db_open (ROOT_DIRECTORY, index_name);
        g_get_current_time (&finish);
        printf ("Took %ld seconds, and %ld milliseconds\n", finish.tv_sec - start.tv_sec, (finish.tv_usec - start.tv_usec) / 1000);

        g_get_current_time (&start);

        if (search_uri) {
                parsed_search_uri = medusa_search_uri_parse (search_uri, 
                                                             master_db);
                if (medusa_parsed_search_uri_error (parsed_search_uri, &error_message)) {
                        g_print ("%s\n", error_message);
                        return 1;
                }
        }
        else {
                parsed_search_uri = construct_and_parse_search_uri (file_name,
                                                                    mtime,
                                                                    path,
                                                                    size,
                                                                    type,
                                                                    mime_type,
                                                                    master_db);
        }
        
        results = medusa_master_db_run_search (master_db, parsed_search_uri);
        g_get_current_time (&finish);
        printf ("Took %ld seconds, and %ld milliseconds\n", finish.tv_sec - start.tv_sec, (finish.tv_usec - start.tv_usec) / 1000);
        medusa_parsed_search_uri_free (parsed_search_uri);

        g_list_foreach (results,
                        print_result,
                        NULL);

        medusa_g_list_free_deep (results);

        return 0;

}











